﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteComercial.aspx.cs" Inherits="Track_Web.ReporteComercial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

  <script type="text/javascript">

    Ext.onReady(function () {

      Ext.QuickTips.init();
      Ext.Ajax.timeout = 600000;
      Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
      Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });
        
      var dateDesde = new Ext.form.DateField({
          id: 'dateDesde',
          fieldLabel: 'Desde',
          labelWidth: 100,
          allowBlank: false,
          anchor: '99%',
          format: 'd-m-Y',
          editable: false,
          value: new Date(),
          maxValue: new Date(),
          style: {
              marginLeft: '5px'
          }
      });

      var dateHasta = new Ext.form.DateField({
          id: 'dateHasta',
          fieldLabel: 'Hasta',
          labelWidth: 100,
          allowBlank: false,
          anchor: '99%',
          format: 'd-m-Y',
          editable: false,
          value: new Date(),
          minValue: Ext.getCmp('dateDesde').getValue(),
          maxValue: new Date(),
          style: {
              marginLeft: '5px'
          }
      });

      dateDesde.on('change', function () {
          var _desde = Ext.getCmp('dateDesde');
          var _hasta = Ext.getCmp('dateHasta');

          _hasta.setMinValue(_desde.getValue());
          _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
          _hasta.validate();
      });

      var storeFiltroCedis = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['Cedis'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllCedisPool&Todas=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroCedis = new Ext.form.field.ComboBox({
        id: 'comboFiltroCedis',
        fieldLabel: 'Cedis',
        labelWidth: 100,
        store: storeFiltroCedis,
        valueField: 'Cedis',
        displayField: 'Cedis',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        multiSelect: true,
        style: {
          marginLeft: '5px'
        },
      });

      Ext.getCmp('comboFiltroCedis').store.load({
        param: {
          transportista: 'Todos'
        },
        callback: function (r, options, success) {
          if (success) {

            var firstceids = Ext.getCmp("comboFiltroCedis").store.getAt(0).get("Cedis");
            Ext.getCmp("comboFiltroCedis").setValue(firstceids);
          }
        }
      })

      var btnBuscar = {
        id: 'btnBuscar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/searchreport_black_20x20.png',
        text: 'Buscar',
        width: 90,
        height: 26,
        handler: function () {
          Buscar();
        }
      };

      var btnExportar = {
        id: 'btnExportar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/export_black_20x20.png',
        text: 'Exportar',
        width: 90,
        height: 26,
        listeners: {
          click: {
            element: 'el',
            fn: function () {

              var mapForm = document.createElement("form");
              mapForm.target = "ToExcel";
              mapForm.method = "POST"; // or "post" if appropriate
              mapForm.action = 'ReporteComercial.aspx?Metodo=ExportExcel';

              document.body.appendChild(mapForm);
              mapForm.submit();

            }
          }
        }
      };

      var panelFilters = new Ext.FormPanel({
        id: 'panelFilters',
        title: 'Filtros Reporte',
        anchor: '100% 100%',
        bodyStyle: 'padding: 5px;',
        layout: 'anchor',
        items: [{
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 1,
          items: [dateDesde, dateHasta, comboFiltroCedis]
        }],
        buttons: [btnExportar, btnBuscar]
      });

      var storeReporte = new Ext.data.JsonStore({
        autoLoad: false,
        fields: [
                  'Id',
                  'Transportista',
                  'CantidadPlacas',
                  'PlacasIntegradas',
                  'PlacasEsperadas',
                  'PlacasRealesReportando',
                  'Integrado',
                  'Reportando'
        ],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetReporteComercial',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var gridPanelReporte = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelReporte',
        title: 'Reporte Comercial',
        store: storeReporte,
        anchor: '100% 100%',
        columnLines: true,
        scroll: true,
        plugins: [
            Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 2
            })
        ],
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columns: [
                    { text: 'Top 16', sortable: true, width: 50, dataIndex: 'Id' },
                    { text: 'Transporte', sortable: true, flex: 1, dataIndex: 'Transportista' },
                    { text: 'Placas Pool', sortable: true, flex: 1, dataIndex: 'CantidadPlacas' },
                    { text: 'Placas Integradas', sortable: true, flex: 1, dataIndex: 'PlacasIntegradas' },
                    { text: 'Placas Esperadas', sortable: true, flex: 1, dataIndex: 'PlacasEsperadas' },
                    { text: 'Placas Reales Reportando', sortable: true, flex: 1, dataIndex: 'PlacasRealesReportando' },
                    { text: 'Integrado', sortable: true, flex: 1, dataIndex: 'Integrado', renderer: renderPorciento },
                    { text: 'Reportando', sortable: true, flex: 1, dataIndex: 'Reportando', renderer: renderPorciento }
        ]

      });

      var leftPanel = new Ext.FormPanel({
        id: 'leftPanel',
        region: 'west',
        border: true,
        margins: '0 0 3 3',
        width: 300,
        minWidth: 200,
        maxWidth: 400,
        layout: 'anchor',
        collapsible: true,
        titleCollapsed: false,
        split: true,
        items: [panelFilters]
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        items: [gridPanelReporte]
      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, leftPanel, centerPanel]
      });
    });

  </script>


  <script type="text/javascript">


    function Buscar() {

      var desde = Ext.getCmp('dateDesde').getValue();
      var hasta = Ext.getCmp('dateHasta').getValue();
      var idCedis = Ext.getCmp('comboFiltroCedis').getValue();

      if (idCedis === null || idCedis === "") {
        idCedis = ""
      }

      var store = Ext.getCmp('gridPanelReporte').store;
      store.load({
        params: {
          desde: desde,
          hasta: hasta,
          idCedis: idCedis
        },
        callback: function (r, options, success) {
          if (!success) {
            Ext.MessageBox.show({
              title: 'Error',
              msg: 'Se ha producido un error.',
              buttons: Ext.MessageBox.OK
            });
          }
        }
      });
    }

      var renderPorciento = function (value, meta) {
          {
              value = value + " %";
                  return value;
          }
      };

  </script>
</asp:Content>
