﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

namespace Track_Web
{
  public partial class MonitoreoOnline : System.Web.UI.Page
  {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel(Request.Form["patente"].ToString(), Request.Form["economico"].ToString(), Request.Form["transportista"].ToString(), Request.Form["estadoViaje"].ToString(), Request.Form["estadoGPS"].ToString(), Request.Form["proveedorGPS"].ToString(), Request.QueryString["nroTransporte"]);
                    return;
                default:
                    break;
            }
        }

        public void ExportExcel(string patente, string economico, string transportista, string estadoViaje, string estadoGPS, string proveedorGPS, string nroTransporte)
        {
            string now = DateTime.Now.ToString();
            now = now.Replace(" ", "_");
            now = now.Replace("-", "");
            now = now.Replace(":", "");

            int _ignicion = -1;

            long _nroTransporte;

            string userName = Utilities.GetUsuarioSession(Session);

            long.TryParse(nroTransporte, out _nroTransporte);

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            List<Track_GetMonitoreoOnline_2_Result> _viajes = _objMethosViajes.GetMonitoreoOnline(patente, economico, transportista, _ignicion, estadoViaje, estadoGPS, proveedorGPS, _nroTransporte, userName);

            Response.Clear();
            Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "text/csv";

            Response.AppendHeader("Content-Disposition", "attachment;filename=MonitoreoOnline_" + now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(Methods_Export.HTML_MonitoreoOnline(_viajes.ToList()));
            Response.End();
        }
    }
}