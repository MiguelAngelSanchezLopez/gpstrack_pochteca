﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DashboardRutas.aspx.cs" Inherits="Track_Web.DashboardRutas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
  <script src="Scripts/RowExpander.js" type="text/javascript"></script>
  
<script type="text/javascript">

    var directionsService = new google.maps.DirectionsService;
    var directionsService2 = new google.maps.DirectionsService;
    var directionsService3 = new google.maps.DirectionsService;

    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsDisplay2 = new google.maps.DirectionsRenderer;
    var directionsDisplay3 = new google.maps.DirectionsRenderer;

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

        var southPanel = new Ext.FormPanel({
            id: 'southPanel',
            region: 'south',
            margins: '0 3 0 3',
            border: true,
            height: 150,
            layout: 'anchor',
            hideCollapseTool: true,
            items: []
        });

        var centerPanel = new Ext.FormPanel({
            id: 'centerPanel',
            region: 'center',
            border: true,
            margins: '0 3 3 3',
            anchor: '100% 100%',
            contentEl: 'dvMap'
        });

        var viewport = Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [topMenu, centerPanel, southPanel]
        });



    });
</script>

<script type="text/javascript">


  Ext.onReady(function () {
      GeneraMapa("dvMap", true);

      Draw();
  });

  function Draw()
  {
      directionsDisplay.setMap(map);
      directionsDisplay2.setMap(map);
      directionsDisplay3.setMap(map);

      var waypts = []
      var stop;

      var latlonOrigen;
      var latlonDestino;

      var request;

      stop = new google.maps.LatLng(-33.392218,-70.764786)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.364301,-70.708275)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.424500,-70.672666)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.480967,-70.657626)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.530761,-70.683946)
      waypts.push({
          location: stop,
          stopover: true
      });

      latlonOrigen = new google.maps.LatLng(-33.453885,-70.872560);
      latlonDestino = new google.maps.LatLng(-33.555089, -70.713664);

      request = {
          origin: latlonOrigen,
          destination: latlonDestino,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC,
          provideRouteAlternatives: false,
          region: 'CL'
      };

      directionsService.route(request, function (response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
              directionsDisplay.setOptions({
                  preserveViewport: true,
                  polylineOptions: {
                      strokeColor: 'blue'
                  },
                  draggable: true
              });
              directionsDisplay.setDirections(response);
          }
      });

      waypts = [];

      stop = new google.maps.LatLng(-33.401145, -71.103089)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.554283, -71.210190)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.672984, -71.182955)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-33.648048, -70.894255)
      waypts.push({
          location: stop,
          stopover: true
      });

      latlonOrigen = new google.maps.LatLng(-33.453885, -70.872560);
      latlonDestino = new google.maps.LatLng(-33.700934, -71.006830);

      request = {
          origin: latlonOrigen,
          destination: latlonDestino,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC,
          provideRouteAlternatives: false,
          region: 'CL'
      };

      directionsService2.route(request, function (response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
              directionsDisplay2.setOptions({
                  preserveViewport: true,
                  polylineOptions: {
                      strokeColor: 'red'
                  },
                  draggable: true
              });
              directionsDisplay2.setDirections(response);
          }
      });

      waypts = [];

      stop = new google.maps.LatLng(-33.184661, -70.995738)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-32.975126, -70.991661)
      waypts.push({
          location: stop,
          stopover: true
      });
      stop = new google.maps.LatLng(-32.924945, -70.813622)
      waypts.push({
          location: stop,
          stopover: true
      });


      latlonOrigen = new google.maps.LatLng(-33.453885, -70.872560);
      latlonDestino = new google.maps.LatLng(-33.057177, -70.873422);

      request = {
          origin: latlonOrigen,
          destination: latlonDestino,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC,
          provideRouteAlternatives: false,
          region: 'CL'
      };

      directionsService3.route(request, function (response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
              directionsDisplay3.setOptions({
                  preserveViewport: true,
                  polylineOptions: {
                      strokeColor: 'red'
                  }
              });
              directionsDisplay3.setDirections(response);
          }
      });


  }

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
