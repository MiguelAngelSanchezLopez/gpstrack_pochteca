﻿var CONTENEDOR_ASPNET = "MainContent_";
var todosFiltros = "";

function MostrarOcultarGeneral(nombreSeccion) {
    ($("#" + nombreSeccion).is(":visible"))
        ? $("#" + nombreSeccion).hide()
        : $("#" + nombreSeccion).show();
}

function MostrarOcultarSeccionAdministraDatos() {
    MostrarOcultarGeneral("seccionAdministraDatos");
}

function MostrarOcultarSeccionFiltros() {
    MostrarOcultarGeneral("seccionFiltros");
}

function MostrarOcultarSeccionConsultaDatos() {
    MostrarOcultarGeneral("seccionConsultaDatos");
}

function MostrarMensajeEsperaCargando() {
    swal({
        title: 'Realizando procesos en la base de datos'
    });
    swal.showLoading();
}

function MostrarAlertaExitosa() {
    MostrarMensajeEsperaCargando();
}

function LlenarDatosTabla() {
    $("#datosTabla").html(ConsumirObjetoJSON());
}

function RevisarPasoTodasValidaciones(pasoTodasValidaciones) {
    if (!pasoTodasValidaciones) {
        MostrarAlertaFallida("Introduzca todos los datos antes de continuar");
        throw "No fueron pasadas todas las validaciones";
    }
}

function ValidarCamposInformacion() {
    var pasoTodasValidaciones = true;

    for (var indice = 0; indice < arregloCamposValidacion.length; indice++)
        if ($("#" + arregloCamposValidacion[indice]).val() == "")
            pasoTodasValidaciones &= false;

    RevisarPasoTodasValidaciones(pasoTodasValidaciones);
}

function ProcesarConfirmacionMensajeBorrado(resultado) {
    if (resultado.value) {
        BorrarRegistroSeleccionado();
        MostrarAlertaGeneral('Registro Borrado', 'El registro ha sido borrado.', 'success');
    }
}

function MostrarMensajeBorrado() {
    swal({
        title: 'Esta Segur@ de querer eliminar este registro?',
        text: "No podra deshacer esta acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        ProcesarConfirmacionMensajeBorrado(result);
    })
}

function ResetearSelects() {
    $('.selectDinamico').val(0).trigger('change');
}

function InicializarTabla() {
    
    $('#table_id').DataTable({
        "columnDefs": [
            {
                "targets": [Columnas.Id, Columnas.Activo],
                "visible": false
            }
        ]
    });
}

function AsignarComportamientoBotonLimpiarFiltros() {
    $("#btnLimpiar").on("click", function () {
        FiltrarRegistrosTabla("");
        LimpiarCamposFiltros();
    });
}

function RehabilitarBotonesEdicionEliminacionRegistros() {
    AsignarComportamientoBotonEditarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
}

function FiltrarRegistrosTabla(filtros) {
    var table = $('#table_id').DataTable();
    table.search(filtros, true, false).draw();
    RehabilitarBotonesEdicionEliminacionRegistros();
}

function LimpiarFiltros() {
    todosFiltros = "";
}


function obtenerRestoFiltrosGeneral(nombreCampo) {
    return ($('#' + nombreCampo).val() != "") ? ("|" + $('#' + nombreCampo).val()) : "";
}

function obtenerTextoTodosFiltros() {
    var cadenaTodosFiltros = [];
    
    for (var indice = 0; indice < arregloCamposFiltros.length; indice++)
        cadenaTodosFiltros.push(obtenerRestoFiltrosGeneral(arregloCamposFiltros[indice]));

    todosFiltros = cadenaTodosFiltros.join("");
    todosFiltros = todosFiltros.substr(1);
    
}

function EstablecerEventoKeyUpTodosFiltros() {
    var cadenaTodosControles = [];

    var listaControles = "";

    for (var indice = 0; indice < arregloCamposFiltros.length; indice++)
        cadenaTodosControles.push('#' + arregloCamposFiltros[indice] + ",");

    listaControles = cadenaTodosControles.join("");

    listaControles=EliminarUltimoCaracterCadena(listaControles);

    $(listaControles).on('keyup', function () {
        LimpiarFiltros();
        obtenerTextoTodosFiltros();
        FiltrarRegistrosTabla(todosFiltros);
    });
}

function InicializarBusquedaAjenaTabla() {
    EstablecerEventoKeyUpTodosFiltros();
}

function OcultarFiltradorTabla() {
    $("#table_id_filter").css("display", "none");
}

function InicializarSelect() {
    $('.selectDinamico').select2();
}

function AsignarComportamientoCambioSelectGeneral(nombreCampo, nombreCampoTexto, elementoAObtenerDelSelect) {
    
    $('#' + nombreCampo).on('change', function () {
        try {

            //console.log("AsignarComportamientoCambioSelectGeneral nombreCampo: " + nombreCampo + " - nombreCampoTexto: " + nombreCampoTexto + " - elementoAObtenerDelSelect: " + elementoAObtenerDelSelect );

            var data = $('#' + nombreCampo).select2('data');

            //console.log("--> data: " + JSON.stringify(data));
            var cadenaData = JSON.stringify(data);
            
            if (cadenaData != "[]") { 

                //console.log("AsignarComportamientoCambioSelectGeneral nombreCampo: " + nombreCampo + " - nombreCampoTexto: " + nombreCampoTexto + " - elementoAObtenerDelSelect: " + elementoAObtenerDelSelect);

                if (elementoAObtenerDelSelect == "text") {
                    //console.log("--> data[0].text: " + data[0].text);
                    $("#" + nombreCampoTexto).val(data[0].text);
                }
                    

                if (elementoAObtenerDelSelect == "id") {
                    //console.log("--> data[0].id: " + data[0].id);
                    $("#" + nombreCampoTexto).val(data[0].id);
                }
                    

                LimpiarCampoTextoValorNulo(data, nombreCampoTexto);

                InvocarCambioCampoTexto(nombreCampoTexto);
            }
        } catch (err) {
            console.log(err.message);
        }
    });
    
    
}

function ObtenerValorActivo(elementoCheckBox) {
    return ($(elementoCheckBox).is(":checked")) ? "True" : "False";
}

function TratamientoJSONGeneral(cadenaJSON, objetoJSONDestino) {
    var objetoJSONTemporal = JSON.parse(cadenaJSON);

    (objetoJSONTemporal.Status) ? AsignarContenidoJSON(objetoJSONTemporal, objetoJSONDestino) : MostrarAlertaFallida(objetoJSONTemporal.Error);
}

function AsignarContenidoJSON(objetoJSONTemporal, objetoJSONDestino) {
    eval(objetoJSONDestino + "= JSON.parse(objetoJSONTemporal.InformacionContenida);");
}

function LlenarSelectGeneral(fuenteJSON, nombreSelect) {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(fuenteJSON, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "Nombre")
                NombreTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + "</option>");

    });

    $("#" + nombreSelect).html(elementosSelectGeneral.join(""));
}

function MostrarOcultarPanelesDeAcordeNecesidad() {
    MostrarOcultarSeccionFiltros();
    MostrarOcultarSeccionConsultaDatos();
    MostrarOcultarSeccionAdministraDatos();
}

var AsignarComportamientoBotonEliminarRegistro =function () {
    //$(".btnEliminarRegistro").on("click", function () {
    $('#table_id tbody').on('click', '.btnEliminarRegistro', function () {
        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        MostrarMensajeBorrado();
    });
}

function AsignarComportamientoCambioSelectGeneralTexto(nombreCampo, nombreCampoTexto) {
    AsignarComportamientoCambioSelectGeneral(nombreCampo, nombreCampoTexto, "text");
}

function AsignarComportamientoCambioSelectGeneralId(nombreCampo, nombreCampoTexto) {
    AsignarComportamientoCambioSelectGeneral(nombreCampo, nombreCampoTexto, "id");
}

function EstablecerObjetoJSONGeneral(nombreContenedor, objetoJSONDestino) {
    try {
        TratamientoJSONGeneral($("#" + CONTENEDOR_ASPNET + nombreContenedor).val(), objetoJSONDestino);
    } catch (err) {
        MostrarAlertaFallida(err.message);
    }
}

function MostrarBotonInsercionNuevoRegistro(mostrar) {
    $("#btnGuardar").css("display", ObtenerArgumentoDisplayDeBoolean(!mostrar));
    $("#btnInsertar").css("display", ObtenerArgumentoDisplayDeBoolean(mostrar));
}

function AsignarComportamientoBotonCancelarAgregacionRegistro() {
    $("#btnCancelar").on("click", function () {
        LimpiarCamposAdministracion();

        $("#seccionAdministraDatos").hide();
        $("#seccionFiltros").show();
        $("#seccionConsultaDatos").show();

        RehabilitarBotonesEdicionEliminacionRegistros();
    });
}

var AsignarComportamientoBotonGuardarRegistro =function () {
    $("#btnGuardar").on("click", function () {
        ActualizarRegistroSeleccionado();
        LimpiarCamposAdministracion();
        MostrarAlertaExitosa();
    });
}

var AsignarComportamientoBotonInsertarRegistro = function () {
    $("#btnInsertar").on("click", function () {
        InsertarRegistroSeleccionado();
        LimpiarCamposAdministracion();
        MostrarAlertaExitosa();
    });
}

function ObtieneDatoPorId(idDatoBuscado, objetoJSON, datosBuscados, nombreCampoClave) {
    var datoEncontrado = "";

    $.each(objetoJSON, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == nombreCampoClave)
                if (value == idDatoBuscado)
                    datoEncontrado = eval(datosBuscados);

        });
    });

    return datoEncontrado;
}

function ReiniciarTablaDatos() {
    try {
        $('#table_id').DataTable().clear();
        $('#table_id').DataTable().destroy();

        
    } catch (err) {
        console.log(err.message);
    }
}

function AsegurarMuestroCorrectoSeccionAdministraDatos() {
    if ($("#seccionAdministraDatos").is(":visible"))
        $("#seccionAdministraDatos").hide();
}