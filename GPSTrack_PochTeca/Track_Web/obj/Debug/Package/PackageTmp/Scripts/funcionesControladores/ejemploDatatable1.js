﻿var objetoJSONUsuarios =
    [
        { "id": "1","usuario": "John", "pasatiempo": "Correr" },
        { "id": "2","usuario": "Thomas", "pasatiempo": "Armas" },
        { "id": "3","usuario": "David", "pasatiempo": "Montañismo" }
    ];

function ObtenerDatosPorId(idRegistroBuscado) {
    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "id")
                if (value == idRegistroBuscado)
                    ObtenerRegistroCompleto(item);
            
        });
    });
}

var idUsuarioOperacionTemporal = "";

function LimpiarCamposAdministracion() {
    $("#txtUsuarioAdm").val("");
    $("#txtPasatiempoAdm").val("");
}

function ObtenerRegistroCompleto(elemento) {
    /*
    console.log("encontre lo que buscaba");
    */
    //console.log("id: " + elemento.id);
    /*
    console.log("usuario: " + elemento.usuario);
    console.log("pasatiempo: " + elemento.pasatiempo);
    */
    idUsuarioOperacionTemporal = elemento.usuario;
    $("#txtUsuarioAdm").val(elemento.usuario);
    $("#txtPasatiempoAdm").val(elemento.pasatiempo);
}

function ConsumirObjetoJSON() {

    var elementosTabla = [];

    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;

        elementosTabla.push("<tr>");

        var idTemporal = "";
        var usuarioTemporal = "";
        var pasatiempoTemporal = "";

        $.each(item, function (key, value) {
            
            if (key == "id")
                idTemporal = value;       

            if (key == "usuario")
                usuarioTemporal = value;   

            if (key == "pasatiempo")
                pasatiempoTemporal = value; 
             
        });

        elementosTabla.push("<td>" + usuarioTemporal+"</td>");
        elementosTabla.push("<td>" + pasatiempoTemporal+"</td>");
        elementosTabla.push("<td>");
        elementosTabla.push("<a data-IdRegistro='" + idTemporal+"' class='btn btn-primary btnEliminarRegistro' href='#' role='button'><i class='fa fa-trash'></i></a>");
        elementosTabla.push("&nbsp;&nbsp;&nbsp;");
        elementosTabla.push("<a data-IdRegistro='" + idTemporal +"' class='btn btn-primary btnEditarRegistro' href='#' role='button'><i class='fa fa-pencil'></i></a>");
        elementosTabla.push("</td>");
        elementosTabla.push("</tr>");
        
    });

    return elementosTabla.join("");
}

function LlenarDatosTabla() {
    $("#datosTabla").html(ConsumirObjetoJSON());
}



function MostrarAlertaGeneral(titulo, descripcion, tipo) {
    swal(
        titulo, descripcion, tipo
    );
}

function MostrarAlertaExitosa() {
    
    swal({
        title: 'Proceso Exitoso',
        text: 'Los Datos han sido actualizados',
        type: 'success'
    }).then(function () {
        MostrarOcultarPanelesDeAcordeNecesidad();
    });
    
}

function MostrarAlertaFallida() {
    MostrarAlertaGeneral('Proceso Fallido','Ocurrio un error','error');
}

function MostrarMensajeBorrado() {
    swal({
        title: 'Esta Segur@ de querer eliminar este registro?',
        text: "No podra deshacer esta acción",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
            MostrarAlertaGeneral('Registro Borrado', 'El registro ha sido borrado.', 'success');
    })
}

function MostrarOcultarGeneral(nombreSeccion) {
    ($("#" + nombreSeccion).is(":visible"))
    ?$("#" + nombreSeccion).hide()
    :$("#" + nombreSeccion).show();
}

function MostrarOcultarSeccionAdministraDatos() {
    MostrarOcultarGeneral("seccionAdministraDatos");
}

function MostrarOcultarSeccionFiltros() {
    MostrarOcultarGeneral("seccionFiltros");
}

function MostrarOcultarSeccionConsultaDatos() {
    MostrarOcultarGeneral("seccionConsultaDatos");
}

function AsignarComportamientoBotonAgregarRegistro() {
    $("#btnAgregar").on("click", function () {
        LimpiarCamposAdministracion();
        MostrarOcultarPanelesDeAcordeNecesidad();
    });
}

function AsignarComportamientoBotonCancelarAgregacionRegistro() {
    $("#btnCancelar").on("click", function () {
        LimpiarCamposAdministracion();
        MostrarOcultarPanelesDeAcordeNecesidad();
    });
}

function AsignarComportamientoBotonGuardarRegistro() {
    $("#btnGuardar").on("click", function () {
        LimpiarCamposAdministracion();
        MostrarAlertaExitosa();
    });
}

function MostrarOcultarPanelesDeAcordeNecesidad() {
    MostrarOcultarSeccionFiltros();
    MostrarOcultarSeccionConsultaDatos();
    MostrarOcultarSeccionAdministraDatos();
}

function AsignarComportamientoBotonEliminarRegistro() {
    $(".btnEliminarRegistro").on("click", function () {
        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        MostrarMensajeBorrado();
    });
}

function AsignarComportamientoBotonEditarRegistro() {
    $(".btnEditarRegistro").on("click", function () {
        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        MostrarOcultarPanelesDeAcordeNecesidad();
    });
}

function AsignarComportamientoBotonLimpiarFiltros() {
    $("#btnLimpiar").on("click", function () {

        FiltrarRegistrosTabla("");
        $('#txtUsuario').val("");
        $('#txtPasatiempo').val("");
    });
}

function InicializarTabla() {
    $('#table_id').DataTable();
}

function FiltrarRegistrosTabla(filtros) {
    var table = $('#table_id').DataTable();
    table.search(filtros, true, false).draw();
}

var filtroUsuario = "";
var filtroPasatiempo = "";

var todosFiltros = "";

function LimpiarFiltros() {
    todosFiltros = "";
}

function obtenerRestoFiltrosGeneral(nombreCampo) {
    return ($('#' + nombreCampo).val() != "") ? ("|" + $('#' + nombreCampo).val()) : "";
}

function obtenerRestoFiltros1() {
    return obtenerRestoFiltrosGeneral("txtPasatiempo");
}

function obtenerRestoFiltros2() {
    return obtenerRestoFiltrosGeneral("txtUsuario");
}

function InicializarBusquedaAjenaTabla() {
    
    $('#txtUsuario').on('keyup', function () {
        LimpiarFiltros();
        filtroUsuario = this.value;
        todosFiltros += filtroUsuario + obtenerRestoFiltros1();
        FiltrarRegistrosTabla(todosFiltros);
    });

    $('#txtPasatiempo').on('keyup', function () {
        LimpiarFiltros();
        filtroPasatiempo = this.value;
        todosFiltros += filtroPasatiempo + obtenerRestoFiltros2();
        FiltrarRegistrosTabla(todosFiltros);
    });

}

function OcultarFiltradorTabla() {
    $("#table_id_filter").css("display","none");
}

$(document).ready(function () {
    LlenarDatosTabla();
    InicializarTabla();
    
    OcultarFiltradorTabla();
    MostrarOcultarSeccionAdministraDatos();
    AsignarComportamientoBotonAgregarRegistro();
    AsignarComportamientoBotonCancelarAgregacionRegistro();
    AsignarComportamientoBotonGuardarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
    AsignarComportamientoBotonEditarRegistro();
    AsignarComportamientoBotonLimpiarFiltros();
    InicializarBusquedaAjenaTabla();
    
});