﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfigViajes_v2.aspx.cs" Inherits="Track_Web.ConfigViajes_v2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

  <script type="text/javascript">

    var nameUsuarioConectado = "";
    var cedisAsociado = 0;
    var fechaActual;    
    var fechaEstimadaLlegada = "";

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 3600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

      var textFechaActual = new Ext.form.TextField({
        fieldLabel: 'Fecha y Hora',
        id: 'textFechaActual',
        anchor: '99%',
        labelWidth: 100,
        readOnly: true,
        style: {
          marginLeft: '15px'
        },
      });

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetDiferenciaHoraria',
        success: function (data, success) {

          fechaActual = new Date().addHours(data.responseText);
          curDateTime = new Date().addHours(data.responseText).toISOString();

          var curDate = curDateTime.substring(0, 10);
          var curTime = curDateTime.substring(11, 16)

          Ext.getCmp("textFechaActual").setValue(curDate + " " + curTime);
        }
      });

      var textUsuarioConectado = new Ext.form.TextField({
        fieldLabel: 'Usuario',
        id: 'textUsuarioConectado',
        anchor: '99%',
        labelWidth: 100,
        readOnly: true,
        style: {
          marginLeft: '15px'
        },
      });

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetNameUsuarioConectado',
        success: function (data, success) {
          nameUsuarioConectado = data.responseText;
          Ext.getCmp("textUsuarioConectado").setValue(nameUsuarioConectado);
        }
      });

      var numberNroTransporte = new Ext.form.NumberField({
        fieldLabel: 'Id. Master',
        id: 'numberNroTransporte',
        allowBlank: false,
        labelWidth: 120,
        anchor: '99%',
        minValue: 1,
        maxValue: 9999999999,
        maxLength: 10,
        enforceMaxLength: true,
        enableKeyEvents: true
      });

      var numberIdEmbarque = new Ext.form.NumberField({
        fieldLabel: 'ID Embarque',
        id: 'numberIdEmbarque',
        allowBlank: false,
        labelWidth: 100,
        anchor: '98%',
        minValue: 1,
        maxValue: 999999,
        maxLength: 6,
        enforceMaxLength: true,
        style: {
          marginLeft: '15px'
        },
        enableKeyEvents: true
      });

      var storeFiltroTransportista = new Ext.data.JsonStore({
          fields: ['Transportista'],
          proxy: new Ext.data.HttpProxy({
              //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetTransportistasRuta&Todos=True',
              url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=False',
              headers: {
                  'Content-type': 'application/json'
              }
          })
      });

      var comboFiltroTransportista = new Ext.form.field.ComboBox({
        id: 'comboFiltroTransportista',
        fieldLabel: 'Línea Transporte',
        forceSelection: true,
        store: storeFiltroTransportista,
        valueField: 'Transportista',
        displayField: 'Transportista',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        allowBlank: false,
        forceSelection: true,
        listeners: {
          select: function () {
            //FiltrarPatentes();
            Ext.getCmp("comboFiltroPatenteTracto").setDisabled(false);
            Ext.getCmp("comboFiltroPatenteTrailer").setDisabled(false);
          }
        }
      });

      storeFiltroTransportista.load({
          callback: function (r, options, success) {
              Ext.getCmp("comboFiltroTransportista").setValue("TLYDSA");

          }
      })

      var storeFiltroPatenteTracto = new Ext.data.JsonStore({
          fields: ['Patente'],
          proxy: new Ext.data.HttpProxy({
              //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPatentesRuta&Todos=True',
              url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=False',
              headers: {
                  'Content-type': 'application/json'
              }
          })
      });

      var storeFiltroPatenteTrailer = new Ext.data.JsonStore({
          fields: ['Patente'],
          proxy: new Ext.data.HttpProxy({
              //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPatentesRuta&Todos=True',
              url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=False',
              headers: {
                  'Content-type': 'application/json'
              }
          })
      });

      var comboFiltroPatenteTracto = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTracto',
        fieldLabel: 'Placa Tracto',        
        store: storeFiltroPatenteTracto,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        allowBlank: false,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,        
        disabled: false,
        listeners: {
          change: function () {

            var patente = Ext.getCmp("comboFiltroPatenteTracto").getValue();

            Ext.Ajax.request({
              url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
              params: {
                  patente: patente,
                  economico: ""
              },
              success: function (data, success) {
                if (data != null) {
                  data = Ext.decode(data.responseText);

                  Ext.getCmp("imageStatusTracto").show();

                  switch (data[0].Estado) {
                    case "Online":
                      Ext.getCmp('imageStatusTracto').setSrc('Images/status_green_16x16.png');
                      break;
                    default:
                      Ext.getCmp('imageStatusTracto').setSrc('Images/status_red_16x16.png');
                  }

                }
              }
            })
          }
        }
      });

      var imageStatusTracto = Ext.create('Ext.Img', {
        id: 'imageStatusTracto',
        height: 18,
        width: 18,
        style: {
            marginTop: '3px',
            marginLeft: '4px'
        },
        hidden: false
      });

      var comboFiltroPatenteTrailer = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTrailer',
        fieldLabel: 'Placa Remolque',        
        store: storeFiltroPatenteTrailer,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        allowBlank: false,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,        
        disabled: false,
        listeners: {
          change: function () {

            var patente = Ext.getCmp("comboFiltroPatenteTrailer").getValue();

            Ext.Ajax.request({
              url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
              params: {
                  patente: patente,
                  economico: ""
              },
              success: function (data, success) {
                if (data != null) {
                  data = Ext.decode(data.responseText);

                  Ext.getCmp("imageStatusTrailer").show();

                  switch (data[0].Estado) {
                    case "Online":
                      Ext.getCmp('imageStatusTrailer').setSrc('Images/status_green_16x16.png');
                      break;
                    default:
                      Ext.getCmp('imageStatusTrailer').setSrc('Images/status_red_16x16.png');
                  }

                }
              }
            })
          }
        }
      });

      storeFiltroPatenteTracto.load();
      storeFiltroPatenteTrailer.load();

      var imageStatusTrailer = Ext.create('Ext.Img', {
        id: 'imageStatusTrailer',
        height: 18,
        width: 18,
        style: {
            marginTop: '3px',
            marginLeft: '4px'
        },
        hidden: false
      });

      var storeFiltroTipoViaje = new Ext.data.JsonStore({
          fields: ['TipoViaje'],
          data: [{ "TipoViaje": "TRASPASO" },
                 { "TipoViaje": "COMPRAS" },
          ]
      });

      var comboFiltroTipoViaje = new Ext.form.field.ComboBox({
          id: 'comboFiltroTipoViaje',
          fieldLabel: 'Tipo Viaje',
          forceSelection: false,
          editable: false,
          readOnly: false,
          store: storeFiltroTipoViaje,
          valueField: 'TipoViaje',
          displayField: 'TipoViaje',
          emptyText: 'Seleccione...',
          queryMode: 'local',
          anchor: '98%',
          labelWidth: 120,
          style: {
              marginLeft: '10px'
          },
      });

      Ext.getCmp("comboFiltroTipoViaje").setValue("TRASPASO");

      var storeFiltroRegresaEnvases = new Ext.data.JsonStore({
          fields: ['RegresaEnvases'],
          data: [{ "RegresaEnvases": "Si" },
                 { "RegresaEnvases": "No" },
          ]
      });

      var comboFiltroRegresaEnvases = new Ext.form.field.ComboBox({
          id: 'comboFiltroRegresaEnvases',
          fieldLabel: 'Regresa envases?',
          forceSelection: false,
          editable: false,
          readOnly: false,
          store: storeFiltroRegresaEnvases,
          valueField: 'RegresaEnvases',
          displayField: 'RegresaEnvases',
          queryMode: 'local',
          anchor: '98%',
          labelWidth: 120,
          style: {
              marginLeft: '10px'
          },
      });

      Ext.getCmp("comboFiltroRegresaEnvases").setValue("No");

      var storeFiltroTipoUnidad = new Ext.data.JsonStore({
          fields: ['TipoUnidad'],
          data: [{ "TipoUnidad": "SENCILLO" },
                 { "TipoUnidad": "FULL" },
                 { "TipoUnidad": "PIPA" },
                 { "TipoUnidad": "RABON" },
                 { "TipoUnidad": "TANQUE" },
          ]
      });

      var comboFiltroTipoUnidad = new Ext.form.field.ComboBox({
          id: 'comboFiltroTipoUnidad',
          fieldLabel: 'Tipo Unidad',
          forceSelection: false,
          editable: false,
          readOnly: false,
          store: storeFiltroTipoUnidad,
          valueField: 'TipoUnidad',
          displayField: 'TipoUnidad',
          queryMode: 'local',
          anchor: '99%',
          labelWidth: 120,
      });

      Ext.getCmp("comboFiltroTipoUnidad").setValue("SENCILLO");

      var storeFiltroTipoDestino = new Ext.data.JsonStore({
        fields: ['IdZona', 'TipoDestino'],
        data: [{ "IdZona": 1, "TipoDestino": "CEDIS" },
               { "IdZona": 2, "TipoDestino": "Sucursal" },
               { "IdZona": 5, "TipoDestino": "Proveedor" }
        ]
      });

      var comboFiltroTipoDestino = new Ext.form.field.ComboBox({
        id: 'comboFiltroTipoDestino',
        fieldLabel: 'Tipo Destino',
        forceSelection: false,
        editable: false,
        readOnly: false,
        store: storeFiltroTipoDestino,
        valueField: 'IdZona',
        displayField: 'TipoDestino',
        emptyText: 'Seleccione...',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 100,
        listeners: {
          select: function () {
            storeZonasDestino.load({
              params: {
                idTipoZona: Ext.getCmp("comboFiltroTipoDestino").value,
                nombreZona: ''
              }
            });
            Ext.getCmp("comboZonaDestino").setDisabled(false);
            Ext.getCmp("dateCita").setDisabled(false);
            Ext.getCmp("hourCita").setDisabled(false);
            Ext.getCmp("btnAddDestino").setDisabled(true);
            Ext.getCmp("comboZonaDestino").reset();
            Ext.getCmp("dateCita").reset();
            Ext.getCmp("hourCita").reset();

          }
        }
      });

      var storeZonasOrigen = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboZonaOrigen = new Ext.form.field.ComboBox({
        id: 'comboZonaOrigen',
        fieldLabel: 'Origen',
        allowBlank: false,
        store: storeZonasOrigen,
        valueField: 'IdZona',
        displayField: 'NombreZona',
        queryMode: 'local',
        anchor: '99%',
        forceSelection: true,
        enableKeyEvents: true,
        editable: true,
        labelWidth: 120,
        emptyText: 'Seleccione...',
        listConfig: {
          loadingText: 'Buscando...',
          getInnerTpl: function () {
            return '<a class="search-item">' +
                              '<span>Id Zona: {IdZona}</span><br />' +
                              '<span>Nombre: {NombreZona}</span>' +
                          '</a>';
          }
        }
      });

      var storeZonasDestino = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        }),
        helper: function (value) {
          if (Ext.isEmpty(value, false)) {
            return false;
          }
          value = this.data.createValueMatcher(value, true, false);
          return function (r) { return value.test(r.data['IdZona']) || value.test(r.data['NombreZona']); };
        },
        filter: function (property, value, anyMatch, caseSensitive) {
          var fn = this.helper(value);
          return fn ? this.filterBy(fn) : this.clearFilter();
        }
      });

      var comboZonaDestino = new Ext.form.field.ComboBox({
        id: 'comboZonaDestino',
        fieldLabel: 'Destino',
        allowBlank: true,
        store: storeZonasDestino,
        valueField: 'IdZona',
        displayField: 'NombreZona',
        queryMode: 'local',
        anchor: '99%',
        allowEmpty: false,
        forceSelection: true,
        enableKeyEvents: true,
        editable: true,
        labelWidth: 100,
        emptyText: 'Seleccione...',
        disabled: true,
        listConfig: {
          loadingText: 'Buscando...',
          getInnerTpl: function () {
            return '<a class="search-item">' +
                              '<span>Id Zona: {IdZona}</span><br />' +
                              '<span>Nombre: {NombreZona}</span>' +
                          '</a>';
          }
        },
        listeners: {
          select: function () {
            Ext.getCmp("btnAddDestino").setDisabled(false);
          }
        }

      });

      var storeDestinos = new Ext.data.JsonStore({
        fields: ['IdZona', 'IdTipoDestino', 'NombreTipoDestino', 'NombreDestino', 'LlegadaEstimada', 'EstadoVentana']
      });

      var gridPanelDestinos = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelDestinos',
        store: storeDestinos,
        width: 550,
        height: 100,
        columnLines: true,
        scroll: false,
        style: {
          marginTop: '1px'
        },
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columns: [
            { text: 'Código', sortable: true, width: 50, dataIndex: 'IdZona' },
            { text: 'Tipo', sortable: true, width: 80, dataIndex: 'NombreTipoDestino' },
            { text: 'Nombre', sortable: true, flex: 1, dataIndex: 'NombreDestino' },
            {
              xtype: 'actioncolumn',
              width: 23,
              editor: false,
              items: [
                  {
                    icon: 'Images/delete.png',
                    tooltip: 'Eliminar',
                    handler: function (grid, rowIndex, colIndex) {
                      var row = grid.getStore().getAt(rowIndex);
                      grid.getStore().remove(row);
                    }
                  }]
            }
        ]
      });

      var dateCita = new Ext.form.DateField({
          id: 'dateCita',
          fieldLabel: 'Cita',
          labelWidth: 100,
          allowBlank: false,
          anchor: '99%',
          format: 'd-m-Y',
          editable: false,
          value: new Date(),
          minValue: new Date(),
          disabled: true
      });

      var hourCita = {
          xtype: 'timefield',
          id: 'hourCita',
          allowBlank: false,
          format: 'H:i',
          minValue: '00:00',
          maxValue: '23:59',
          increment: 10,
          anchor: '95%',
          editable: true,
          value: '00:00',
          disabled: true,
          style: {
              marginTop: '4px'
          },
      };

      var btnAddDestino = {
        id: 'btnAddDestino',
        xtype: 'button',
        iconAlign: 'left',
        text: 'Agregar',
        icon: 'Images/add_blue_16x15.png',
        width: 95,
        height: 23,
        disabled: true,
        handler: function () {

          var idZona = Ext.getCmp("comboZonaDestino").getValue();
          var idTipoDestino = Ext.getCmp("comboFiltroTipoDestino").getValue();
          var nombreTipoDestino = Ext.getCmp("comboFiltroTipoDestino").getRawValue();
          var nombreDestino = Ext.getCmp("comboZonaDestino").getRawValue();

          var record = storeDestinos.findRecord('IdZona', idZona);

          if (record == null) {

            var recordOrigen = storeZonasOrigen.findRecord('IdZona', Ext.getCmp("comboZonaOrigen").getValue());
            var recordDestino = storeZonasDestino.findRecord('IdZona', Ext.getCmp("comboZonaDestino").getValue());

            storeDestinos.add({
              IdZona: idZona,
              IdTipoDestino: idTipoDestino,
              NombreTipoDestino: nombreTipoDestino,
              NombreDestino: nombreDestino
            });

            Ext.getCmp("comboZonaDestino").reset();

          }
        }
      };

      var textIdConductor = new Ext.form.TextField({
          id: 'textIdConductor',
          fieldLabel: 'Id. Conductor',
          anchor: '99%',
          labelWidth: 120
      });

      var textNombreConductor = new Ext.form.TextField({
          id: 'textNombreConductor',
        fieldLabel: 'Nombre Conductor',
        anchor: '99%',
        labelWidth: 120
      });

      var btnCrearViaje = {
        id: 'btnCrearViaje',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/add_blue_20x19.png',
        text: 'Nuevo viaje',
        width: 100,
        height: 27,
        handler: function () {
          winCrearViaje.show();
        }
      };

      var storeViajesAsignados = new Ext.data.JsonStore({
        autoLoad: true,
        fields: [
            'IdViaje',
            'NroTransporte',
            'IdEmbarque',
            'SecuenciaDestino',
            'RutConductor',
            'NombreConductor',
            'PatenteTracto',
            'PatenteTrailer',
            'RutTransportista',
            'NombreTransportista',
            'CodigoOrigen',
            'NombreOrigen',
            'CodigoDestino',
            'NombreDestino',
            'Comentarios',
            'TipoViaje',

             { name: 'FechaAsignacion', type: 'date', dateFormat: 'c' }
        ],
        proxy: new Ext.data.HttpProxy({
          url: 'Ajaxpages/AjaxViajes.aspx?Metodo=GetViajesAsignados',
          reader: { type: 'json', root: 'd' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var gridViajesAsignados = Ext.create('Ext.grid.Panel', {
        id: 'gridViajesAsignados',
        title: 'Viajes asignados',
        hideCollapseTool: true,
        anchor: '100% 99%',
        buttons: [btnCrearViaje],
        store: storeViajesAsignados,
        scroll: false,
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columnLines: true,
        columns: [  { text: 'ID Master', width: 100, sortable: true, dataIndex: 'NroTransporte' },
                    { text: 'Tipo Viaje', flex: 1, sortable: true, dataIndex: 'TipoViaje' },
                    { text: 'Línea Transporte', flex: 1, sortable: true, dataIndex: 'NombreTransportista' },
                    { text: 'Operador', flex: 1, sortable: true, dataIndex: 'NombreConductor' },
                    { text: 'Tracto', sortable: true, width: 80, dataIndex: 'PatenteTracto' },
                    { text: 'Trailer', sortable: true, width: 80, dataIndex: 'PatenteTrailer' },
                    { text: 'Origen', flex: 1, sortable: true, dataIndex: 'NombreOrigen' },
                    { text: 'Destino', flex: 1, sortable: true, dataIndex: 'NombreDestino' },
                    { text: 'Fecha asignación', width: 110, sortable: true, dataIndex: 'FechaAsignacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    {
                        xtype: 'actioncolumn',
                        width: 20,
                        items: [
                          {
                              icon: 'Images/delete.png',
                              tooltip: 'Eliminar viaje',
                              handler: function (grid, rowIndex, colIndex) {
                                  var row = grid.getStore().getAt(rowIndex);
                                  DeleteViaje(row.data.NroTransporte, row.data.CodigoDestino);


                              }
                          }]
                    }
                ]
      });

      storeZonasOrigen.load({
        params: {
          idTipoZona: 1,
          nombreZona: ''
        },
        callback: function (r, options, success) {
          if (success) {
            Ext.Ajax.request({
              url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetCedisAsociado',
              success: function (data, success) {
                cedisAsociado = parseInt(data.responseText);

                if (cedisAsociado > 0) {
                  Ext.getCmp("comboZonaOrigen").setValue(cedisAsociado);
                  Ext.getCmp("comboZonaOrigen").setReadOnly(true);

                  storeFiltroConductores.load({
                    params: {
                      CEDIS: cedisAsociado,
                    }
                  })

                  storeFiltroTransportista.load({
                      params: {
                          CEDIS: Ext.getCmp("comboZonaOrigen").getValue()
                      },
                      callback: function (r, options, success) {

                          if (Ext.getCmp('comboFiltroTransportista').store.count() > 0) {
                              var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.data.items[0].data.Transportista;
                              Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);
                          }

                          var storeTracto = Ext.getCmp('comboFiltroPatenteTracto').store;
                          storeTracto.load({
                              params: {
                                  tipoMovil: 1
                              }
                          });

                          var storeTrailer = Ext.getCmp('comboFiltroPatenteTrailer').store;
                          storeTrailer.load({
                              params: {
                                  tipoMovil: "2"
                              }
                          });

                          Ext.getCmp("comboFiltroPatenteTracto").setDisabled(false);
                          Ext.getCmp("comboFiltroPatenteTrailer").setDisabled(false);
                          Ext.getCmp("comboFiltroTipoUnidad").setDisabled(false);
                          
                      }


                  })

                }
              }
            });
          }
        }
      });

      var formCrearViaje = new Ext.FormPanel({
        id: 'formCrearViaje',
        border: false,
        frame: true,
        width: 600,
        height: 560,
        items: [textFechaActual, textUsuarioConectado,
            {
              xtype: 'fieldset',
              title: 'Información del Viaje',
              style: {
                marginLeft: '5px',
                marginRight: '5px'
              },
              layout: 'column',
              anchor: '100% 20%',
              items: [
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.5,
                items: [numberNroTransporte]
              },
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.5,
                items: [comboFiltroTipoViaje]
              },
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.5,
                items: [comboFiltroTransportista]
              }, {
                  xtype: 'container',
                  layout: 'anchor',
                  columnWidth: 0.5,
                  items: [comboFiltroRegresaEnvases]
              },
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboZonaOrigen]
                }
              ]
            },
            {
              xtype: 'fieldset',
              title: 'Detalle de Destino',
              style: {
                marginLeft: '5px',
                marginRight: '5px'
              },
              anchor: '100% 40%',
              layout: 'column',
              items: [{
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 1,
                items: [comboFiltroTipoDestino]
              },
                      {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.82,
                        items: [comboZonaDestino, dateCita]
                      },
                      {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.18,
                        items: [btnAddDestino, hourCita]
                      },
                      {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 1,
                        items: [gridPanelDestinos]
                      }

              ]
            },
            {
              xtype: 'fieldset',
              title: 'Información de placas',
              style: {
                marginLeft: '5px',
                marginRight: '5px'
              },
              layout: 'column',
              anchor: '100% 30%',
              items: [{
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.95,
                        items: [comboFiltroPatenteTracto]
                      },
                        {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.05,
                        items: [imageStatusTracto]
                        },
                        {
                            xtype: 'container',
                            layout: 'anchor',
                            columnWidth: 1,
                            items: [comboFiltroTipoUnidad]
                        },
                        {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.95,
                        items: [comboFiltroPatenteTrailer]
                        },
                        {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.05,
                        items: [imageStatusTrailer]
                        },
                    {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 1,
                        items: [textIdConductor]
                    },
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [textNombreConductor]
                }
              ]
            },
        ]
      });

      var btnGuardar = {
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/save_black_20x20.png',
        text: 'Guardar',
        width: 90,
        height: 26,
        handler: function () {
          GuardarViaje();
        }
      };

      var btnCancelar = {
        id: 'btnCancelar',
        xtype: 'button',
        width: 90,
        height: 26,
        iconAlign: 'left',
        icon: 'Images/back_black_20x20.png',
        text: 'Cancelar',
        handler: function () {
          Cancelar();
        }
      };

      var winCrearViaje = new Ext.Window({
        id: 'winCrearViaje',
        title: 'Datos del viaje',
        closeAction: 'hide',
        modal: true,
        items: formCrearViaje,
        resizable: false,
        border: false,
        constrain: true,
        buttons: [btnGuardar, btnCancelar]
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        items: [gridViajesAsignados]

      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, centerPanel]
      });

    });

  </script>

  <script type="text/javascript">

    function Cancelar() {

      Ext.getCmp("winCrearViaje").hide();

      Ext.getCmp("numberNroTransporte").setDisabled(false);
      Ext.getCmp("numberIdEmbarque").setDisabled(false);

      Ext.getCmp("imageStatusTracto").hide();
      Ext.getCmp("imageStatusTrailer").hide();

      Ext.getCmp("comboZonaDestino").setDisabled(true);
      Ext.getCmp("dateCita").setDisabled(true);
      Ext.getCmp("hourCita").setDisabled(true);
      Ext.getCmp("btnAddDestino").setDisabled(true);

      Ext.getCmp("numberNroTransporte").reset();
      Ext.getCmp("numberIdEmbarque").reset();
      Ext.getCmp("comboFiltroTransportista").reset();
      Ext.getCmp("comboFiltroPatenteTracto").reset();
      Ext.getCmp("comboFiltroPatenteTrailer").reset();
      Ext.getCmp("comboFiltroTipoUnidad").reset();
      
      Ext.getCmp("comboFiltroTipoDestino").reset();
      Ext.getCmp("comboZonaDestino").reset();
      Ext.getCmp("dateCita").reset();
      Ext.getCmp("hourCita").reset();
      Ext.getCmp("textIdConductor").reset();
      Ext.getCmp("textNombreConductor").reset();

      Ext.getCmp("comboFiltroTransportista").setValue("TLYDSA");

      Ext.getCmp('gridPanelDestinos').getStore().removeAll();
    }

    function FiltrarPatentes() {
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

      var storeTracto = Ext.getCmp('comboFiltroPatenteTracto').store;
      storeTracto.load({
        params: {
          transportista: transportista
        }
      });

      var storeTrailer = Ext.getCmp('comboFiltroPatenteTrailer').store;
      storeTrailer.load({
        params: {
          transportista: "SIMSA"
        }
      });
    }

    function ValidarNroTransporte() {
      Ext.Ajax.request({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=ValidarNroTransporte',
        params: {
          nroTransporte: Ext.getCmp('numberNroTransporte').getValue()
        },
        success: function (data, success) {
          if (data != null) {
            data = (data.responseText.toLowerCase() == 'true');
            if (!data) {
              Ext.getCmp('numberNroTransporte').markInvalid("El ID embarque ingresado se encuentra repetido.");
            }
            else {
              Ext.getCmp('numberNroTransporte').clearInvalid();
            }
          }
        },
        failure: function (msg) {
          alert('Se ha producido un error.');
        }
      });
    }


    function GuardarViaje() {
      var flag = true;
      var message = '';
      var _vertices = new Array();

      if (Ext.getCmp('numberNroTransporte').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboFiltroPatenteTracto').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboFiltroPatenteTrailer').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboZonaOrigen').hasActiveError()) {
        return;
      }      

      if (!Ext.getCmp('formCrearViaje').getForm().isValid() || !Ext.getCmp("numberNroTransporte").getValue > 0 ) {
        return;
      }

      var listaDestinos = "";

      var store = Ext.getCmp('gridPanelDestinos').getStore();
      if (store.count() == 0)
      {
          return;
      }

      for (var i = 0; i < store.count() ; i++) {
        listaDestinos = listaDestinos + store.data.items[i].data.IdZona

        if (i < store.count() - 1) {
          listaDestinos = listaDestinos + ";"
        }

      }

      var dateCita = Ext.getCmp('dateCita').getValue();
      var hourCita = Ext.getCmp('hourCita').getRawValue();

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=NuevoViaje',
        params: {
          'nroTransporte': Ext.getCmp('numberNroTransporte').getValue(),
          'tipoViaje': Ext.getCmp('comboFiltroTipoViaje').getRawValue(),
          'transportista': Ext.getCmp('comboFiltroTransportista').getValue(),
          'regresaEnvases': Ext.getCmp('comboFiltroRegresaEnvases').getValue(),
          'tracto': Ext.getCmp('comboFiltroPatenteTracto').getValue(),
          'trailer': Ext.getCmp('comboFiltroPatenteTrailer').getValue(),
          'tipoUnidad': Ext.getCmp('comboFiltroTipoUnidad').getValue(),
          'rutConductor': Ext.getCmp('textIdConductor').getValue(),
          'NombreConductor': Ext.getCmp('textNombreConductor').getValue(),
          'codOrigen': Ext.getCmp('comboZonaOrigen').getValue(),
          'listaDestinos': listaDestinos,
          'dateCita': dateCita,
          'hourCita': hourCita
        },
        success: function (msg, success) {
          alert(msg.responseText);

          if (msg.responseText == "Viaje ingresado correctamente.") {
            Cancelar();
            Ext.getCmp('gridViajesAsignados').getStore().load();
          }

        },
        failure: function (msg) {
          alert('Se ha producido un error.');
        }
      });
    }

    function CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon) {

      var service = new google.maps.DistanceMatrixService();
      var origen = new google.maps.LatLng(estadoLat, estadoLon);
      var destino = new google.maps.LatLng(destinoLat, destinoLon);

      service.getDistanceMatrix(
          {
            origins: [origen],
            destinations: [destino],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
          }, callback);
    }

    function callback(response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {

        var distance = response.rows[0].elements[0].distance.text;
        var time = response.rows[0].elements[0].duration.value / 60;

        fechaEstimadaLlegada = addMinutes(fechaActual, time).toISOString();

        //Ext.getCmp("gridPanelDestinos").getView().refresh()
      }
    }

    Date.prototype.addHours = function (h) {
      this.setTime(this.getTime() + (h * 60 * 60 * 1000));
      return this;
    }

    function addMinutes(date, minutes) {
      return new Date(date.getTime() + minutes * 60000);
    }

    var renderIconVentana = function (val) {
      if (val == 1) {
        return '<img data-qtip="Llegará en ventana horaria." src="Images/status_green_16x16.png">';
      }
      if (val == 0) {
        return '<img data-qtip="No llegará en ventana horaria." src="Images/status_red_16x16.png">';
      }

    };
      
    function DeleteViaje(nroTransporte, codLocal) {
        if (confirm("El viaje se eliminará permanentemente.¿Desea continuar?")) {

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxViajes.aspx?Metodo=EliminarViaje',
                params: {
                    'nroTransporte': nroTransporte,
                    'idEmbarque': nroTransporte,
                    'codLocal': codLocal
                },
                success: function (data, success) {
                    alert(data.responseText);
                    Ext.getCmp('gridViajesAsignados').getStore().load();
                },
                failure: function (msg) {
                    alert('Se ha producido un error.');
                }
            });
        }
    }

  </script>
    <!--
    <script type="text/javascript">
        //CODIGO PARA EVITAR QUE EL MENU DE VIAJES SE OCULTE
        $(document).ready(function () {
        
            $("#btnMenuViajes").on("click", function () {
                $("#menuViajes").attr("style","position: absolute; left: 293px; top: 39px; z-index: 19011; width: 150px;height: 110px;");
                $("#menuViajesConfigViajes_v2").attr("style", "margin: 0px; width: 144px; height: 27px; left: 0px; top: 78px;");
                $("#menuViajes").children().attr("style", "height: 181px;");
                $('#menuViajes').find('.x-vertical-box-overflow-body:first').attr("style", "height: 181px;");
                $("#menuViajesConfigViajes_v2").children().attr("style", "height: 181px;");
            });
            
        });
        //ESTO OCURRE PORQUE LOS ESTILOS CSS QUE CARGA ESTA SECCION ENTRAN EN CONFLICTO CON LOS DEL MENU
    </script>
    -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
