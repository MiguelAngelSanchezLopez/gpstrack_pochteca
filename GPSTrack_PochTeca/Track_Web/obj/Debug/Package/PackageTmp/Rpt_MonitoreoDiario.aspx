﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rpt_MonitoreoDiario.aspx.cs" Inherits="Track_Web.Rpt_MonitoreoDiario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

  <script type="text/javascript">

    Ext.onReady(function () {

      Ext.QuickTips.init();
      Ext.Ajax.timeout = 600000;
      Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
      Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
      Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });
      //Ext.form.Field.prototype.msgTarget = 'side';
      //if (Ext.isIE) { Ext.enableGarbageCollector = false; }

      var dateDesde = new Ext.form.DateField({
        id: 'dateDesde',
        fieldLabel: 'Desde',
        labelWidth: 80,
        allowBlank: false,
        anchor: '99%',
        format: 'd-m-Y',
        editable: false,
        value: new Date(),
        maxValue: new Date(),
        style: {
          marginLeft: '5px'
        }
      });

      var dateHasta = new Ext.form.DateField({
        id: 'dateHasta',
        fieldLabel: 'Hasta',
        labelWidth: 80,
        allowBlank: false,
        anchor: '99%',
        format: 'd-m-Y',
        editable: false,
        value: new Date(),
        minValue: Ext.getCmp('dateDesde').getValue(),
        maxValue: new Date(),
        style: {
          marginLeft: '5px'
        }
      });

      dateDesde.on('change', function () {
        var _desde = Ext.getCmp('dateDesde');
        var _hasta = Ext.getCmp('dateHasta');

        _hasta.setMinValue(_desde.getValue());
        _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
        _hasta.validate();
      });

      dateHasta.on('change', function () {
        var _desde = Ext.getCmp('dateDesde');
        var _hasta = Ext.getCmp('dateHasta');

        _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
        //_desde.setMaxValue(_hasta.getValue());
        _desde.validate();
      });

      Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
      Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

      var btnBuscar = {
        id: 'btnBuscar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/searchreport_black_20x20.png',
        text: 'Buscar',
        width: 90,
        height: 26,
        handler: function () {
          Buscar();
        }
      };

      var btnExportar = {
        id: 'btnExportar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/export_black_20x20.png',
        text: 'Exportar',
        width: 90,
        height: 26,
        listeners: {
          click: {
            element: 'el',
            fn: function () {

              var desde = Ext.getCmp('dateDesde').getRawValue();
              var hasta = Ext.getCmp('dateHasta').getRawValue();

              var mapForm = document.createElement("form");
              mapForm.target = "ToExcel";
              mapForm.method = "POST"; // or "post" if appropriate
              mapForm.action = 'Rpt_MonitoreoDiario.aspx?Metodo=ExportExcel';

              //
              var _desde = document.createElement("input");
              _desde.type = "text";
              _desde.name = "desde";
              _desde.value = desde;
              mapForm.appendChild(_desde);

              var _hasta = document.createElement("input");
              _hasta.type = "text";
              _hasta.name = "hasta";
              _hasta.value = hasta;
              mapForm.appendChild(_hasta);

              document.body.appendChild(mapForm);
              mapForm.submit();

            }
          }
        }
      };

      var panelFilters = new Ext.FormPanel({
        id: 'panelFilters',
        title: 'Filtros Reporte',
        anchor: '100% 100%',
        bodyStyle: 'padding: 5px;',
        layout: 'anchor',
        items: [{
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 1,
          items: [dateDesde]
        }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 1,
          items: [dateHasta]
        }],
        buttons: [btnExportar, btnBuscar]
      });

      var storeReporte = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['Carga',
                  'NroTransporte',
                  'CodigoInterno',
                  'NombreLocal',
                  'OrdenEntrega',
                  'NombreTransportista',
                  'HoraDespacho',
                  'VentanaHoraria',
                  'Plataforma',
                  'Visualizacion',
                  'FechaDespacho',
                  'HoraDespachoReal',
                  'FechaEntregaLocal',
                  'HoraEntregaLocal',
                  'CumpleVentana',
                  'CumpleDespacho'
        ],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetRpt_MonitoreoDiario',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var gridPanelReporte = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelReporte',
        title: 'Reporte Monitoreo Diario',
        store: storeReporte,
        anchor: '100% 100%',
        columnLines: true,
        scroll: false,
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columns: [
                    { text: 'CD', sortable: true, width: 60, dataIndex: 'Carga' },
                    { text: 'Transporte', sortable: true, width: 65, dataIndex: 'NroTransporte' },
                    { text: 'Local', sortable: true, width: 45, dataIndex: 'CodigoInterno' },
                    { text: 'Nombre Local', sortable: true, flex: 1, dataIndex: 'NombreLocal' },
                    { text: 'Secuen.', sortable: true, width: 50, dataIndex: 'OrdenEntrega' },
                    { text: 'Transportista', sortable: true, width: 80, dataIndex: 'NombreTransportista' },
                    { text: 'Hora Despacho', sortable: true, width: 85, dataIndex: 'HoraDespacho' },
                    { text: 'Ventana Horaria', sortable: true, width: 90, dataIndex: 'VentanaHoraria' },
                    { text: 'Plataforma', sortable: true, width: 90, dataIndex: 'Plataforma' },
                    { text: 'Visualiz.', sortable: true, width: 60, dataIndex: 'Visualizacion' },
                    { text: 'Fecha Despacho', sortable: true, width: 95, dataIndex: 'FechaDespacho' },
                    { text: 'Hora Despacho Real', sortable: true, width: 115, dataIndex: 'HoraDespachoReal' },
                    { text: 'Fecha Entrega', sortable: true, width: 80, dataIndex: 'FechaEntregaLocal' },
                    { text: 'Hora Entrega', sortable: true, width: 80, dataIndex: 'HoraEntregaLocal' },
                    { text: 'Cumple Ventana', sortable: true, width: 90, dataIndex: 'CumpleVentana' },
                    { text: 'Cumple Despacho', sortable: true, width: 90, dataIndex: 'CumpleDespacho' }
        ]

      });

      var leftPanel = new Ext.FormPanel({
        id: 'leftPanel',
        region: 'west',
        border: true,
        margins: '0 0 3 3',
        width: 250,
        minWidth: 200,
        maxWidth: 400,
        layout: 'anchor',
        collapsible: true,
        titleCollapsed: false,
        split: true,
        items: [panelFilters]
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        items: [gridPanelReporte]
      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, leftPanel, centerPanel]
      });
    });

  </script>


  <script type="text/javascript">

    function Buscar() {
      if (!Ext.getCmp('leftPanel').getForm().isValid()) {
        return;
      }

      var desde = Ext.getCmp('dateDesde').getValue();
      var hasta = Ext.getCmp('dateHasta').getValue();

      var store = Ext.getCmp('gridPanelReporte').store;
      store.load({
        params: {
          desde: desde,
          hasta: hasta
        },
        callback: function (r, options, success) {
          if (!success) {
            Ext.MessageBox.show({
              title: 'Error',
              msg: 'Se ha producido un error.',
              buttons: Ext.MessageBox.OK
            });
          }
        }
      });
    }

  </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
