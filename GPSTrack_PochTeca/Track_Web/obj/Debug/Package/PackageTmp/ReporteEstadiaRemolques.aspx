﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteEstadiaRemolques.aspx.cs" Inherits="Track_Web.ReporteEstadiaRemolques" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    AltoTrack Platform 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
    <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>
    <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

    <script type="text/javascript">

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.Ajax.timeout = 600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            var storeFiltroPatente = new Ext.data.JsonStore({
                fields: ['Patente'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroPatente = new Ext.form.field.ComboBox({
                id: 'comboFiltroPatente',
                fieldLabel: 'Placa',
                forceSelection: true,
                store: storeFiltroPatente,
                valueField: 'Patente',
                displayField: 'Patente',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                forceSelection: true,
                disabled: false
            });


            var storeFiltroTransportista = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Transportista'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroTransportista = new Ext.form.field.ComboBox({
                id: 'comboFiltroTransportista',
                fieldLabel: 'Línea Transporte',
                labelWidth: 100,
                forceSelection: true,
                store: storeFiltroTransportista,
                valueField: 'Transportista',
                displayField: 'Transportista',
                queryMode: 'local',
                anchor: '99%',
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                forceSelection: true,
                multiSelect: true,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (field, newVal) {
                        FiltrarPatentes();
                    }
                }
            });

            Ext.getCmp('comboFiltroTransportista').store.load({
                callback: function (r, options, success) {
                    if (success) {
                        var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.getAt(0).get("Transportista");
                        Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);
                    }
                }
            });


            var btnBuscar = {
                id: 'btnBuscar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/searchreport_black_20x20.png',
                text: 'Buscar',
                width: 90,
                height: 26,
                handler: function () {
                    Buscar();
                }
            };

            var btnExportar = {
                id: 'btnExportar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/export_black_20x20.png',
                text: 'Exportar',
                width: 90,
                height: 26,
                listeners: {
                    click: {
                        element: 'el',
                        fn: function () {

                            var mapForm = document.createElement("form");
                            mapForm.target = "ToExcel";
                            mapForm.method = "POST"; // or "post" if appropriate
                            mapForm.action = 'ReporteEstadiaRemolques.aspx?Metodo=ExportExcel';

                            document.body.appendChild(mapForm);
                            mapForm.submit();

                        }
                    }
                }
            };

            var displayEstado = new Ext.form.field.Display({
                id: 'displayEstado',
                labelWidth: 150,
                value: '',
                width: 500,
                style: {
                    marginTop: '4px',
                    marginLeft: '5px'
                },
                labelStyle: 'font-size: large'
            });

            var panelFilters = new Ext.FormPanel({
                id: 'panelFilters',
                title: 'Filtros Reporte',
                anchor: '100% 100%',
                bodyStyle: 'padding: 5px;',
                layout: 'anchor',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroTransportista, comboFiltroPatente, displayEstado]
                }],
                buttons: [btnExportar, btnBuscar]
            });

            
            var habilitarCampoReferencia = false;
            var listaEncabezadosCamposReporte = [];

            function AsignarEncabezadosCamposReporte() {
                listaEncabezadosCamposReporte.push('Placa');
                listaEncabezadosCamposReporte.push('Transportista');
                listaEncabezadosCamposReporte.push('TipoUnidad');
                listaEncabezadosCamposReporte.push('EstadoEventos');
                listaEncabezadosCamposReporte.push({ name: 'FechaEvento', type: 'date', dateFormat: 'c' });
                listaEncabezadosCamposReporte.push('FechaReporte');
                listaEncabezadosCamposReporte.push('DesfaseReporte');
                listaEncabezadosCamposReporte.push('Antiguedad');
                listaEncabezadosCamposReporte.push('Velocidad');
                listaEncabezadosCamposReporte.push('Latitud');
                listaEncabezadosCamposReporte.push('Longitud');

                if (habilitarCampoReferencia)
                    listaEncabezadosCamposReporte.push('Referencia');

                listaEncabezadosCamposReporte.push('Detenido');
                listaEncabezadosCamposReporte.push('TiempoDetenido');
                listaEncabezadosCamposReporte.push('FechaDetencion');
                listaEncabezadosCamposReporte.push('HorasDetenido');
                listaEncabezadosCamposReporte.push('UltimaEntrada');
                listaEncabezadosCamposReporte.push('UltimaSalida');
                listaEncabezadosCamposReporte.push('UltimaZona');
                listaEncabezadosCamposReporte.push('EnZona');
                listaEncabezadosCamposReporte.push('Zona');
                listaEncabezadosCamposReporte.push('NombreZona');
                listaEncabezadosCamposReporte.push('Formato');
                listaEncabezadosCamposReporte.push('TiempoEnZona');
                listaEncabezadosCamposReporte.push('CodZonaCercana');
                listaEncabezadosCamposReporte.push('NombreZonaCercana');
                listaEncabezadosCamposReporte.push('DistanciaZonaCercana');
                listaEncabezadosCamposReporte.push('CEDIS');
                listaEncabezadosCamposReporte.push('NombreCEDIS');
                listaEncabezadosCamposReporte.push('EstadoCarga');
                listaEncabezadosCamposReporte.push('EstadoViaje');
                
            }

            AsignarEncabezadosCamposReporte();

            var storeReporte = new Ext.data.JsonStore({
                autoLoad: false,
                fields:listaEncabezadosCamposReporte,
                groupField: 'Transportista',
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetReporteEstadiaRemolques',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            
            var listaCamposReporte = [];

            function AsignarCamposReporte() {
                listaCamposReporte.push({ text: 'Placa', sortable: true, width: 60, dataIndex: 'Placa' });
                listaCamposReporte.push({ text: 'Línea', sortable: true, width: 80, dataIndex: 'Transportista' });
                listaCamposReporte.push({ text: 'Tipo Unidad', sortable: true, width: 80, dataIndex: 'Transportista' });
                listaCamposReporte.push({ text: 'Estado', sortable: true, width: 60, dataIndex: 'EstadoEventos' });
                listaCamposReporte.push({ text: 'Fecha GPS', sortable: true, width: 60, dataIndex: 'FechaEvento', renderer: Ext.util.Format.dateRenderer('d-m-Y') });
                listaCamposReporte.push({ text: 'Fecha Transmisión', sortable: true, width: 60, dataIndex: 'FechaReporte', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') });
                listaCamposReporte.push({ text: 'Desfase(min)', sortable: true, width: 60, dataIndex: 'DesfaseReporte' });
                listaCamposReporte.push({ text: 'T. última señal (d-h-m)', sortable: true, width: 105, dataIndex: 'Antiguedad' });
                listaCamposReporte.push({ text: 'Vel.', sortable: true, width: 30, dataIndex: 'Velocidad' });
                listaCamposReporte.push({ text: 'Latitud', sortable: true, width: 60, dataIndex: 'Latitud' });
                listaCamposReporte.push({ text: 'Longitud', sortable: true, width: 60, dataIndex: 'Longitud' });
                
                if(habilitarCampoReferencia)
                    listaCamposReporte.push({ text: 'Referencia', sortable: true, flex: 1, dataIndex: 'Referencia' });

                listaCamposReporte.push({ text: 'Detenido', sortable: true, width: 60, dataIndex: 'Detenido' });
                listaCamposReporte.push({ text: 'Fec. Detencion', sortable: true, width: 85, dataIndex: 'FechaDetencion' });
                listaCamposReporte.push({ text: 'T. Detenido', sortable: true, width: 75, dataIndex: 'TiempoDetenido' });
                listaCamposReporte.push({ text: 'Horas Detenido', sortable: true, width: 75, dataIndex: 'HorasDetenido' });
                listaCamposReporte.push({ text: 'Ult. Entrada', sortable: true, width: 80, dataIndex: 'UltimaEntrada' });
                listaCamposReporte.push({ text: 'Ult. Salida', sortable: true, width: 80, dataIndex: 'UltimaSalida' });
                listaCamposReporte.push({ text: 'Ult. Zona', sortable: true, width: 80, dataIndex: 'UltimaZona' });
                listaCamposReporte.push({ text: 'En Zona', sortable: true, width: 60, dataIndex: 'EnZona' });
                listaCamposReporte.push({ text: 'Zona', sortable: true, width: 100, dataIndex: 'NombreZona' });
                listaCamposReporte.push({ text: 'T. En Zona', sortable: true, width: 70, dataIndex: 'TiempoEnZona' });
                listaCamposReporte.push({ text: 'Zona cercana', sortable: true, width: 80, dataIndex: 'NombreZonaCercana' });
                listaCamposReporte.push({ text: 'Dist. zona cercana', sortable: true, width: 90, dataIndex: 'DistanciaZonaCercana' });
                listaCamposReporte.push({ text: 'CEDIS', sortable: true, width: 90, dataIndex: 'NombreCEDIS' });
                
            }

            AsignarCamposReporte();

            var gridPanelReporte = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelReporte',
                title: 'Reporte Estadia Remolques',
                store: storeReporte,
                anchor: '100% 100%',
                columnLines: true,
                scroll: true,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                features: [{
                    ftype: 'groupingsummary',
                    groupHeaderTpl: '{name}'
                }],
                columns:listaCamposReporte
            });

            var leftPanel = new Ext.FormPanel({
                id: 'leftPanel',
                region: 'west',
                border: true,
                margins: '0 0 3 3',
                width: 300,
                minWidth: 200,
                maxWidth: 400,
                layout: 'anchor',
                collapsible: true,
                titleCollapsed: false,
                split: true,
                items: [panelFilters]
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 3 3 0',
                anchor: '100% 100%',
                items: [gridPanelReporte]
            });

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, leftPanel, centerPanel]
            });
        });

    </script>


    <script type="text/javascript">

        function Buscar() {

            var patente = Ext.getCmp('comboFiltroPatente').getValue();
            var listCarrier = Ext.getCmp('comboFiltroTransportista').getValue();

            if (listCarrier === null || listCarrier === "") {
                listCarrier = ""
            }

            var store = Ext.getCmp('gridPanelReporte').store;
            store.load({
                params: {
                    patente: patente,
                    listCarrier: listCarrier
                },
                callback: function (r, options, success) {
                    if (!success) {
                        Ext.MessageBox.show({
                            title: 'Error',
                            msg: 'Se ha producido un error.',
                            buttons: Ext.MessageBox.OK
                        });
                    }
                }
            });
        }

        function FiltrarPatentes() {
            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

            var store = Ext.getCmp('comboFiltroPatente').store;
            store.load({
                params: {
                    transportista: transportista
                },
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp('comboFiltroPatente').store.insert(0, [{ Placa: "Todas" }]);
                        Ext.getCmp("comboFiltroPatente").setValue("Todas");
                    }
                }
            });
        }

    </script>
</asp:Content>
