﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rpt_Temperatura.aspx.cs" Inherits="Track_Web.Rpt_Temperatura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

<script type="text/javascript">

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });
        //Ext.form.Field.prototype.msgTarget = 'side';
        //if (Ext.isIE) { Ext.enableGarbageCollector = false; }
        
        var chkNroTransporte = new Ext.form.Checkbox({
            id: 'chkNroTransporte',
            labelSeparator: '',
            hideLabel: true,
            checked: true,
            style: {
                marginTop: '7px',
                marginLeft: '5px'
            },
            listeners: {
                change: function (cb, checked) {
                    if (checked == true) {
                        Ext.getCmp("textFiltroNroTransporte").setDisabled(false);
                        Ext.getCmp("dateDesde").setDisabled(true);
                        Ext.getCmp("dateHasta").setDisabled(true);
                        Ext.getCmp("hourDesde").setDisabled(true);
                        Ext.getCmp("hourHasta").setDisabled(true);
                        Ext.getCmp("comboFiltroTransportista").setDisabled(true);
                        Ext.getCmp("comboFiltroPatente").setDisabled(true);
                    }
                    else {
                        Ext.getCmp("textFiltroNroTransporte").setDisabled(true);
                        Ext.getCmp("dateDesde").setDisabled(false);
                        Ext.getCmp("dateHasta").setDisabled(false);
                        Ext.getCmp("hourDesde").setDisabled(false);
                        Ext.getCmp("hourHasta").setDisabled(false);
                        Ext.getCmp("comboFiltroTransportista").setDisabled(false);
                        Ext.getCmp("comboFiltroPatente").setDisabled(false);
                        Ext.getCmp('textFiltroNroTransporte').reset();
                    }
                }
            }
        });

        var textFiltroNroTransporte = new Ext.form.TextField({
            id: 'textFiltroNroTransporte',
            fieldLabel: 'ID Embarque',
            anchor: '99%',
            labelWidth: 80,
            maxLength: 20,
            style: {
                marginTop: '5px',
                marginLeft: '5px'
            },
            disabled: false,
        });

        var dateDesde = new Ext.form.DateField({
            id: 'dateDesde',
            fieldLabel: 'Desde',
            labelWidth: 100,
            allowBlank: false,
            anchor: '99%',
            format: 'd-m-Y',
            editable: false,
            value: new Date(),
            maxValue: new Date(),
            style: {
                marginLeft: '5px'
            },
            disabled: true
        });

        dateDesde.on('change', function () {
            var _desde = Ext.getCmp('dateDesde');
            var _hasta = Ext.getCmp('dateHasta');

            _hasta.setMinValue(_desde.getValue());
            _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 2));
            _hasta.validate();
        });

        var hourDesde = {
            xtype: 'timefield',
            id: 'hourDesde',
            allowBlank: false,
            format: 'H:i',
            minValue: '00:00',
            maxValue: '23:59',
            increment: 10,
            anchor: '99%',
            editable: true,
            value: '00:00',
            disabled: true
        };

        var dateHasta = new Ext.form.DateField({
            id: 'dateHasta',
            fieldLabel: 'Hasta',
            labelWidth: 100,
            allowBlank: false,
            anchor: '99%',
            format: 'd-m-Y',
            editable: false,
            value: new Date(),
            minValue: Ext.getCmp('dateDesde').getValue(),
            maxValue: new Date(),
            style: {
                marginLeft: '5px'
            },
            disabled: true
        });


        dateHasta.on('change', function () {
            var _desde = Ext.getCmp('dateDesde');
            var _hasta = Ext.getCmp('dateHasta');

            _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
            //_desde.setMaxValue(_hasta.getValue());
            _desde.validate();
        });

        var hourHasta = {
            xtype: 'timefield',
            id: 'hourHasta',
            allowBlank: false,
            format: 'H:i',
            minValue: '00:00',
            maxValue: '23:59',
            increment: 10,
            anchor: '99%',
            editable: true,
            value: new Date(),
            disabled: true
        };

        Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
        Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

        var storeFiltroPatente = new Ext.data.JsonStore({
            fields: ['Patente'],
            proxy: new Ext.data.HttpProxy({
                //url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPatentesRuta&Todos=True',
                url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=False',
                headers: {
                    'Content-type': 'application/json'
                }
            })
        });

        var comboFiltroPatente = new Ext.form.field.ComboBox({
            id: 'comboFiltroPatente',
            fieldLabel: 'Tracto',
            forceSelection: true,
            store: storeFiltroPatente,
            valueField: 'Patente',
            displayField: 'Patente',
            queryMode: 'local',
            anchor: '99%',
            labelWidth: 100,
            style: {
                marginLeft: '5px'
            },
            emptyText: 'Seleccione...',
            enableKeyEvents: true,
            editable: true,
            forceSelection: true,
            disabled: true
        });

        var storeFiltroTransportista = new Ext.data.JsonStore({
            fields: ['Transportista'],
            proxy: new Ext.data.HttpProxy({
                url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
                headers: {
                    'Content-type': 'application/json'
                }
            })
        });

        var comboFiltroTransportista = new Ext.form.field.ComboBox({
            id: 'comboFiltroTransportista',
            fieldLabel: 'Línea Transporte',
            forceSelection: true,
            store: storeFiltroTransportista,
            valueField: 'Transportista',
            displayField: 'Transportista',
            queryMode: 'local',
            anchor: '99%',
            labelWidth: 100,
            style: {
                marginLeft: '5px'
            },
            emptyText: 'Seleccione...',
            enableKeyEvents: true,
            editable: false,
            forceSelection: true,
            listeners: {
                select: function () {
                    FiltrarPatentes();
                }
            },
            disabled: true
        });

        storeFiltroTransportista.load({
            callback: function (r, options, success) {
                if (success) {

                    var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.getAt(0).get("Transportista");
                    Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);

                    storeFiltroPatente.load({
                        callback: function (r, options, success) {
                            if (success) {

                                FiltrarPatentes();

                            }
                        }
                    })

                }
            }
        })

        var btnBuscar = {
            id: 'btnBuscar',
            xtype: 'button',
            iconAlign: 'left',
            icon: 'Images/searchreport_black_20x20.png',
            text: 'Buscar',
            width: 90,
            height: 26,
            handler: function () {
                Buscar();
            }
        };


        var panelFilters = new Ext.FormPanel({
            id: 'panelFilters',
            title: 'Filtros Reporte',
            anchor: '100% 100%',
            bodyStyle: 'padding: 5px;',
            layout: 'column',
            items: [{
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.1,
                items: [chkNroTransporte]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.9,
                items: [textFiltroNroTransporte]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.78,
                items: [dateDesde, dateHasta]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.22,
                items: [hourDesde, hourHasta]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 1,
                items: [comboFiltroTransportista]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 1,
                items: [comboFiltroPatente]
            }],
            buttons: [btnBuscar]
        });

        var storeGraphTemperatura = new Ext.data.JsonStore({
            //autoLoad: false,
            fields: ['Fecha',
                    'FechaText',   
                    'Temperatura1',
                    'Temperatura2',
                    'LineaCentral',
                    'ZonaAlta',
                    'ZonaBaja'

                    ],
            proxy: new Ext.data.HttpProxy({
                url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetRpt_Temperatura',
                reader: { type: 'json', root: 'Zonas' },
                headers: {
                    'Content-type': 'application/json'
                }
            }),
            data: [ { FechaText: '00:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '01:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '02:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '03:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '04:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '05:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '06:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '07:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '08:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '09:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '10:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '11:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                    { FechaText: '12:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 }]
        });

        var graphTemperatura = new Ext.chart.Chart({
            id: 'graphTemperatura',
            anchor: '100% 100%',
            animate: true,
            shadow: true,
            store: storeGraphTemperatura,
            legend: {
                position: 'right'
            },
            /*style: {
                marginLeft: '30px'
            },*/
            axes: [{
                type: 'Numeric',
                minimum: -25,
                maximum: 25,
                position: 'left',
                fields: ['Temperatura1'],
                title: true,
                grid: true,
                label: {
                    renderer: Ext.util.Format.numberRenderer('0,0'),
                    font: '9px Arial',

                },
                roundToDecimal: false
            }, {
                type: 'Category',
                position: 'bottom',
                fields: ['FechaText'],
                //title: ['Detención', 'Pérdida señal'],
                grid: true
                /*label: {
                    font: '9px Arial',
                    renderer: function (name) {
                        return name.substr(0, 3);
                    }
                }*/
            }],
            series: [{
                type: 'line',
                axis: 'left',
                //smooth: true,
                highlight: false,
                xField: 'FechaText',
                yField: ['Temperatura1'],
                markerConfig: {
                    type: 'circle',
                    size: 3,
                    radius: 3,
                    'stroke-width': 0
                },
                tips: {
                    trackMouse: true,
                    width: 140,
                    height: 45,
                    renderer: function (storeItem, item) {
                        this.setTitle('Fecha: ' + storeItem.get('Fecha') + ' Temperatura: ' + storeItem.get('Temperatura1'));
                    }
                },
            }, {
                type: 'line',
                axis: 'left',
                //smooth: true,
                highlight: false,
                xField: 'FechaText',
                yField: ['Temperatura2'],
                markerConfig: {
                    type: 'circle',
                    size: 3,
                    radius: 3,
                    'stroke-width': 0
                },
                tips: {
                    trackMouse: true,
                    width: 140,
                    height: 45,
                    renderer: function (storeItem, item) {
                        this.setTitle('Fecha: ' + storeItem.get('Fecha') + ' Temp. ' + storeItem.get('Temperatura2'));
                    }
                },
            }, {
                type: 'line',
                axis: 'left',
                highlight: false,
                xField: 'FechaText',
                yField: ['LineaCentral'],
                markerConfig: {
                    type: 'circle',
                    size: 0,
                    radius: 0,
                    color: 'red',
                    'stroke-width': 0
                },
                style: {
                    fill: "#ef4038",
                    stroke: "#ef4038"
                },
            }, {
                type: 'line',
                axis: 'left',
                highlight: false,
                xField: 'FechaText',
                yField: ['ZonaAlta'],
                markerConfig: {
                    type: 'circle',
                    size: 0,
                    radius: 0,
                    'stroke-width': 0
                },
                style: {
                    fill: "#f3cede",
                    stroke: "#f3cede",
                    'stroke-width': 3 
                },
            }, {
                type: 'line',
                axis: 'left',
                highlight: false,
                xField: 'FechaText',
                yField: ['ZonaBaja'],
                markerConfig: {
                    type: 'circle',
                    size: 0,
                    radius: 0,
                    'stroke-width': 0
                },
                style: {
                    fill: "#38e7ef",
                    stroke: "#38e7ef",
                    'stroke-width': 3
                },
            }
            ]
        });

        var panGraphTemperatura = new Ext.FormPanel({
            id: 'panGraphTemperatura',
            anchor: '100% 100%',
            title: 'Gráfico de temperatura',
            //renderTo: Ext.getBody(),
            layout: 'fit',
            flex: 1,
            items: [graphTemperatura]
        });

        var leftPanel = new Ext.FormPanel({
            id: 'leftPanel',
            region: 'west',
            border: true,
            margins: '0 0 3 3',
            width: 275,
            minWidth: 200,
            maxWidth: 350,
            layout: 'anchor',
            collapsible: true,
            titleCollapsed: false,
            split: true,
            items: [panelFilters]
        });

        var centerPanel = new Ext.FormPanel({
            id: 'centerPanel',
            region: 'center',
            border: true,
            margins: '0 3 3 0',
            anchor: '100% 100%',
            items: [panGraphTemperatura]
        });

        var viewport = Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [topMenu, leftPanel, centerPanel]
        });
    });

</script>



<script type="text/javascript">

    function FiltrarPatentes() {
        var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

        var store = Ext.getCmp('comboFiltroPatente').store;
        store.load({
            params: {
                transportista: transportista
            }
        });
    }

    function Buscar() {
        if (!Ext.getCmp('leftPanel').getForm().isValid()) {
            return;
        }
        
        //Ext.Msg.wait('Espere por favor...', 'Generando gráfico');

        var myMask = new Ext.LoadMask(Ext.getBody(), { msg: "Cargando..." });
        myMask.show();

        var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();
        var desde = Ext.getCmp('dateDesde').getRawValue() + " " + Ext.getCmp('hourDesde').getRawValue();
        var hasta = Ext.getCmp('dateHasta').getRawValue() + " " + Ext.getCmp('hourHasta').getRawValue();
        var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
        var patente = Ext.getCmp('comboFiltroPatente').getValue();

        var store = Ext.getCmp('graphTemperatura').store;
        store.removeAll();

        store.load({
            params: {
                nroTransporte: nroTransporte,
                desde: desde,
                hasta: hasta,
                transportista: transportista,
                patente: patente
            },
            callback: function (r, options, success) {

                if (Ext.getCmp('graphTemperatura').store.count() == 0)
                {
                    Ext.getCmp('graphTemperatura').surface.removeAll();

                    store.add({ FechaText: '00:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '01:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '02:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '03:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '04:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '05:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '06:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '07:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '08:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '09:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '10:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '11:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 },
                                { FechaText: '12:00', LineaCentral: 0, ZonaAlta: 15, ZonaBaja: -15 });

                }

                myMask.hide();
            }

        });


    }

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
