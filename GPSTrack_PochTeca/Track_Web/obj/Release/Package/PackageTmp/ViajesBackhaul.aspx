﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViajesBackhaul.aspx.cs" Inherits="Track_Web.ViajesBackhaul" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
  <script src="Scripts/RowExpander.js" type="text/javascript"></script>
  
<script type="text/javascript">

  var geoLayer = new Array();
  var arrayLayerLocales = new Array();
  var arrayLabelLocales = new Array();
  var arrayPositions = new Array();
  var arrayAlerts = new Array();
  var trafficLayer = new google.maps.TrafficLayer();
  var infowindow = new google.maps.InfoWindow();
  var arrayHouses = new Array();
  var arrayLocalesBackhaul = new Array();
  var arrayCamionesBackhaul = new Array();
  var nroTransporte;
  var idEmbarque;
  var transportista;
  var patenteTracto;
  var patenteTrailer;
  var _editing = true;
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  Ext.onReady(function () {

    Ext.QuickTips.init();
    Ext.Ajax.timeout = 600000;
    Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
    Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
      Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });
      
        var chkNroTransporte = new Ext.form.Checkbox({
            id: 'chkNroTransporte',
            labelSeparator: '',
            hideLabel: true,
            checked: false,
            style: {
                marginTop: '7px',
                marginLeft: '5px'
            },
            listeners: {
                change: function (cb, checked) {
                    if (checked == true) {
                        Ext.getCmp("textFiltroNroTransporte").setDisabled(false);
                        Ext.getCmp("dateDesde").setDisabled(true);
                        Ext.getCmp("dateHasta").setDisabled(true);
                        Ext.getCmp("comboFiltroTransportista").setDisabled(true);
                        Ext.getCmp("comboFiltroPatente").setDisabled(true);
                        Ext.getCmp("comboFiltroEstadoViaje").setDisabled(true);
                        Ext.getCmp("comboFiltroLocales").setDisabled(true);
                    
                    }
                    else {
                        Ext.getCmp("textFiltroNroTransporte").setDisabled(true);
                        Ext.getCmp("dateDesde").setDisabled(false);
                        Ext.getCmp("dateHasta").setDisabled(false);
                        Ext.getCmp("comboFiltroTransportista").setDisabled(false);
                        Ext.getCmp("comboFiltroPatente").setDisabled(false);
                        Ext.getCmp("comboFiltroEstadoViaje").setDisabled(false);
                        Ext.getCmp('textFiltroNroTransporte').reset();
                        Ext.getCmp("comboFiltroLocales").setDisabled(false);
                    }
                }
            }
        });
    
    var textFiltroNroTransporte = new Ext.form.TextField({
        id: 'textFiltroNroTransporte',
        fieldLabel: 'ID Master',
        anchor: '99%',
        labelWidth: 80,
        maxLength: 20,
        style: {
            marginTop: '5px',
            marginLeft: '5px'
        },
        disabled: true,
    });

    var dateDesde = new Ext.form.DateField({
      id: 'dateDesde',
      fieldLabel: 'Desde',
      labelWidth: 110,
      allowBlank: false,
      anchor: '99%',
      format: 'd-m-Y',
      editable: false,
      value: new Date(),
      maxValue: new Date(),
      style: {
        marginLeft: '5px'
      },
      disabled: false 
    });

    var dateHasta = new Ext.form.DateField({
      id: 'dateHasta',
      fieldLabel: 'Hasta',
      labelWidth: 80,
      allowBlank: false,
      anchor: '99%',
      format: 'd-m-Y',
      editable: false,
      value: new Date(),
      minValue: Ext.getCmp('dateDesde').getValue(),
      maxValue: new Date(),
      style: {
        marginLeft: '5px'
      },
      disabled: false
    });

    dateDesde.on('change', function () {
      var _desde = Ext.getCmp('dateDesde');
      var _hasta = Ext.getCmp('dateHasta');

      _hasta.setMinValue(_desde.getValue());
      _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
      _hasta.validate();
    });

    dateHasta.on('change', function () {
      var _desde = Ext.getCmp('dateDesde');
      var _hasta = Ext.getCmp('dateHasta');

      _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
      _desde.validate();
    });

    Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
    Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

    var storeFiltroPatente = new Ext.data.JsonStore({
      fields: ['Patente'],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=True',
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

    var comboFiltroPatente = new Ext.form.field.ComboBox({
      id: 'comboFiltroPatente',
      fieldLabel: 'Placa',
      forceSelection: true,
      store: storeFiltroPatente,
      valueField: 'Patente',
      displayField: 'Patente',
      queryMode: 'local',
      anchor: '99%',
      labelWidth: 80,
      style: {
        marginTop: '5px',
        marginLeft: '5px'
      },
      emptyText: 'Seleccione...',
      enableKeyEvents: true,
      editable: true,
      forceSelection: true,
      disabled: false
    });

    var storeFiltroTransportista = new Ext.data.JsonStore({
      fields: ['Transportista'],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

    var comboFiltroTransportista = new Ext.form.field.ComboBox({
      id: 'comboFiltroTransportista',
      fieldLabel: 'Línea Transporte',
      forceSelection: true,
      store: storeFiltroTransportista,
      valueField: 'Transportista',
      displayField: 'Transportista',
      queryMode: 'local',
      anchor: '99%',
      labelWidth: 110,
      style: {
        marginTop: '5px',
        marginLeft: '5px'
      },
      emptyText: 'Seleccione...',
      enableKeyEvents: true,
      editable: false,
      forceSelection: true,
      disabled: false
    });

    var storeFiltroEstadoViaje = new Ext.data.JsonStore({
      fields: ['EstadoViaje'],
      data: [{ "EstadoViaje": "Todos" },
              { "EstadoViaje": "Asignado" },
              { "EstadoViaje": "En Ruta" },
              { "EstadoViaje": "En Local" },
              { "EstadoViaje": "Finalizado" }
            ]
    });

    var comboFiltroEstadoViaje = new Ext.form.field.ComboBox({
      id: 'comboFiltroEstadoViaje',
      fieldLabel: 'Estado',
      store: storeFiltroEstadoViaje,
      valueField: 'EstadoViaje',
      displayField: 'EstadoViaje',
      queryMode: 'local',
      anchor: '99%',
      labelWidth: 80,
      editable: false,
      style: {
        marginTop: '5px',
        marginLeft: '5px'
      },
      emptyText: 'Seleccione...',
      enableKeyEvents: true,
      forceSelection: true,
      disabled: false
    });

    Ext.getCmp('comboFiltroEstadoViaje').setValue('Todos');
      
    var storeFiltroProveedorGPS = new Ext.data.JsonStore({
        fields: ['ProveedorGPS'],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetProveedoresGPS&Todos=True',
            headers: {
                'Content-type': 'application/json'
            }
        })
    });

    var comboFiltroProveedorGPS = new Ext.form.field.ComboBox({
        id: 'comboFiltroProveedorGPS',
        fieldLabel: 'Proveedor',
        forceSelection: true,
        store: storeFiltroProveedorGPS,
        valueField: 'ProveedorGPS',
        displayField: 'ProveedorGPS',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 80,
        style: {
            marginTop: '5px',
            marginLeft: '5px'
        },
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true
    });
      
    storeFiltroProveedorGPS.load({
        callback: function (r, options, success) {
            if (success) {
                Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
            }
        }
    })
    
    var storeFiltroLocales = new Ext.data.JsonStore({
        autoLoad: true,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'Latitud', 'Longitud', 'Capacidad'],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetLocalesBackhaul',
            headers: {
                'Content-type': 'application/json'
            }
        })
    });

    var comboFiltroLocales = new Ext.form.field.ComboBox({
        id: 'comboFiltroLocales',
        fieldLabel: 'Tienda',
        store: storeFiltroLocales,
        valueField: 'IdZona',
        displayField: 'NombreZona',
        anchor: '99%',
        labelWidth: 80,
        style: {
            marginTop: '5px',
            marginLeft: '5px'
        },
        allowBlank: false,
        editable: true,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        forceSelection: true,
        disabled: false
    });

    storeFiltroLocales.load({
        params: {
            idTipoZona: 0,
            nombreZona: ''
        },
        callback: function (r, options, success) {
            if (success) {
                Ext.getCmp("comboFiltroLocales").setValue(0);

                var store = Ext.getCmp('comboFiltroLocales').store;
                for (var i = 0; i < store.count() ; i++) {

                    var idZona = store.getAt(i).data.IdZona;

                    if (idZona > 0) {
                        var nombreZona = store.getAt(i).data.NombreZona;
                        var capacidad = store.getAt(i).data.Capacidad;
                        var latitud = store.getAt(i).data.Latitud;
                        var longitud = store.getAt(i).data.Longitud;

                        DrawLocal(idZona, nombreZona, capacidad, latitud, longitud)
                    }
                }

            }
        }
    });

    var btnActualizar = {
      id: 'btnActualizar',
      xtype: 'button',
      iconAlign: 'left',
      icon: 'Images/refresh_gray_20x20.png',
      text: 'Actualizar',
      width: 80,
      height: 26,
      handler: function () {
        FiltrarViajes();
      }
    };
 

    var toolbarViajesBackhaul = Ext.create('Ext.toolbar.Toolbar', {
      id: 'toolbarViajesBackhaul',
      height: 100,
      layout: 'column',
      items: [{
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.05,
          items: [chkNroTransporte]
      }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.45,
          items: [textFiltroNroTransporte]
      }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.5,
          items: [comboFiltroLocales]
      }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.5,
          items: [dateDesde, comboFiltroTransportista]
      }, {
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 0.5,
          items: [dateHasta, comboFiltroPatente]
      }]
    });

    var storeViajesBackhaul = new Ext.data.JsonStore({
      autoLoad: false,
      fields: [ 'NroTransporte',
                'IdEmbarque',    
                'Fecha',
                'SecuenciaDestino',
                'PatenteTracto',
                'PatenteTrailer',
                'Transportista',
                { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                'CodigoOrigen',
                'NombreOrigen',
                'FHAsignacion',
                'FHSalidaOrigen',
                'CodigoDestino',
                'NombreDestino',
                'FHLlegadaDestino',
                { name: 'FHCierreSistema', type: 'date', dateFormat: 'c' },
                'TiempoViaje',
                'FHSalidaDestino',
                'EstadoViaje',
                'EstadoLat',
                'EstadoLon',
                'DestinoLat',
                'DestinoLon',
                'CantidadAlertas',
                'Determinante',
                'NombreDeterminante',
                'LatDeterminante',
                'LonDeterminante',
                'CodProveedor',
                'NombreProveedor'
                ],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetViajesBackhaul',
        reader: { type: 'json', root: 'Zonas' },
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

        storeFiltroTransportista.load({
          callback: function (r, options, success) {
            if (success) {

                var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.getAt(0).get("Transportista");
                Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);

              storeFiltroPatente.load({
                callback: function (r, options, success) {
                  if (success) {
                    Ext.getCmp("comboFiltroPatente").setValue("Todas");
                    //FiltrarPatentes();

                  }
                }
              })

            }
          }
        })

    var gridPanelViajesBackhaul = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelViajesBackhaul',
      title: 'Viajes Backhaul',
      store: storeViajesBackhaul,
      anchor: '100% 70%',
      columnLines: true,
      tbar: toolbarViajesBackhaul,
      buttons: [btnActualizar],
      scroll: false,
      viewConfig: {
        style: { overflow: 'auto', overflowX: 'hidden' }
      },
      columns: [
                    { text: 'Id Master', sortable: true, width: 80, dataIndex: 'NroTransporte' },
                    { text: 'Fecha', sortable: true, width: 70, dataIndex: 'Fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y') },
                    { text: 'Tracto', sortable: true, width: 56, dataIndex: 'PatenteTracto' },
                    { text: 'Remolque', sortable: true, width: 60, dataIndex: 'PatenteTrailer' },
                    { text: 'Línea', sortable: true, flex: 1, dataIndex: 'Transportista' },
                    { text: 'Det.', sortable: true, width: 40, dataIndex: 'Determinante' },
                    { text: 'Proveedor', sortable: true, flex: 1, dataIndex: 'NombreProveedor' },
                    {text:  'Alertas', sortable: true, width: 50, dataIndex: 'CantidadAlertas', renderer: renderCantidadAlertas },
                    { text: 'Estado', sortable: true, flex: 1, dataIndex: 'EstadoViaje', renderer: renderEstadoViaje }
      ],
      listeners: {
          select: function (sm, row, rec) {

          Ext.Msg.wait('Espere por favor...', 'Generando ruta');

          Ext.getCmp("chkMostrarZonas").setValue(false);
          Ext.getCmp("chkMostrarCamionesCercanos").setValue(false);
          

          var nroTransporte = row.data.NroTransporte;
          var idEmbarque = row.data.IdEmbarque;
          var origen = row.data.CodigoOrigen;
          var destino = row.data.CodigoDestino;
          var estadoViaje = row.data.EstadoViaje;
          var patenteTracto = row.data.PatenteTracto;
          var patenteTrailer = row.data.PatenteTrailer;
          var FechaHoraCreacion = row.data.FechaHoraCreacion;
          var FHSalidaOrigen = row.data.FHSalidaOrigen;
          var FHLlegadaDestino = row.data.FHLlegadaDestino;
          var FHCierreSistema = row.data.FHCierreSistema;

          var estadoLat = row.data.EstadoLat;
          var estadoLon = row.data.EstadoLon;

          var origenLat = row.data.LatDeterminante;
          var origenLon = row.data.LonDeterminante;
          var destinoLat = row.data.DestinoLat;
          var destinoLon = row.data.DestinoLon;

          ClearMap();
          arrayPositions.splice(0, arrayPositions.length);
          arrayAlerts.splice(0, arrayAlerts.length);
          eraseHouses();

          if (directionsDisplay) {
              directionsDisplay.setDirections({ routes: [] });
          }

          GetPosiciones(origen, destino, patenteTracto, patenteTrailer, FechaHoraCreacion, FHSalidaOrigen, FHLlegadaDestino, FHCierreSistema, nroTransporte, idEmbarque, destino, estadoViaje)
          GetAlertasRuta(nroTransporte, idEmbarque, destino, estadoViaje);

          DrawRoute(origenLat, origenLon, destinoLat, destinoLon);

          if (estadoViaje == 'En Ruta' || estadoViaje == 'RUTA') {
            CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon);
          }
          else {
            Ext.getCmp('winDistanciaTiempo').hide();
          }

          Ext.Ajax.request({
              url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
              success: function (response, opts) {

                  var task = new Ext.util.DelayedTask(function () {
                      Ext.Msg.hide();
                  });

                  task.delay(100);

              },
              failure: function (response, opts) {
                  Ext.Msg.hide();
              }
          });

        }
      }

    });

    var storeZonasBackhaul = new Ext.data.JsonStore({
        id: 'storeZonasBackhaul',
        autoLoad: true,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud', 'Capacidad'],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetLocalesBackhaul',
            reader: { type: 'json', root: 'Zonas' },
            headers: {
                'Content-type': 'application/json'
            }
        })
    });

    var storeCamionesCercanosBackhaul = new Ext.data.JsonStore({
        autoLoad: false,
        fields: [{ name: 'UltReporte', type: 'date', dateFormat: 'c' },
                  'TextUltReporte',
                  'Patente',
                  'IdTipoMovil',
                  'Transportista',
                  'Latitud',
                  'Longitud',
                  'Ignicion',
                  'Velocidad',
                  'Direccion',
                  'EstadoGPS',
                  'EstadoViaje',
                  'NroTransporte',
                  'IdEmbarque',
                  'CodigoDestino',
                  'ProveedorGPS',
                  'CodigoOrigen',
                  'NombreOrigen',
                  'Destinos',
                  'CapacidadUnidad',
                  'PatenteTracto',
                  'PatenteTrailer'
        ],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetCamionesCercanosBackhaul',
            reader: { type: 'json', root: 'Zonas' },
            headers: {
                'Content-type': 'application/json'
            }
        })
    });

    var gridCamionesCercanosBackhaul = Ext.create('Ext.grid.Panel', {
        id: 'gridCamionesCercanosBackhaul',
        store: storeCamionesCercanosBackhaul,
        anchor: '100% 100%',
        columns: [
                      { text: 'Nro. Viaje', flex: 1, dataIndex: 'NroTransporte' }
        ],
    });

    storeCamionesCercanosBackhaul.load({
        callback: function (r, options, success) {
            if (success) {
                MostrarFlota();

            }
        }
    })

    var storeZonasToDraw = new Ext.data.JsonStore({
      id: 'storeZonasToDraw',
      autoLoad: false,
      fields: ['IdZona'],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonasToDraw',
        reader: { type: 'json', root: 'Zonas' },
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

    var gridZonasToDraw = Ext.create('Ext.grid.Panel', {
      id: 'gridZonasToDraw',
      store: storeZonasToDraw,
      columns: [
                { text: 'IdZona', flex: 1, dataIndex: 'IdZona' }
             ]

    });

    var storeZonasProveedores = new Ext.data.JsonStore({
        id: 'storeZonasProveedores',
        autoLoad: false,
        fields: ['IdZona', 'NombreZona'],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
            reader: { type: 'json', root: 'Zonas' },
            headers: {
                'Content-type': 'application/json'
            }
        })
    });

    storeZonasProveedores.load({
        params: {
            idTipoZona: 5,
            nombreZona: ''
        },
        callback: function (r, options, success) {
            if (success) {

                var store = Ext.getCmp('comboZonaProveedor').store;
                for (var i = 0; i < store.count() ; i++) {

                    var idZona = store.getAt(i).data.IdZona;
                    /*
                    if (idZona > 0) {
                        DrawZone(store.getAt(i).data.IdZona);
                    }*/
                }

            }
        }
    });

    var storePosicionesRuta = new Ext.data.JsonStore({
      autoLoad: false,
      fields: ['Patente',
                'IdTipoMovil',
                'NombreTipoMovil',
                { name: 'Fecha', type: 'date', dateFormat: 'c' },
                'Latitud',
                'Longitud',
                'Velocidad',
                'Direccion',
                'Ignicion',
                ],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPosicionesRuta',
        reader: { type: 'json', root: 'Zonas' },
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

    var gridPosicionesRuta = Ext.create('Ext.grid.Panel', {
      id: 'gridPosicionesRuta',
      store: storePosicionesRuta,
      columns: [
                    { text: 'Patente', dataIndex: 'Patente', hidden: true },
                    { text: 'IdTipoMovil', dataIndex: 'IdTipoMovil', hidden: true },
                    { text: 'NombreTipoMovil', dataIndex: 'NombreTipoMovil', hidden: true },
                    { text: 'Fecha', dataIndex: 'Fecha', hidden: true, renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    { text: 'Latitud', dataIndex: 'Latitud', hidden: true },
                    { text: 'Longitud', dataIndex: 'Longitud', hidden: true },
                    { text: 'Velocidad', dataIndex: 'Velocidad', hidden: true },
                    { text: 'Direccion', dataIndex: 'Direccion', hidden: true },
                    { text: 'Ignicion', dataIndex: 'Ignicion', hidden: true }
             ]
    });

    var storeAlertasRuta = new Ext.data.JsonStore({
      autoLoad: false,
      fields: [{ name: 'FechaInicioAlerta', type: 'date', dateFormat: 'c' },
               { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                'PatenteTracto',
                'TextFechaCreacion',
                'PatenteTrailer',
                'Velocidad',
                'Latitud',
                'Longitud',
                'DescripcionAlerta',
                'Ocurrencia'
                ],
      proxy: new Ext.data.HttpProxy({
        url: 'AjaxPages/AjaxAlertas.aspx?Metodo=GetAlertasRuta',
        reader: { type: 'json', root: 'Zonas' },
        headers: {
          'Content-type': 'application/json'
        }
      })
    });

    var gridPanelAlertasRuta = Ext.create('Ext.grid.Panel', {
      id: 'gridPanelAlertasRuta',
      title: 'Alertas',
      store: storeAlertasRuta,
      anchor: '100% 30%',
      columnLines: true,
      scroll: false,
      viewConfig: {
        style: { overflow: 'auto', overflowX: 'hidden' }
      },
      columns: [
                    { text: 'Fecha Inicio', sortable: true, width: 110, dataIndex: 'FechaInicioAlerta', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    { text: 'Fecha Envío', sortable: true, width: 110, dataIndex: 'FechaHoraCreacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                    {text: 'Descripción', sortable: true, flex: 1, dataIndex: 'DescripcionAlerta' }
              ],
      listeners: {
        select: function (sm, row, rec) {

          var date = Ext.getCmp('gridPanelAlertasRuta').getStore().data.items[rec].raw.FechaHoraCreacion.toString();

          for (var i = 0; i < markers.length; i++) {
            if (markers[i].labelText == date) {
              markers[i].setAnimation(google.maps.Animation.BOUNCE);
              setTimeout('markers[' + i + '].setAnimation(null);', 800);

              var contentString =

                  '<br>' +
                      '<table>' +
                        '<tr>' +
                            '       <td><b>Fecha</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + row.data.TextFechaCreacion + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Velocidad:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + row.data.Velocidad + ' Km/h </td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Latitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + row.data.Latitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Longitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + row.data.Longitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Descripción:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + row.data.DescripcionAlerta + '</td>' +
                        '</tr>' +

                      '</table>' +
                    '<br>';
              infowindow.setContent(contentString);
              infowindow.open(map, markers[i]);

              Ext.getCmp("gridPanelAlertasRuta").getSelectionModel().deselectAll();

              break;
            }
          }

          map.setCenter(new google.maps.LatLng(row.data.Latitud, row.data.Longitud));

        }
      }
    });

    var viewWidth = Ext.getBody().getViewSize().width;
    var viewHeight = Ext.getBody().getViewSize().height;

    var chkMostrarZonas = new Ext.form.Checkbox({
        id: 'chkMostrarZonas',
        fieldLabel: 'Mostrar locales',
        labelWidth: 150,
        width: 170,
        checked: true,
        style: {
            marginLeft: '5px'
        },
        listeners: {
            change: function (cb, checked) {
                if (checked == true) {
                    MostrarZonas();
                }
                else {
                    eraseAllZones();
                }
            }
        }
    });

    var chkMostrarCamionesCercanos = new Ext.form.Checkbox({
        id: 'chkMostrarCamionesCercanos',
        fieldLabel: 'Mostrar placas cercanas',
        labelWidth: 150,
        width: 170,
        checked: true,
        style: {
            marginLeft: '5px'
        },
        listeners: {
            change: function (cb, checked) {
                if (checked == true) {
                    MostrarFlota();
                }
                else {
                    ClearMap();
                }
            }
        }
    });

    var chkMostrarProveedores = new Ext.form.Checkbox({
        id: 'chkMostrarProveedores',
        fieldLabel: 'Mostrar proveedores',
        labelWidth: 150,
        width: 170,
        checked: false,
        style: {
            marginLeft: '5px'
        },
        listeners: {
            change: function (cb, checked) {
                if (checked == true) {
                    MostrarProveedores();
                }
                else {
                    EliminarProveedores();
                }
            }
        }
    });

    var chkMostrarLabels = new Ext.form.Checkbox({
        id: 'chkMostrarLabels',
        fieldLabel: 'Mostrar etiquetas',
        labelWidth: 150,
        width: 170,
        checked: false,
        style: {
            marginLeft: '5px'
        },
        listeners: {
            change: function (cb, checked) {
                if (checked == true) {
                    for (var i = 0; i < arrayLabelLocales.length; i++) {
                        arrayLabelLocales[i].setMap(map);
                    }

                    
                }
                else {
                    for (var i = 0; i < arrayLabelLocales.length; i++) {
                        arrayLabelLocales[i].setMap(null);
                    }
                }



            }
        }
    });

    var chkMostrarTrafico = new Ext.form.Checkbox({
        id: 'chkMostrarTrafico',
        fieldLabel: 'Mostrar tráfico',
        labelWidth: 150,
        width: 170,
        checked: false,
        style: {
            marginLeft: '5px'
        },
        listeners: {
            change: function (cb, checked) {
                if (checked == true) {
                    trafficLayer.setMap(map);
                }
                else {
                    trafficLayer.setMap(null);
                }
            }
        }
    });

    var btnNuevoViajeBackhaul = {
        id: 'btnNuevoViajeBackhaul',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/add_blue_20x19.png',
        text: 'Nuevo viaje',
        width: 100,
        height: 27,
        style: {
            marginLeft: '20px'
        },
        handler: function () {

            _editing = false;
            NewBackhaul();

        }
    };

    var winPanelBackhaul = new Ext.Window({
        id: 'winPanelBackhaul',
        title: 'Opciones Backhaul',
        width: 220,
        height: 200,
        closable: false,
        closeAction: 'hide',
        modal: false,
        initCenter: false,
        x: viewWidth - 230,
        y: 50,
        buttons: [btnNuevoViajeBackhaul],
        items: [{
            xtype: 'container',
            layout: 'anchor',
            style: 'padding-top:3px;padding-left:5px;',
            items: [chkMostrarZonas]
        }, {
            xtype: 'container',
            layout: 'anchor',
            style: 'padding-top:3px;padding-left:5px;',
            items: [chkMostrarCamionesCercanos]
        }, {
            xtype: 'container',
            layout: 'anchor',
            style: 'padding-top:3px;padding-left:5px;',
            items: [chkMostrarProveedores]
        }, {
            xtype: 'container',
            layout: 'anchor',
            style: 'padding-left:5px;',
            items: [chkMostrarLabels]
        }, {
            xtype: 'container',
            layout: 'anchor',
            style: 'padding-left:5px;',
            items: [chkMostrarTrafico]
        }
        ],
        resizable: false,
        border: true,
        draggable: false
    });

    winPanelBackhaul.show();

    var textDistancia = new Ext.form.TextField({
      id: 'textDistancia',
      fieldLabel: 'Distancia',
      labelWidth: 60,
      anchor: '99%',
      readOnly: true
    });

    var textTiempo = new Ext.form.TextField({
      id: 'textTiempo',
      fieldLabel: 'Tiempo',
      labelWidth: 60,
      anchor: '99%',
      readOnly: true
    });

    var winDistanciaTiempo = new Ext.Window({
      id: 'winDistanciaTiempo',
      title: 'Distancia / Tiempo hasta destino',
      width: 220,
      height: 30,
      closable: false,
      closeAction: 'hide',
      modal: false,
      initCenter: false,
      x: viewWidth - 230,
      y: 340,
      items: [{
        xtype: 'container',
        layout: 'anchor',
        style: 'padding-top:3px;padding-left:5px;',
        items: [textDistancia]
      }, {
        xtype: 'container',
        layout: 'anchor',
        style: 'padding-left:5px;',
        items: [textTiempo]
      }
      ],
      resizable: false,
      border: true,
      draggable: false
    });

    var numberNroTransporte = new Ext.form.NumberField({
        fieldLabel: 'Id. Master',
        id: 'numberNroTransporte',
        allowBlank: false,
        labelWidth: 120,
        anchor: '99%',
        minValue: 1,
        maxValue: 999999999999,
        enableKeyEvents: true,
        listeners: {
            'blur': function (_field) {
                //ValidarNroTransporte();
            }
        }
    });

    var numberIdEmbarque = new Ext.form.NumberField({
        fieldLabel: 'Id. Embarque',
        id: 'numberIdEmbarque',
        allowBlank: false,
        labelWidth: 120,
        anchor: '99%',
        minValue: 1,
        maxValue: 999999999999,
        enableKeyEvents: true,
        listeners: {
            'blur': function (_field) {
                //ValidarNroTransporte();
            }
        }
    });

    var comboFiltroTransportistaBackhaul = new Ext.form.field.ComboBox({
        id: 'comboFiltroTransportistaBackhaul',
        fieldLabel: 'Línea Transp.',
        forceSelection: true,
        store: storeFiltroTransportista,
        valueField: 'Transportista',
        displayField: 'Transportista',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: false,
        forceSelection: true,
        listeners: {
            select: function () {
                //FiltrarPatentes();
                Ext.getCmp("comboFiltroPatenteTractoBackhaul").setDisabled(false);
                Ext.getCmp("comboFiltroPatenteTrailerBackhaul").setDisabled(false);
            }
        }
    });

    var comboFiltroPatenteTractoBackhaul = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTractoBackhaul',
        fieldLabel: 'Tracto',
        forceSelection: true,
        store: storeFiltroPatente,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true,
        disabled: true
    });

    var comboFiltroPatenteTrailerBackhaul = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTrailerBackhaul',
        fieldLabel: 'Remolque',
        forceSelection: true,
        store: storeFiltroPatente,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true,
        disabled: true
    });

    var comboZonaOrigen = new Ext.form.field.ComboBox({
        id: 'comboZonaOrigen',
        fieldLabel: 'Determinante',
        allowBlank: false,
        store: storeZonasBackhaul,
        queryMode: 'local',
        valueField: 'IdZona',
        displayField: 'IdZona',
        anchor: '99%',
        forceSelection: true,
        enableKeyEvents: true,
        editable: true,
        labelWidth: 120,
        emptyText: 'Seleccione...',
        listConfig: {
            loadingText: 'Buscando...',
            getInnerTpl: function () {
                return '<a class="search-item">' +
                                '<span>{NombreZona}</span><br />' +
                                '<span>Capacidad: {Capacidad} ft</span><br />' +
                            '</a>';
            }
        }
    });
      
    storeZonasBackhaul.load({
        params: {
            idTipoZona: 5,
            nombreZona: ''
        }
    });
    
    var comboZonaProveedor = new Ext.form.field.ComboBox({
        id: 'comboZonaProveedor',
        fieldLabel: 'Proveedor',
        allowBlank: false,
        store: storeZonasProveedores,
        queryMode: 'local',
        valueField: 'IdZona',
        displayField: 'NombreZona',
        anchor: '99%',
        forceSelection: true,
        enableKeyEvents: true,
        editable: true,
        labelWidth: 120,
        emptyText: 'Seleccione...'
    });

    var storeZonasDestino = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
        proxy: new Ext.data.HttpProxy({
            url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
            reader: { type: 'json', root: 'Zonas' },
            headers: {
                'Content-type': 'application/json'
            }
        })
    });

    var comboZonaDestino = new Ext.form.field.ComboBox({
        id: 'comboZonaDestino',
        fieldLabel: 'CEDIS',
        allowBlank: false,
        store: storeZonasDestino,
        valueField: 'IdZona',
        displayField: 'NombreZona',
        queryMode: 'local',
        anchor: '99%',
        forceSelection: true,
        enableKeyEvents: true,
        editable: true,
        labelWidth: 120,
        emptyText: 'Seleccione...'
    });

    storeZonasDestino.load({
        params: {
            idTipoZona: 1,
            nombreZona: ''
        }
    });

    var formCrearViaje = new Ext.FormPanel({
        id: 'formCrearViaje',
        border: false,
        frame: true,
        items: [numberNroTransporte, numberIdEmbarque, comboFiltroTransportistaBackhaul, comboFiltroPatenteTractoBackhaul, comboFiltroPatenteTrailerBackhaul, comboZonaOrigen, comboZonaProveedor, comboZonaDestino]
    });

    var btnGuardar = {
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/save_black_20x20.png',
        text: 'Guardar',
        width: 90,
        height: 26,
        handler: function () {
            GuardarViaje();
        }
    };

    var btnCancelar = {
        id: 'btnCancelar',
        xtype: 'button',
        width: 90,
        height: 26,
        iconAlign: 'left',
        icon: 'Images/back_black_20x20.png',
        text: 'Cancelar',
        handler: function () {
            Cancelar();
        }
    };

    var winCrearViaje = new Ext.Window({
        id: 'winCrearViaje',
        title: 'Nuevo viaje Backhaul',
        width: 400,
        closeAction: 'hide',
        modal: true,
        items: formCrearViaje,
        resizable: false,
        border: false,
        constrain: true,    
        buttons: [btnGuardar, btnCancelar]
    });

    var leftPanel = new Ext.FormPanel({
      id: 'leftPanel',
      region: 'west',
      margins: '0 0 3 3',
      border: true,
      width: 580,
      minWidth: 400,
      maxWidth: viewWidth / 1.5,
      layout: 'anchor',
      split: true,
      collapsible: true,
      hideCollapseTool: true,
      items: [gridPanelViajesBackhaul, gridPanelAlertasRuta]
    });

    leftPanel.on('collapse', function () {
      google.maps.event.trigger(map, "resize");
    });

    leftPanel.on('expand', function () {
      google.maps.event.trigger(map, "resize");
    });

    var centerPanel = new Ext.FormPanel({
      id: 'centerPanel',
      region: 'center',
      border: true,
      margins: '0 3 3 0',
      anchor: '100% 100%',
      contentEl: 'dvMap'
    });

    var viewport = Ext.create('Ext.container.Viewport', {
      layout: 'border',
      items: [topMenu, leftPanel, centerPanel]
    });

    viewport.on('resize', function () {
      google.maps.event.trigger(map, "resize");
      Ext.getCmp('winPanelBackhaul').setPosition(Ext.getBody().getViewSize().width - 230, 50, true)
      Ext.getCmp('winDistanciaTiempo').setPosition(Ext.getBody().getViewSize().width - 230, 250, true)

    });

  }); 

</script>

<script type="text/javascript">

  Ext.onReady(function () {
    GeneraMapa("dvMap", true);
  });

  function FiltrarViajes() {
    
      Ext.getCmp("chkMostrarZonas").setValue(false);
      Ext.getCmp("chkMostrarCamionesCercanos").setValue(false);

    var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();
    if (Ext.getCmp("chkNroTransporte").getValue() == true && nroTransporte == "") {
        return;
    }

    ClearMap();
    arrayPositions.splice(0, arrayPositions.length);
    arrayAlerts.splice(0, arrayAlerts.length);
    eraseHouses();

    if (directionsDisplay) {
        directionsDisplay.setDirections({ routes: [] });
    }

    Ext.getCmp('winDistanciaTiempo').hide();

    Ext.getCmp("gridPanelViajesBackhaul").getStore().removeAll();
    Ext.getCmp("gridPanelAlertasRuta").getStore().removeAll()

    var desde = Ext.getCmp('dateDesde').getValue();
    var hasta = Ext.getCmp('dateHasta').getValue();

    var patente = Ext.getCmp('comboFiltroPatente').getValue();
    var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
    var local = Ext.getCmp('comboFiltroLocales').getValue();

    var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

    switch (estadoViaje) {
      case "Finalizado":
        estadoViaje = "EnLocal-P";
        break;
      case "En Local":
        estadoViaje = "EnLocal-R";
        break;
      case "En Ruta":
        estadoViaje = "RUTA";
        break;
      case "Asignado":
        estadoViaje = "ASIGNADO";
        break;
      case "Cerrado por Sistema":
        estadoViaje = "Cerrado por Sistema";
        break;
      case "Todos":
        estadoViaje = "Todos";
        break;
      default:
        estadoViaje = "Todos";
    }

    var store = Ext.getCmp('gridPanelViajesBackhaul').store;
    store.load({
      params: {
        desde: desde,
        hasta: hasta,
        nroTransporte: nroTransporte,
        patente: patente,
        transportista: transportista,
        codLocal: local
      }
    });
  }

  function GetAlertasRuta(nroTransporte, idEmbarque, destino, estadoViaje) {

    var store = Ext.getCmp('gridPanelAlertasRuta').store;
    store.load({
      params: {
        nroTransporte: nroTransporte,
        idEmbarque: idEmbarque,
        destino: destino,
        estadoViaje: estadoViaje
      },
      callback: function (r, options, success) {
          if (success) {
              MuestraAlertasViaje();
        }
      }
    });
  }

  function GetPosiciones(origen, destino, patenteTracto, patenteTrailer, fechaHoraCreacion, fechaHoraSalidaOrigen, fechaHoraLlegadaDestino, fechaHoraCierreSistema, nroTransporte, idEmbarque, destino, estadoViaje) {

    Ext.getCmp('gridPosicionesRuta').store.removeAll();

    var store = Ext.getCmp('gridPosicionesRuta').store;
    var storeZone = Ext.getCmp('gridZonasToDraw').store;

    var fec;

    if (estadoViaje == 'Finalizado') {
      fec = fechaHoraLlegadaDestino;
    }
    if (estadoViaje == 'Cerrado por Sistema') {
      fec = fechaHoraCierreSistema;
    }
    if (fec == null) {
        fec = new Date();
    }

    store.load({
      params: {
        patenteTracto: patenteTracto,
        patenteTrailer: patenteTrailer,
        fechaHoraCreacion: fechaHoraCreacion,
        fechaHoraSalidaOrigen: fechaHoraSalidaOrigen,
        fechaHoraLlegadaDestino: fec,
        nroTransporte: nroTransporte,
        idEmbarque: idEmbarque,
        destino: destino,
        estadoViaje: estadoViaje
      },
      callback: function (r, options, success) {
        if (success) {

          storeZone.load({
            params: {
              fechaDesde: fechaHoraCreacion,
              fechaHasta: fec,
              patente1: patenteTrailer,
              patente2: patenteTracto
            },
            callback: function (r, options, success) {
              if (success) {

                MuestraRutaViaje();

                var store = Ext.getCmp('gridZonasToDraw').getStore();
                for (var i = 0; i < store.count() ; i++) {
                    if (store.getAt(i).data.IdZona > 0) {
                        DrawZone(store.getAt(i).data.IdZona);
                    }
                }

                DrawZone(origen);
                var storeViajes = Ext.getCmp('gridPanelViajesBackhaul').store;

                for (var i = 0; i < storeViajes.count(); i++) {
                  if (storeViajes.getAt(i).data.NroTransporte == nroTransporte) {
                      DrawZone(storeViajes.getAt(i).data.CodigoDestino);
                      DrawHouse(storeViajes.getAt(i).data.CodigoDestino, storeViajes.getAt(i).data.DestinoLat, storeViajes.getAt(i).data.DestinoLon, storeViajes.getAt(i).data.EstadoViaje);
                  }
                }

              }
            }

          });

        }
      }
    });

  }

  function MuestraRutaViaje() {

    var store = Ext.getCmp('gridPosicionesRuta').getStore();
    var rowCount = store.count();
    var iterRow = 0;

    while (iterRow < rowCount) {

      var dir = parseInt(store.data.items[iterRow].raw.Direccion);

      var lat = store.data.items[iterRow].raw.Latitud;
      var lon = store.data.items[iterRow].raw.Longitud;

      var Latlng = new google.maps.LatLng(lat, lon);

      arrayPositions.push({ Fecha: store.data.items[iterRow].raw.Fecha.toString(),
        Velocidad: store.data.items[iterRow].raw.Velocidad,
        Latitud: lat,
        Longitud: lon,
        LatLng: Latlng
      });

      if (store.data.items[iterRow].raw.Velocidad > 0) {

        switch (true) {
          case ((dir >= 338) || (dir < 22)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/1_arrowcircle_blue_N_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 22) && (dir < 67)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/2_arrowcircle_blue_NE_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 67) && (dir < 112)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/3_arrowcircle_blue_E_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 112) && (dir < 157)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/4_arrowcircle_blue_SE_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 157) && (dir < 202)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/5_arrowcircle_blue_S_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 202) && (dir < 247)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/6_arrowcircle_blue_SW_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 247) && (dir < 292)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/7_arrowcircle_blue_W_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
          case ((dir >= 292) && (dir < 338)):
            marker = new google.maps.Marker({
              position: Latlng,
              icon: 'Images/Circle_Arrow/8_arrowcircle_blue_NW_20x20.png',
              map: map,
              labelText: store.data.items[iterRow].raw.Fecha.toString()
            });
            break;
        }
      }
      else {
        marker = new google.maps.Marker({
          position: Latlng,
          icon: 'Images/dot_red_16x16.png',
          map: map,
          labelText: store.data.items[iterRow].raw.Fecha.toString()
        });
      }

      var label = new Label({
        map: null
      });
      label.bindTo('position', marker, 'position');
      label.bindTo('text', marker, 'labelText');

      google.maps.event.addListener(marker, 'click', function () {
        var latLng = this.position;
        var fec = this.labelText;

        for (i = 0; i < arrayPositions.length; i++) {
          if (arrayPositions[i].Fecha.toString() == fec.toString() & arrayPositions[i].LatLng.toString() == latLng.toString()) {

            var Lat = arrayPositions[i].Latitud;
            var Lon = arrayPositions[i].Longitud;

            var contentString =

                  '<br>' +
                      '<table>' +
                        '<tr>' +
                            '       <td><b>Fecha</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + (arrayPositions[i].Fecha.toString()).replace("T", " ") + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Velocidad:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Velocidad + ' Km/h </td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Latitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Latitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Longitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayPositions[i].Longitud + '</td>' +
                        '</tr>' +
                      '</table>' +
                    '<br>';

            infowindow.setContent(contentString);
            infowindow.open(map, this);

            break;
          }
        }

      });

      markers.push(marker);
      labels.push(label);

      iterRow++;
    }

    if (rowCount > 0) {
      var len = markers.length - 1
      map.setCenter(markers[len].position);
      markers[len].setAnimation(google.maps.Animation.BOUNCE);
      setTimeout('markers[' + len + '].setAnimation(null);', 800);
    }

  }

  function MuestraAlertasViaje() {

    var store = Ext.getCmp('gridPanelAlertasRuta').getStore();
    var rowCount = store.count();
    var iterRow = 0;

    while (iterRow < rowCount) {
      var descrip = store.data.items[iterRow].raw.DescripcionAlerta;

      var lat = store.data.items[iterRow].raw.Latitud;
      var lon = store.data.items[iterRow].raw.Longitud;

      var Latlng = new google.maps.LatLng(lat, lon);

      arrayAlerts.push({ Fecha: store.data.items[iterRow].raw.FechaHoraCreacion.toString(),
        TextFechaCreacion: store.data.items[iterRow].raw.TextFechaCreacion,
        Velocidad: store.data.items[iterRow].raw.Velocidad,
        Latitud: lat,
        Longitud: lon,
        LatLng: Latlng,
        Descripcion: store.data.items[iterRow].raw.DescripcionAlerta
      });

      switch (true) {
          case (descrip == 'CRUCE GEOCERCA PARA INGRESAR A TIENDA'):
          marker = new google.maps.Marker({
            position: Latlng,
            icon: 'Images/finishflag_24x24.png',
            map: map,
            labelText: store.data.items[iterRow].raw.FechaHoraCreacion.toString()
          });
          break;
        default:
          marker = new google.maps.Marker({
            position: Latlng,
            icon: 'Images/alert_orange_22x22.png',
            map: map,
            labelText: store.data.items[iterRow].raw.FechaHoraCreacion.toString()
          });
          break;
      }

      var label = new Label({
        map: null
      });
      label.bindTo('position', marker, 'position');
      label.bindTo('text', marker, 'labelText');

      google.maps.event.addListener(marker, 'click', function () {

        var latLng = this.position;
        var fec = this.labelText;

        for (i = 0; i < arrayAlerts.length; i++) {
          if (arrayAlerts[i].Fecha.toString() == fec.toString() & arrayAlerts[i].LatLng.toString() == latLng.toString()) {

            var contentString =

                  '<br>' +
                      '<table>' +
                        '<tr>' +
                            '       <td><b>Fecha</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + arrayAlerts[i].TextFechaCreacion + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Velocidad:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayAlerts[i].Velocidad + ' Km/h </td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Latitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayAlerts[i].Latitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Longitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayAlerts[i].Longitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Descripción:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + arrayAlerts[i].Descripcion + '</td>' +
                        '</tr>' +

                      '</table>' +
                    '<br>';

            infowindow.setContent(contentString);
            infowindow.open(map, this);

            break;

          }
        }

      });

      markers.push(marker);
      labels.push(label);

      iterRow++;
    }

  }

  function DrawLocal(idZona, nombreZona, capacidad, latitud, longitud)
  {
      var object = new Object();
      object.IdZona = idZona;

      var image = new google.maps.MarkerImage("Images/house_blue_24x24.png");
      var point = new google.maps.LatLng(latitud, longitud);

      object.layer = new google.maps.Marker({
          position: point,
          icon: image,
          labelText: nombreZona,
          map: map
      });

      var viewLabel = Ext.getCmp('chkMostrarLabels').getValue();

      object.label = new Label({
          text: idZona,
          position: new google.maps.LatLng(latitud, longitud),
          map: viewLabel ? map : null
      });

      object.label.bindTo('text', object.layer, 'labelText');
      object.layer.setMap(map);

      arrayLocalesBackhaul.push({
          IdZona: idZona,
          NombreZona: nombreZona,
          Capacidad: capacidad
      });

      google.maps.event.addListener(object.layer, 'click', function () {
          var latLng = this.position;
          var patFec = this.labelText;

          for (i = 0; i < arrayLocalesBackhaul.length; i++) {
              if (arrayLocalesBackhaul[i].IdZona == idZona) {

                  var contentString =

                        '<br>' +
                            '<table>' +
                              '<tr>' +
                                  '        <td><b>Determinante:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + arrayLocalesBackhaul[i].IdZona + '</td>' +
                              '</tr>' +
                              '<tr>' +
                                  '        <td><b>Nombre:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + arrayLocalesBackhaul[i].NombreZona + '</td>' +
                              '</tr>' +
                              '<tr>' +
                                  '        <td><b>Capacidad:</b></td>' +
                                  '       <td><pre>     </pre></td>' +
                                  '        <td>' + arrayLocalesBackhaul[i].Capacidad + ' ft' + '</td>' +
                              '</tr>' +
                              '</table>' +
                          '<br>';

                  infowindow.setContent(contentString);
                  infowindow.open(map, this);

                  break;
              }
          }

      });

      arrayLayerLocales.push(object);
      arrayLabelLocales.push(object.label);

  }

    
  function DrawZone(idZona) {

    for (var i = 0; i < geoLayer.length; i++) {
      geoLayer[i].layer.setMap(null);
      geoLayer[i].label.setMap(null);
      geoLayer.splice(i, 1);
    }

    Ext.Ajax.request({
      url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
      params: {
        IdZona: idZona
      },
      success: function (data, success) {
        if (data != null) {
          data = Ext.decode(data.responseText);
          if (data.Vertices.length > 1) { //Polygon
            var polygonGrid = new Object();
            polygonGrid.IdZona = data.IdZona;

            var arr = new Array();
            for (var i = 0; i < data.Vertices.length; i++) {
              arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
            }

            if (data.idTipoZona == 3) {
              var colorZone = "#FF0000";
            }
            else {
              var colorZone = "#7f7fff";
            }

            polygonGrid.layer = new google.maps.Polygon({
              paths: arr,
              strokeColor: "#000000",
              strokeWeight: 1,
              strokeOpacity: 0.9,
              fillColor: colorZone,
              fillOpacity: 0.3,
              labelText: data.NombreZona
            });

            var viewLabel = Ext.getCmp('chkMostrarLabels').getValue();

            polygonGrid.label = new Label({
                text: idZona,
                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                map: viewLabel ? map : null
            });

            polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
            polygonGrid.layer.setMap(map);
            geoLayer.push(polygonGrid);

            arrayLabelLocales.push(polygonGrid.label);

          }
        }
      },
      failure: function (msg) {
        alert('Se ha producido un error. 3');
      }
    });
  }
    
  function containsZone(a, obj) {
      var i = a.length;
      while (i--) {
          if (a[i].IdZona === obj) {
              return true;
          }
      }
      return false;
  }

  function containsLabel(a, obj) {
      var i = a.length;
      while (i--) {
          if (a[i].text === obj) {
              return true;
          }
      }
      return false;
  }

  function eraseAllZones() {
      var countZonas = Ext.getCmp('comboFiltroLocales').store.count()

      for (i = 0; i < countZonas; i++) {
          var idZona = Ext.getCmp("comboFiltroLocales").store.data.getAt(i).data.IdZona;
          EraseZone(idZona);
      }

  }

  function EraseZone(idZona) {
      for (var i = 0; i < arrayLayerLocales.length; i++) {
          if (idZona == arrayLayerLocales[i].IdZona) {
              arrayLayerLocales[i].layer.setMap(null);
              arrayLayerLocales[i].label.setMap(null);
              arrayLayerLocales.splice(i, 1);
          }
      }

      for (var i = 0; i < arrayLabelLocales.length; i++) {
          if (arrayLabelLocales[i].text == idZona) {
              arrayLabelLocales[i].setMap(null);
              arrayLabelLocales.splice(i, 1);

          }
      }

  }

  function CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon) {

    var service = new google.maps.DistanceMatrixService();
    var origen = new google.maps.LatLng(estadoLat, estadoLon);
    var destino = new google.maps.LatLng(destinoLat, destinoLon);

    service.getDistanceMatrix(
    {
      origins: [origen],
      destinations: [destino],
      travelMode: google.maps.TravelMode.DRIVING,
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
    }, callback);
  }

  function callback(response, status) {
    if (status == google.maps.DistanceMatrixStatus.OK) {

      var distance = response.rows[0].elements[0].distance.text;
      var time = response.rows[0].elements[0].duration.text;

      Ext.getCmp('winDistanciaTiempo').show();

      Ext.getCmp('textDistancia').setValue(distance);
      Ext.getCmp('textTiempo').setValue(time);

      Ext.getCmp('winDistanciaTiempo').setPosition(Ext.getBody().getViewSize().width - 230, 250, true)
    }
  }

  function FiltrarPatentes() {
    var transportista = Ext.getCmp('comboFiltroTransportistaBackhaul').getValue();

    var store = Ext.getCmp('comboFiltroPatente').store;
    store.load({
      params: {
        transportista: transportista
      }
    });
  }

  var renderEstadoViaje = function (value, meta) {
    {
      if (value == 'Cerrado por Sistema') {
        meta.tdCls = 'red-cell';
        return value;
      }
      if (value == 'Finalizado') {
        meta.tdCls = 'blue-cell';
        return value;
      }
      else {
        meta.tdCls = 'black-cell';
        return value;
      }
    }
  };

  var renderCantidadAlertas = function (value, meta) {
    {
      if (value >= 1) {
        meta.tdCls = 'red-cell';
        return value;
      }
      else {
        return value;
      }
    }
  };

  function MostrarZonas() {

      //Ext.Msg.wait('Espere por favor...', 'Generando zonas');

      var countZonas = Ext.getCmp('comboFiltroLocales').store.count()

      for (i = 0; i < countZonas; i++) {
          var idZona = Ext.getCmp("comboFiltroLocales").store.data.getAt(i).data.IdZona;
          var nombreZona = Ext.getCmp("comboFiltroLocales").store.data.getAt(i).data.NombreZona;
          var capacidad = Ext.getCmp("comboFiltroLocales").store.data.getAt(i).data.Capacidad;
          var latitud = Ext.getCmp("comboFiltroLocales").store.data.getAt(i).data.Latitud;
          var longitud = Ext.getCmp("comboFiltroLocales").store.data.getAt(i).data.Longitud;
          if (idZona > 0) {
              DrawLocal(idZona, nombreZona, capacidad, latitud, longitud);
          }
      }
      /*
      Ext.Ajax.request({
          url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
          success: function (response, opts) {

              var task = new Ext.util.DelayedTask(function () {
                  Ext.Msg.hide();
              });

              task.delay(1500);

          },
          failure: function (response, opts) {
              Ext.Msg.hide();
          }
      });*/
  }

  function DrawHouse(idZona, latitud, longitud, estadoViaje) {

      var image = new google.maps.MarkerImage("Images/house_blue_24x24.png");

      if (estadoViaje == "Finalizado") {
          image = new google.maps.MarkerImage("Images/house_green_24x24.png");
      }
      if (estadoViaje == "Cerrado por Sistema") {
          image = new google.maps.MarkerImage("Images/house_red_24x24.png");
      }

      var point = new google.maps.LatLng(latitud, longitud);

      var markerHouse = new google.maps.Marker({
          position: point,
          icon: image,

          map: map
      });

      arrayHouses.push(markerHouse);
  }

  function eraseHouses() {

      for (i = 0; i < arrayHouses.length; i++) {
          arrayHouses[i].setMap(null);
      }
      arrayHouses = [];
  }

  function MostrarFlota() {

      ClearMap();

      arrayCamionesBackhaul.splice(0, arrayCamionesBackhaul.length);

      var store = Ext.getCmp('gridCamionesCercanosBackhaul').getStore();
      var rowCount = store.count();
      var iterRow = 0;

      while (iterRow < rowCount) {

          var dir = parseInt(store.data.items[iterRow].raw.Direccion);

          var lat = store.data.items[iterRow].raw.Latitud;
          var lon = store.data.items[iterRow].raw.Longitud;

          var Latlng = new google.maps.LatLng(lat, lon);

          arrayCamionesBackhaul.push({
              Fecha: store.data.items[iterRow].raw.UltReporte.toString(),
              EstadoViaje: store.data.items[iterRow].raw.EstadoViaje,
              NroTransporte: store.data.items[iterRow].raw.NroTransporte,
              IdEmbarque: store.data.items[iterRow].raw.IdEmbarque,
              Transportista: store.data.items[iterRow].raw.Transportista,
              Patente: store.data.items[iterRow].raw.Patente,
              Velocidad: store.data.items[iterRow].raw.Velocidad,
              Latitud: lat,
              Longitud: lon,
              LatLng: Latlng,
              NombreOrigen: store.data.items[iterRow].raw.NombreOrigen,
              Destinos: store.data.items[iterRow].raw.Destinos,
              Capacidad: store.data.items[iterRow].raw.CapacidadUnidad,
              PatenteTracto: store.data.items[iterRow].raw.PatenteTracto,
              PatenteTrailer: store.data.items[iterRow].raw.PatenteTrailer,
          });

          var iconRoute;

          if (store.data.items[iterRow].raw.EstadoViaje == 'Liberado') {
              iconRoute = 'Images/Truck_Empty/';
          }
          else {
              iconRoute = 'Images/Truck_Loaded/'
          }

          switch (true) {
              case ((dir >= 338) || (dir < 22)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '1_N_21x29.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 22) && (dir < 67)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '2_NE_32x30.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 67) && (dir < 112)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '3_E_30x22.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 112) && (dir < 157)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '4_SE_30x32.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 157) && (dir < 202)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '5_S_21x29.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 202) && (dir < 247)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '6_SW_30x32.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 247) && (dir < 292)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '7_W_30x22.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
              case ((dir >= 292) && (dir < 338)):
                  marker = new google.maps.Marker({
                      position: Latlng,
                      icon: iconRoute + '8_NW_32x30.png',
                      map: map,
                      labelText: store.data.items[iterRow].raw.Patente + store.data.items[iterRow].raw.UltReporte.toString()
                  });
                  break;
          }

          var label = new Label({
              map: null
          });
          label.bindTo('position', marker, 'position');
          label.bindTo('text', marker, 'labelText');

          google.maps.event.addListener(marker, 'click', function () {
              var latLng = this.position;
              var patFec = this.labelText;

              for (i = 0; i < arrayCamionesBackhaul.length; i++) {
                  if ((arrayCamionesBackhaul[i].Patente + arrayCamionesBackhaul[i].Fecha.toString()) == patFec.toString() & arrayCamionesBackhaul[i].LatLng.toString() == latLng.toString()) {

                      nroTransporte = arrayCamionesBackhaul[i].NroTransporte;
                      idEmbarque = arrayCamionesBackhaul[i].IdEmbarque;
                      patenteTracto = arrayCamionesBackhaul[i].PatenteTracto;
                      patenteTrailer = arrayCamionesBackhaul[i].PatenteTrailer;
                      transportista = arrayCamionesBackhaul[i].Transportista;
                      
                      if (arrayCamionesBackhaul[i].NroTransporte > 0) {
                          var contentString =

                                '<br>' +
                                    '<table>' +
                                      '<tr>' +
                                          '       <td><b>Fecha</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '       <td>' + (arrayCamionesBackhaul[i].Fecha.toString()).replace("T", " ") + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Estado:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].EstadoViaje + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Id. Master:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].NroTransporte + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Línea Transporte:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].Transportista + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Placa:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].Patente + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Origen:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].NombreOrigen + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Destinos:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].Destinos + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Capacidad:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].Capacidad + ' ft' + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><button onclick="NewBackhaul()">Backhaul</button></td>' +
                                      '</tr>' +
                                    '</table>' +
                                  '<br>';
                      }
                      else{
                          var contentString =

                            '<br>' +
                                '<table>' +
                                  '<tr>' +
                                      '       <td><b>Fecha</b></td>' +
                                      '       <td><pre>     </pre></td>' +
                                      '       <td>' + (arrayCamionesBackhaul[i].Fecha.toString()).replace("T", " ") + '</td>' +
                                  '</tr>' +
                                  '<tr>' +
                                      '        <td><b>Estado:</b></td>' +
                                      '       <td><pre>     </pre></td>' +
                                      '        <td>' + arrayCamionesBackhaul[i].EstadoViaje + '</td>' +
                                  '</tr>' +
                                  '<tr>' +
                                      '        <td><b>Línea Transporte:</b></td>' +
                                      '       <td><pre>     </pre></td>' +
                                      '        <td>' + arrayCamionesBackhaul[i].Transportista + '</td>' +
                                  '</tr>' +
                                  '<tr>' +
                                      '        <td><b>Placa:</b></td>' +
                                      '       <td><pre>     </pre></td>' +
                                      '        <td>' + arrayCamionesBackhaul[i].Patente + '</td>' +
                                  '</tr>' +
                                '<tr>' +
                                          '        <td><b>Capacidad:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayCamionesBackhaul[i].Capacidad + ' ft' + '</td>' +
                                    '</tr>' +
                                  '<tr>' +
                                      '        <td><button onclick="NewBackhaul()">Backhaul</button></td>' +
                                  '</tr>' +
                                '</table>' +
                              '<br>';
                        }

                      infowindow.setContent(contentString);
                      infowindow.open(map, this);

                      break;
                  }
              }

          });

          markers.push(marker);
          labels.push(label);

          iterRow++;
      }
  }

  function NewBackhaul()
  {
      if (_editing == true)
      {
          Ext.getCmp('numberNroTransporte').setValue(nroTransporte);
          Ext.getCmp('numberIdEmbarque').setValue(idEmbarque);
          Ext.getCmp("comboFiltroTransportistaBackhaul").setValue(transportista);
          Ext.getCmp("comboFiltroPatenteTractoBackhaul").setValue(patenteTracto);
          Ext.getCmp("comboFiltroPatenteTrailerBackhaul").setValue(patenteTrailer);
          Ext.getCmp("comboFiltroPatenteTractoBackhaul").setDisabled(false);
          Ext.getCmp("comboFiltroPatenteTrailerBackhaul").setDisabled(false);
      }
  
      Ext.getCmp("winCrearViaje").show();
  }

  function GuardarViaje() {
      var flag = true;
      var message = '';

      if (Ext.getCmp('numberNroTransporte').hasActiveError()) {
          return;
      }
      if (Ext.getCmp('numberIdEmbarque').hasActiveError()) {
          return;
      }
      if (Ext.getCmp('comboFiltroPatenteTractoBackhaul').hasActiveError()) {
          return;
      }
      if (Ext.getCmp('comboFiltroPatenteTrailerBackhaul').hasActiveError()) {
          return;
      }
      if (Ext.getCmp('comboZonaOrigen').hasActiveError()) {
          return;
      }
      if (Ext.getCmp('comboZonaProveedor').hasActiveError()) {
          return;
      }
      if (Ext.getCmp('comboZonaDestino').hasActiveError()) {
          return;
      }

      if (!Ext.getCmp('formCrearViaje').getForm().isValid() || !Ext.getCmp("numberNroTransporte").getValue > 0 || !Ext.getCmp("numberIdEmbarque").getValue > 0) {
          return;
      }

    Ext.Ajax.request({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=NuevoViajeBackhaul',
        params: {
            'nroTransporte': Ext.getCmp('numberNroTransporte').getValue(),
            'idEmbarque': Ext.getCmp('numberIdEmbarque').getValue(),
            'rutTransportista': Ext.getCmp('comboFiltroTransportistaBackhaul').getValue(),
            'tracto': Ext.getCmp('comboFiltroPatenteTractoBackhaul').getValue(),
            'trailer': Ext.getCmp('comboFiltroPatenteTrailerBackhaul').getValue(),
            'codDeterminante': Ext.getCmp('comboZonaOrigen').getValue(),
            'codProveedor': Ext.getCmp('comboZonaProveedor').getValue(),
            'codCEDIS': Ext.getCmp("comboZonaDestino").getValue()
        },
        success: function (msg, success) {

            alert(msg.responseText);
            Cancelar();

            Ext.getCmp('gridPanelViajesBackhaul').getStore().load();
            FiltrarViajes()

        },
        failure: function (msg) {
            alert('Se ha producido un error.');
        }
    });
      
  }

  function Cancelar() {

      Ext.getCmp("winCrearViaje").hide();

      Ext.getCmp("numberNroTransporte").setDisabled(false);
      Ext.getCmp("numberIdEmbarque").setDisabled(false);
      Ext.getCmp("comboFiltroPatenteTractoBackhaul").setDisabled(true);
      Ext.getCmp("comboFiltroPatenteTrailerBackhaul").setDisabled(true);
      Ext.getCmp("comboZonaOrigen").setDisabled(false);
      Ext.getCmp("comboZonaDestino").setDisabled(false);

      Ext.getCmp("numberNroTransporte").reset();
      Ext.getCmp("numberIdEmbarque").reset();
      Ext.getCmp("comboFiltroTransportistaBackhaul").reset();
      Ext.getCmp("comboFiltroPatenteTractoBackhaul").reset();
      Ext.getCmp("comboFiltroPatenteTrailerBackhaul").reset();
      Ext.getCmp("comboZonaOrigen").reset();
      Ext.getCmp("comboZonaDestino").reset();

      //FiltrarPatentes();

      _editing = true;

  }

  function MostrarProveedores()
  {
      Ext.Msg.wait('Espere por favor...', 'Generando zonas');

      var store = Ext.getCmp('comboZonaProveedor').store;
      for (var i = 0; i < store.count() ; i++) {

          var idZona = store.getAt(i).data.IdZona;

          if (idZona > 0) {
              DrawZone(idZona);
          }

      }
 
      Ext.Ajax.request({
          url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
          success: function (response, opts) {

              var task = new Ext.util.DelayedTask(function () {
                  Ext.Msg.hide();
              });

              task.delay(1500);

          },
          failure: function (response, opts) {
              Ext.Msg.hide();
          }
      });

  }
        
  function EliminarProveedores()
  {
      for (var i = geoLayer.length -1 ; i >= 0; i--) {
          geoLayer[i].layer.setMap(null);
          geoLayer[i].label.setMap(null);
          geoLayer.splice(i, 1);
      }
  }

  function DrawRoute(origenLat, origenLon, destinoLat, destinoLon)
  {
      var latlonOrigen = new google.maps.LatLng(origenLat, origenLon);
      var latlonDestino = new google.maps.LatLng(destinoLat, destinoLon);

      if (directionsDisplay) {
          directionsDisplay.setDirections({ routes: [] });
      }

      directionsDisplay.setMap(map);

      var rendererOptions = {
          map: map,
          suppressMarkers: true
      }
      directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

      var request = {
          origin: latlonOrigen,
          destination: latlonDestino,
          travelMode: google.maps.TravelMode.DRIVING,
          unitSystem: google.maps.UnitSystem.METRIC,
          provideRouteAlternatives: false,
          region: 'MX'
      };

      directionsService.route(request, function (response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
              directionsDisplay.setOptions({ preserveViewport: true });
              directionsDisplay.setDirections(response);
          }
      });

      // Marcador Inicio Ruta
      var startMarker = new google.maps.Marker({
          position: latlonOrigen,
          map: map,
          icon: new google.maps.MarkerImage("Images/marker_green_32x32.png"),
          animation: google.maps.Animation.DROP
      });
      markers.push(startMarker);

  }

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
