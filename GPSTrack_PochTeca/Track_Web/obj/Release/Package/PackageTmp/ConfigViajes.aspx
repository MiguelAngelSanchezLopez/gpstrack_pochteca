﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfigViajes.aspx.cs" Inherits="Track_Web.ConfigViajes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

  <script type="text/javascript">

    var nameUsuarioConectado = "";
    var cedisAsociado = 0;
    var fechaActual;
    var fechaEstimadaLlegada = "";

    Ext.onReady(function () {

      Ext.QuickTips.init();
      Ext.Ajax.timeout = 600000;
      Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
      Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
      Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

      var textFechaActual = new Ext.form.TextField({
        fieldLabel: 'Fecha y Hora',
        id: 'textFechaActual',
        anchor: '99%',
        labelWidth: 100,
        readOnly: true,
        style: {
          marginLeft: '15px'
        },
      });

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetDiferenciaHoraria',
        success: function (data, success) {

          fechaActual = new Date().addHours(data.responseText);
          curDateTime = new Date().addHours(data.responseText).toISOString();

          var curDate = curDateTime.substring(0, 10);
          var curTime = curDateTime.substring(11, 16)

          Ext.getCmp("textFechaActual").setValue(curDate + " " + curTime);
        }
      });

      var textUsuarioConectado = new Ext.form.TextField({
        fieldLabel: 'Usuario',
        id: 'textUsuarioConectado',
        anchor: '99%',
        labelWidth: 100,
        readOnly: true,
        style: {
          marginLeft: '15px'
        },
      });

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetNameUsuarioConectado',
        success: function (data, success) {
          nameUsuarioConectado = data.responseText;
          Ext.getCmp("textUsuarioConectado").setValue(nameUsuarioConectado);
        }
      });

      var numberNroTransporte = new Ext.form.NumberField({
        fieldLabel: 'Id. GLS',
        id: 'numberNroTransporte',
        allowBlank: false,
        labelWidth: 100,
        anchor: '99%',
        minValue: 1,
        maxValue: 9999999999,
        maxLength: 10,
        enforceMaxLength: true,
        enableKeyEvents: true
      });

      var numberIdEmbarque = new Ext.form.NumberField({
        fieldLabel: 'ID Embarque',
        id: 'numberIdEmbarque',
        allowBlank: false,
        labelWidth: 100,
        anchor: '98%',
        minValue: 1,
        maxValue: 999999,
        maxLength: 6,
        enforceMaxLength: true,
        style: {
          marginLeft: '15px'
        },
        enableKeyEvents: true
      });

      var storeFiltroTransportista = new Ext.data.JsonStore({
          autoLoad: false,
          fields: ['Transporte'],
          proxy: new Ext.data.HttpProxy({
              url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetTransporistasPool',
              headers: {
                  'Content-type': 'application/json'
              }
          })
      });
        /*
      var storeFiltroTransportista = new Ext.data.JsonStore({
        fields: ['Transportista'],
        data: [
            //{ Transportista: 'GOR' },
            //{ Transportista: 'VAQUEROS' },
            { Transportista: 'TRACUSA' }]
      })
        */

      var comboFiltroTransportista = new Ext.form.field.ComboBox({
        id: 'comboFiltroTransportista',
        fieldLabel: 'Línea Tracto',
        forceSelection: true,
        store: storeFiltroTransportista,
        valueField: 'Transporte',
        displayField: 'Transporte',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 100,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: false,
        forceSelection: true,
        listeners: {
          select: function () {
            FiltrarPatentes();
            Ext.getCmp("comboFiltroPatenteTracto").setDisabled(false);
            Ext.getCmp("comboFiltroPatenteTrailer").setDisabled(false);
            Ext.getCmp("comboFiltroPatenteTrailer2").setDisabled(false);
          }
        }
      });

      var storeFiltroPatenteTracto = new Ext.data.JsonStore({
        fields: ['Patente'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=False',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var storeFiltroPatenteTrailer = new Ext.data.JsonStore({
        fields: ['Patente'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=False',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroPatenteTracto = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTracto',
        fieldLabel: 'Placa Tracto',        
        store: storeFiltroPatenteTracto,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 100,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,        
        disabled: true,
        listeners: {
          select: function () {

            var patente = Ext.getCmp("comboFiltroPatenteTracto").getValue();

            Ext.Ajax.request({
              url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
              params: {
                patente: patente
              },
              success: function (data, success) {
                if (data != null) {
                  data = Ext.decode(data.responseText);

                  Ext.getCmp("imageStatusTracto").show();
                  showComentArea(data[0].Estado, '');
                  switch (data[0].Estado) {
                    case "Online":
                      Ext.getCmp('imageStatusTracto').setSrc('Images/status_green_16x16.png');
                      break;
                    default:
                      Ext.getCmp('imageStatusTracto').setSrc('Images/status_red_16x16.png');
                  }

                }
              }
            })
          }
        }
      });

      var imageStatusTracto = Ext.create('Ext.Img', {
        id: 'imageStatusTracto',
        height: 18,
        width: 18,
        style: {
          marginTop: '3px'
        },
        hidden: false
      });

      var comboFiltroPatenteTrailer = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTrailer',
        fieldLabel: 'Placa Remolque',        
        store: storeFiltroPatenteTrailer,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 100,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,        
        disabled: true,
        listeners: {
          select: function () {

            var patente = Ext.getCmp("comboFiltroPatenteTrailer").getValue();

            Ext.Ajax.request({
              url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
              params: {
                patente: patente
              },
              success: function (data, success) {
                if (data != null) {
                  data = Ext.decode(data.responseText);

                  Ext.getCmp("imageStatusTrailer").show();

                  showComentArea('', data[0].Estado);
                  switch (data[0].Estado) {
                    case "Online":
                      Ext.getCmp('imageStatusTrailer').setSrc('Images/status_green_16x16.png');
                      break;
                    default:
                      Ext.getCmp('imageStatusTrailer').setSrc('Images/status_red_16x16.png');
                  }

                }
              }
            })
          }
        }
      });

      var imageStatusTrailer = Ext.create('Ext.Img', {
        id: 'imageStatusTrailer',
        height: 18,
        width: 18,
        style: {
          marginTop: '3px'
        },
        hidden: false
      });

      var comboFiltroPatenteTrailer2 = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatenteTrailer2',
        fieldLabel: 'Segundo Remolque',
        forceSelection: true,
        store: storeFiltroPatenteTrailer,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 120,
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        forceSelection: true,
        disabled: true,
        hidden: true,
        listeners: {
          select: function () {

            var patente = Ext.getCmp("comboFiltroPatenteTrailer2").getValue();

            Ext.Ajax.request({
              url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
              params: {
                patente: patente
              },
              success: function (data, success) {
                if (data != null) {
                  data = Ext.decode(data.responseText);

                  Ext.getCmp("imageStatusTrailer2").show();

                  switch (data[0].Estado) {
                    case "Online":
                      Ext.getCmp('imageStatusTrailer2').setSrc('Images/status_green_16x16.png');
                      break;
                    default:
                      Ext.getCmp('imageStatusTrailer2').setSrc('Images/status_red_16x16.png');
                  }

                }
              }
            })
          }
        }
      });

      var imageStatusTrailer2 = Ext.create('Ext.Img', {
        id: 'imageStatusTrailer2',
        height: 18,
        width: 18,
        style: {
          marginTop: '3px'
        },
        hidden: false
      });

      var textAreaObservaciones = new Ext.form.field.TextArea({
        id: 'textAreaObservaciones',
        disabled: true,
        anchor: '100% 90%',
        maxLength: 500,
        emptyText: 'Ingrese un comentario'
      });



      var storeFiltroTipoDestino = new Ext.data.JsonStore({
        fields: ['IdZona', 'TipoDestino'],
        data: [{ "IdZona": 2, "TipoDestino": "Tienda" },
            //{ "IdZona": 5, "TipoDestino": "Proveedor" },
            //{ "IdZona": 1, "TipoDestino": "CEDIS" }
        ]
      });

      var comboFiltroTipoDestino = new Ext.form.field.ComboBox({
        id: 'comboFiltroTipoDestino',
        fieldLabel: 'Tipo Destino',
        forceSelection: true,
        editable: false,
        store: storeFiltroTipoDestino,
        valueField: 'IdZona',
        displayField: 'TipoDestino',
        emptyText: 'Seleccione...',
        queryMode: 'local',
        anchor: '99%',
        labelWidth: 100,
        listeners: {
            select: function () {

            var _cedis = Ext.getCmp('comboFiltroCedis').getRawValue();

            storeZonasDestino.load({
              params: {
                cedis: _cedis
              }
            });
            Ext.getCmp("comboZonaDestino").setDisabled(false);
            Ext.getCmp("dateCita").setDisabled(false);
            Ext.getCmp("hourCita").setDisabled(false);
            Ext.getCmp("btnAddDestino").setDisabled(true);
            Ext.getCmp("comboZonaDestino").reset();
            Ext.getCmp("dateCita").reset();
            Ext.getCmp("hourCita").reset();

          }
        }
      });

      var storeFiltroCedis = new Ext.data.JsonStore({
          autoLoad: false,
          fields: ['DeterminanteCEDIS', 'Cedis'],
          proxy: new Ext.data.HttpProxy({
              url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllCedisPool&Todas=False',
              headers: {
                  'Content-type': 'application/json'
              }
          })
      });

      var comboFiltroCedis = new Ext.form.field.ComboBox({
          id: 'comboFiltroCedis',
          fieldLabel: 'Cedis',
          labelWidth: 100,
          store: storeFiltroCedis,
          valueField: 'DeterminanteCEDIS',
          displayField: 'Cedis',
          queryMode: 'local',
          anchor: '99%',
          emptyText: 'Seleccione...',
          enableKeyEvents: true,
          editable: false,
          listConfig: {
              loadingText: 'Buscando...',
              getInnerTpl: function () {
                  return '<a class="search-item">' +
                      '<span>Determinante: {DeterminanteCEDIS}</span><br />' +
                      '<span>CEDIS: {Cedis}</span>' +
                      '</a>';
              }
          },
          listeners: {
              change: function (field, newVal) {
                  FiltrarTransportistas();
              }
          }
      });

      Ext.getCmp('comboFiltroCedis').store.load({
          param: {
              transportista: 'Todos'
          },
          callback: function (r, options, success) {
              if (success) {
                  
                  Ext.Ajax.request({
                      url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetCedisAsociado',
                      success: function (data, success) {
                          cedisAsociado = parseInt(data.responseText);

                          if (cedisAsociado > 0) {
                              Ext.getCmp("comboFiltroCedis").setValue(cedisAsociado);
                              Ext.getCmp("comboFiltroCedis").setReadOnly(true);

                              storeFiltroConductores.load({
                                  params: {
                                      CEDIS: cedisAsociado,
                                  }
                              })

                          }
                      }
                  });
                  
                  var firstceids = Ext.getCmp("comboFiltroCedis").store.getAt(0).get("DeterminanteCEDIS");
                  Ext.getCmp("comboFiltroCedis").setValue(firstceids);
              }
          }
      })
      
      var storeZonasDestino = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxZonas.aspx?Metodo=getTiendas',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        }),
        helper: function (value) {
          if (Ext.isEmpty(value, false)) {
            return false;
          }
          value = this.data.createValueMatcher(value, true, false);
          return function (r) { return value.test(r.data['IdZona']) || value.test(r.data['NombreZona']); };
        },
        filter: function (property, value, anyMatch, caseSensitive) {
          var fn = this.helper(value);
          return fn ? this.filterBy(fn) : this.clearFilter();
        }
      });

      var comboZonaDestino = new Ext.form.field.ComboBox({
        id: 'comboZonaDestino',
        fieldLabel: 'Destino',
        allowBlank: false,
        store: storeZonasDestino,
        valueField: 'IdZona',
        displayField: 'NombreZona',
        queryMode: 'local',
        anchor: '99%',
        allowEmpty: false,
        forceSelection: true,
        enableKeyEvents: true,
        editable: true,
        labelWidth: 100,
        emptyText: 'Seleccione...',
        disabled: true,
        listConfig: {
          loadingText: 'Buscando...',
          getInnerTpl: function () {
            return '<a class="search-item">' +
                              '<span>Id Zona: {IdZona}</span><br />' +
                              '<span>Nombre: {NombreZona}</span>' +
                          '</a>';
          }
        },
        listeners: {
          select: function () {
            Ext.getCmp("btnAddDestino").setDisabled(false);
          }
        }

      });

      var storeDestinos = new Ext.data.JsonStore({
        fields: ['IdZona', 'IdTipoDestino', 'NombreTipoDestino', 'NombreDestino', 'LlegadaEstimada', 'EstadoVentana']
      });

      var gridPanelDestinos = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelDestinos',
        store: storeDestinos,
        width: 405,
        height: 110,
        columnLines: true,
        scroll: false,
        style: {
          marginTop: '1px'
        },
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columns: [
            { text: 'Código', sortable: true, width: 45, dataIndex: 'IdZona' },
            { text: 'Tipo', sortable: true, width: 65, dataIndex: 'NombreTipoDestino' },
            { text: 'Nombre', sortable: true, flex: 1, dataIndex: 'NombreDestino' },
            
            {
              xtype: 'actioncolumn',
              width: 23,
              editor: false,
              items: [
                  {
                    icon: 'Images/delete.png',
                    tooltip: 'Eliminar',
                    handler: function (grid, rowIndex, colIndex) {
                      var row = grid.getStore().getAt(rowIndex);
                      grid.getStore().remove(row);
                    }
                  }]
            }
        ]
      });

      var dateCita = new Ext.form.DateField({
          id: 'dateCita',
          fieldLabel: 'Cita',
          labelWidth: 100,
          allowBlank: false,
          anchor: '99%',
          format: 'd-m-Y',
          editable: false,
          value: new Date(),
          minValue: new Date(),
          disabled: true
      });

      var hourCita = {
          xtype: 'timefield',
          id: 'hourCita',
          allowBlank: false,
          format: 'H:i',
          minValue: '00:00',
          maxValue: '23:59',
          increment: 10,
          anchor: '99%',
          editable: true,
          value: '00:00',
          disabled: true,
          style: {
              marginTop: '4px'
          },
      };

      var btnAddDestino = {
        id: 'btnAddDestino',
        xtype: 'button',
        iconAlign: 'left',
        text: 'Agregar',
        icon: 'Images/add_blue_16x15.png',
        width: 70,
        height: 23,
        disabled: true,
        handler: function () {

          var idZona = Ext.getCmp("comboZonaDestino").getValue();
          var idTipoDestino = Ext.getCmp("comboFiltroTipoDestino").getValue();
          var nombreTipoDestino = Ext.getCmp("comboFiltroTipoDestino").getRawValue();
          var nombreDestino = Ext.getCmp("comboZonaDestino").getRawValue();

          var record = storeDestinos.findRecord('IdZona', idZona);

          if (record == null) {

            var recordDestino = storeZonasDestino.findRecord('IdZona', Ext.getCmp("comboZonaDestino").getValue());

            latDestino = recordDestino.data.Latitud;
            lonDestino = recordDestino.data.Longitud;

            storeDestinos.add({
              IdZona: idZona,
              IdTipoDestino: idTipoDestino,
              NombreTipoDestino: nombreTipoDestino,
              NombreDestino: nombreDestino
            });

          }
        }
      };

      var chkIsFull = new Ext.form.Checkbox({
        id: 'chkIsFull',
        fieldLabel: 'Viaje Full',
        labelWidth: 70,
        width: 100,
        checked: false,
        listeners: {
          change: function (cb, checked) {
            if (checked == true) {
              Ext.getCmp("comboFiltroPatenteTrailer2").show();
            }
            else {
              Ext.getCmp("comboFiltroPatenteTrailer2").hide();
              Ext.getCmp("imageStatusTrailer2").hide();
            }
          }
        }
      });

      var storeFiltroConductores = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['RutConductor'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetConductoresCEDIS&Todos=False',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroConductores = new Ext.form.field.ComboBox({
        id: 'comboFiltroConductores',
        fieldLabel: 'Operador',
        allowBlank: true,
        store: storeFiltroConductores,
        valueField: 'RutConductor',
        displayField: 'RutConductor',
        forceSelection: true,
        queryMode: 'local',
        anchor: '99%',
        enableKeyEvents: true,
        editable: true,
        labelWidth: 100,
        emptyText: 'Seleccione...'
      });

      var textNombreConductorOpcional = new Ext.form.field.ComboBox({
        id: 'textNombreConductorOpcional',
        fieldLabel: 'Operador',
        allowBlank: true,
        store: storeFiltroConductores,
        valueField: 'RutConductor',
        displayField: 'RutConductor',
        forceSelection: true,
        queryMode: 'local',
        anchor: '99%',
        enableKeyEvents: true,
        editable: false,
        labelWidth: 100,
        emptyText: 'Seleccione...'
      });

      var btnCrearViaje = {
        id: 'btnCrearViaje',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/add_blue_20x19.png',
        text: 'Nuevo viaje',
        width: 100,
        height: 27,
        handler: function () {
            winCrearViaje.show();

        }
      };

      var storeViajesAsignados = new Ext.data.JsonStore({
        autoLoad: true,
        fields: [
            'IdViaje',
            'NroTransporte',
            'IdEmbarque',
            'SecuenciaDestino',
            'RutConductor',
            'NombreConductor',
            'PatenteTracto',
            'PatenteTrailer',
            'RutTransportista',
            'NombreTransportista',
            'CodigoOrigen',
            'NombreOrigen',
            'CodigoDestino',
            'NombreDestino',
            'Comentarios',
             { name: 'FechaAsignacion', type: 'date', dateFormat: 'c' }
        ],
        proxy: new Ext.data.HttpProxy({
          url: 'Ajaxpages/AjaxViajes.aspx?Metodo=GetViajesAsignados',
          reader: { type: 'json', root: 'd' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var gridViajesAsignados = Ext.create('Ext.grid.Panel', {
        id: 'gridViajesAsignados',
        title: 'Viajes asignados',
        hideCollapseTool: true,
        anchor: '100% 99%',
        buttons: [btnCrearViaje],
        store: storeViajesAsignados,
        scroll: false,
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        columnLines: true,
        columns: [{ text: 'Id. GLS', width: 100, sortable: true, dataIndex: 'NroTransporte' },
                    { text: 'Id Embarque', width: 100, sortable: true, dataIndex: 'IdEmbarque' },
                    { text: 'Operador', flex: 1, sortable: true, dataIndex: 'NombreConductor' },
                    { text: 'Tracto', sortable: true, width: 80, dataIndex: 'PatenteTracto' },
                    { text: 'Trailer', sortable: true, width: 80, dataIndex: 'PatenteTrailer' },
                    { text: 'Origen', flex: 1, sortable: true, dataIndex: 'NombreOrigen' },
                    { text: 'Destino', flex: 1, sortable: true, dataIndex: 'NombreDestino' },
                    { text: 'Comentarios', flex: 1, sortable: true, dataIndex: 'Comentarios' },
                    { text: 'Fecha asignación', width: 110, sortable: true, dataIndex: 'FechaAsignacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') }
               ]
      });

      var formCrearViaje = new Ext.FormPanel({
        id: 'formCrearViaje',
        border: false,
        frame: true,
        width: 450,
        height: 600,
        items: [textFechaActual, textUsuarioConectado,
            {
              xtype: 'fieldset',
              title: 'Información de Embarque',
              style: {
                marginLeft: '5px',
                marginRight: '5px'
              },
              layout: 'column',
              anchor: '100% 19%',
              items: [
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.5,
                items: [numberNroTransporte]
              },
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.5,
                items: [numberIdEmbarque]
              },
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 1,
                items: [comboFiltroCedis]
              },
              {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 1,
                items: [comboFiltroTransportista]
              },
              ]
            },
            {
              xtype: 'fieldset',
              title: 'Detalle de Destino',
              style: {
                marginLeft: '5px',
                marginRight: '5px'
              },
              anchor: '100% 39%',
              layout: 'column',
              items: [{
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 1,
                items: [comboFiltroTipoDestino]
              },
                      {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.82,
                        items: [comboZonaDestino, dateCita]
                      },
                      {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 0.18,
                        items: [btnAddDestino, hourCita]
                      },
                      {
                        xtype: 'container',
                        layout: 'anchor',
                        columnWidth: 1,
                        items: [gridPanelDestinos]
                      }

              ]
            },
            {
              xtype: 'fieldset',
              title: 'Información de placas',
              style: {
                marginLeft: '5px',
                marginRight: '5px'
              },
              layout: 'column',
              anchor: '100% 33%',
              items: [{
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.95,
                items: [comboFiltroPatenteTracto]
              },
                          {
                            xtype: 'container',
                            layout: 'anchor',
                            columnWidth: 0.05,
                            items: [imageStatusTracto]
                          },
                          {
                            xtype: 'container',
                            layout: 'anchor',
                            columnWidth: 0.95,
                            items: [comboFiltroPatenteTrailer]
                          },
                          {
                            xtype: 'container',
                            layout: 'anchor',
                            columnWidth: 0.05,
                            items: [imageStatusTrailer]
                          },
                          {
                            xtype: 'container',
                            layout: 'anchor',
                            columnWidth: 1,
                            items: [textAreaObservaciones]
                          },
                        {
                          xtype: 'container',
                          layout: 'anchor',
                          columnWidth: 0.26,
                          items: [chkIsFull]
                        },
                        {
                          xtype: 'container',
                          layout: 'anchor',
                          columnWidth: 0.69,
                          items: [comboFiltroPatenteTrailer2]
                        },
                        {
                          xtype: 'container',
                          layout: 'anchor',
                          columnWidth: 0.05,
                          items: [imageStatusTrailer2]
                        },
                        {
                          xtype: 'container',
                          layout: 'anchor',
                          columnWidth: 1,
                          items: [comboFiltroConductores]
                        }
              ]
            },
        ]
      });

      var btnGuardar = {
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/save_black_20x20.png',
        text: 'Guardar',
        width: 90,
        height: 26,
        handler: function () {
          GuardarViaje();
        }
      };

      var btnCancelar = {
        id: 'btnCancelar',
        xtype: 'button',
        width: 90,
        height: 26,
        iconAlign: 'left',
        icon: 'Images/back_black_20x20.png',
        text: 'Cancelar',
        handler: function () {
          Cancelar();
        }
      };

      var winCrearViaje = new Ext.Window({
        id: 'winCrearViaje',
        title: 'Datos del viaje',
        closeAction: 'hide',
        modal: true,
        items: formCrearViaje,
        resizable: false,
        border: false,
        constrain: true,
        buttons: [btnGuardar, btnCancelar]
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        items: [gridViajesAsignados]

      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, centerPanel]
      });

    });

  </script>

  <script type="text/javascript">

    function Cancelar() {

      Ext.getCmp("winCrearViaje").hide();

      Ext.getCmp("numberNroTransporte").setDisabled(false);
      Ext.getCmp("numberIdEmbarque").setDisabled(false);

      Ext.getCmp("comboFiltroPatenteTracto").setDisabled(true);
      Ext.getCmp("comboFiltroPatenteTrailer").setDisabled(true);
      Ext.getCmp("comboFiltroPatenteTrailer2").setDisabled(true);

      Ext.getCmp("imageStatusTracto").hide();
      Ext.getCmp("imageStatusTrailer").hide();
      Ext.getCmp("imageStatusTrailer2").hide();

      Ext.getCmp("comboZonaDestino").setDisabled(true);
      Ext.getCmp("dateCita").setDisabled(true);
      Ext.getCmp("hourCita").setDisabled(true);
      Ext.getCmp("btnAddDestino").setDisabled(true);

      Ext.getCmp("numberNroTransporte").reset();
      Ext.getCmp("numberIdEmbarque").reset();
      Ext.getCmp("comboFiltroPatenteTracto").reset();
      Ext.getCmp("comboFiltroPatenteTrailer").reset();
      Ext.getCmp("comboFiltroPatenteTrailer2").reset();
      Ext.getCmp("comboFiltroTipoDestino").reset();
      Ext.getCmp("comboZonaDestino").reset();
      Ext.getCmp("dateCita").reset();
      Ext.getCmp("hourCita").reset();
      Ext.getCmp("comboFiltroConductores").reset();
      Ext.getCmp('textAreaObservaciones').reset();
      Ext.getCmp('textAreaObservaciones').setDisabled(true);

      Ext.getCmp('gridPanelDestinos').getStore().removeAll();
    }

    function FiltrarPatentes() {
      var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

      var storeTracto = Ext.getCmp('comboFiltroPatenteTracto').store;
      storeTracto.load({
        params: {
          transportista: transportista
        }
      });

      var storeTrailer = Ext.getCmp('comboFiltroPatenteTrailer').store;
      storeTrailer.load({
        params: {
          transportista: "SIMSA"
        }
      });

      Ext.getCmp("comboFiltroPatenteTracto").setDisabled(false);
      Ext.getCmp("comboFiltroPatenteTrailer").setDisabled(false);
      Ext.getCmp("comboFiltroPatenteTrailer2").setDisabled(false);

    }

    function ValidarNroTransporte() {
      Ext.Ajax.request({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=ValidarNroTransporte',
        params: {
          nroTransporte: Ext.getCmp('numberNroTransporte').getValue()
        },
        success: function (data, success) {
          if (data != null) {
            data = (data.responseText.toLowerCase() == 'true');
            if (!data) {
              Ext.getCmp('numberNroTransporte').markInvalid("El ID embarque ingresado se encuentra repetido.");
            }
            else {
              Ext.getCmp('numberNroTransporte').clearInvalid();
            }
          }
        },
        failure: function (msg) {
          alert('Se ha producido un error.');
        }
      });
    }


    function GuardarViaje() {
      var flag = true;
      var message = '';
      var _vertices = new Array();

      if (Ext.getCmp('numberNroTransporte').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboFiltroPatenteTracto').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboFiltroPatenteTrailer').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboFiltroCedis').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboZonaDestino').hasActiveError()) {
        return;
      }
      if (Ext.getCmp('comboFiltroConductores').getValue() == null) {
          return;
      }
      

      if (!Ext.getCmp('formCrearViaje').getForm().isValid() || !Ext.getCmp("numberNroTransporte").getValue > 0 || !Ext.getCmp("numberIdEmbarque").getValue > 0) {
        return;
      }

      var listaDestinos = "";

      var store = Ext.getCmp('gridPanelDestinos').getStore();
      if (store.count() == 0)
      {
          return;
      }

      for (var i = 0; i < store.count() ; i++) {
        listaDestinos = listaDestinos + store.data.items[i].data.IdZona

        if (i < store.count() - 1) {
          listaDestinos = listaDestinos + ";"
        }

      }

      var dateCita = Ext.getCmp('dateCita').getValue();
      var hourCita = Ext.getCmp('hourCita').getRawValue();

      Ext.Ajax.request({
        url: 'AjaxPages/AjaxViajes.aspx?Metodo=NuevoViaje',
        params: {
          'nroTransporte': Ext.getCmp('numberNroTransporte').getValue(),
          'idEmbarque': Ext.getCmp('numberIdEmbarque').getValue(),
          'transportista': Ext.getCmp('comboFiltroTransportista').getValue(),
          'tracto': Ext.getCmp('comboFiltroPatenteTracto').getValue(),
          'trailer': Ext.getCmp('comboFiltroPatenteTrailer').getValue(),
          'rutConductor': Ext.getCmp('comboFiltroConductores').getValue(),
          'codOrigen': Ext.getCmp('comboFiltroCedis').getValue(),
          'listaDestinos': listaDestinos,
          'comentarios': Ext.getCmp('textAreaObservaciones').getValue(),
          'dateCita': dateCita,
          'hourCita': hourCita
        },
        success: function (msg, success) {
          alert(msg.responseText);

          if (msg.responseText == "Viaje ingresado correctamente.") {
            Cancelar();
            Ext.getCmp('gridViajesAsignados').getStore().load();
          }

        },
        failure: function (msg) {
          alert('Se ha producido un error.');
        }
      });
    }

    function CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon) {

      var service = new google.maps.DistanceMatrixService();
      var origen = new google.maps.LatLng(estadoLat, estadoLon);
      var destino = new google.maps.LatLng(destinoLat, destinoLon);

      service.getDistanceMatrix(
          {
            origins: [origen],
            destinations: [destino],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
          }, callback);
    }

    function callback(response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK) {

        var distance = response.rows[0].elements[0].distance.text;
        var time = response.rows[0].elements[0].duration.value / 60;

        fechaEstimadaLlegada = addMinutes(fechaActual, time).toISOString();

        //Ext.getCmp("gridPanelDestinos").getView().refresh()
      }
    }

    Date.prototype.addHours = function (h) {
      this.setTime(this.getTime() + (h * 60 * 60 * 1000));
      return this;
    }

    function addMinutes(date, minutes) {
      return new Date(date.getTime() + minutes * 60000);
    }

    var renderIconVentana = function (val) {
      if (val == 1) {
        return '<img data-qtip="Llegará en ventana horaria." src="Images/status_green_16x16.png">';
      }
      if (val == 0) {
        return '<img data-qtip="No llegará en ventana horaria." src="Images/status_red_16x16.png">';
      }

    };

    function showComentArea(Tracto, Trailer) {

      var patente;

      if (Tracto == '') {
        patente = Ext.getCmp("comboFiltroPatenteTracto").getValue();
      } else {
        patente = Ext.getCmp("comboFiltroPatenteTrailer").getValue();
      }

      if (patente == null || patente == '') {
        if (Tracto == '') {
          if (Trailer == 'Online') {
            Ext.getCmp('textAreaObservaciones').setValue('');
            Ext.getCmp('textAreaObservaciones').setDisabled(true);
          }
          else {
            Ext.getCmp('textAreaObservaciones').setDisabled(false);
          }
        }
        else {
          if (Tracto == 'Online') {
            Ext.getCmp('textAreaObservaciones').setValue('');
            Ext.getCmp('textAreaObservaciones').setDisabled(true);
          }
          else {
            Ext.getCmp('textAreaObservaciones').setDisabled(false);
          }
        }
      }
      else {
        Ext.Ajax.request({
          url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
          autoSync: true,
          params: {
            patente: patente
          },
          success: function (data, success) {
            if (data != null) {
              data = Ext.decode(data.responseText);
              var estado = data[0].Estado;

              if (estado == 'Placa no integrada')
              {
                estado = 'Offline';
              }

              if (Tracto == '') {
                if ((estado == 'Online') && (Trailer == 'Online')) {
                  Ext.getCmp('textAreaObservaciones').setValue('');
                  Ext.getCmp('textAreaObservaciones').setDisabled(true);
                }

                if ((estado == 'Offline') || (Trailer == 'Offline')) {
                  Ext.getCmp('textAreaObservaciones').setDisabled(false);
                }
              } else {
                if ((estado == 'Online') && (Tracto == 'Online')) {
                  Ext.getCmp('textAreaObservaciones').setValue('');
                  Ext.getCmp('textAreaObservaciones').setDisabled(true);
                }
                if ((estado == 'Offline') || (Tracto == 'Offline')) {
                  Ext.getCmp('textAreaObservaciones').setDisabled(false);

                }
              }
            }
          }
        })

      }

      }

      function FiltrarTransportistas() {
          var _cedis = Ext.getCmp('comboFiltroCedis').getRawValue();
            
          var store = Ext.getCmp('comboFiltroTransportista').store;
          store.load({
              params: {
                  cedis: _cedis
              },
              callback: function (r, options, success) {
                  if (success) {
                      Ext.getCmp("comboFiltroTransportista").reset();

                      if (_cedis == "CEDIS San Martín Obispo") {
                          Ext.getCmp("comboFiltroTransportista").setValue("TRCU");
                          Ext.getCmp("comboFiltroTransportista").setReadOnly(true);

                          FiltrarPatentes();
                      }
                      else
                      {
                          Ext.getCmp("comboFiltroTransportista").setReadOnly(false);
                      }

                  }
              }
          });
      }

  </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>
