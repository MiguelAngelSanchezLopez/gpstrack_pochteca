﻿var Columnas =
    {
        Id: 0,
        Usuario: 1,
        Tablero: 2,
        Perfil: 3,
        FechaCreacion: 4,
        Acciones: 5
    };

var elementoTableroPorUsuarioTemporal = new Object();

var objetoJSONTablerosPorUsuario = new Object();

var objetoJSONPerfilesPorUsuario = new Object();

var objetoJSONTableros = new Object();

var objetoJSONPerfiles = new Object();

var objetoJSONUsuarios = new Object();

var idRegistroOperacionTemporal = "";

var filtroId = "";

var filtroIdTablero = "";

var arregloCamposFiltros =
    [
        "txtId",
        "txtIdTablero",
        "txtIdPerfil",
        "txtIdUsuario"
    ];

var arregloCamposValidacion =
    [
        "txtIdTableroAdm",
        "txtIdPerfilAdm",
        "txtIdUsuarioAdm"
    ];

var IdPerfilRemovido = 0;
var listadoArregloTablerosTemporal = [];
var arregloTablerosTemporal = [];

var IdsTablerosSeleccionadas = [];
var IdsPerfilesSeleccionados = [];
var IdsUsuariosSeleccionados = [];

function LimpiarElementoTableroPorUsuarioTemporal() {
    elementoTableroPorUsuarioTemporal = {
        "Id": "0",
        "IdUsuario": "0",
        "IdTablero": "0",
        "FechaCreacion": "01/01/0001",
        "Activo": "False"
    };
}

function LlenarElementoTableroPorUsuarioTemporal() {
    ValidarCamposInformacion();

    elementoTableroPorUsuarioTemporal = {
        "Id": idRegistroOperacionTemporal,
        "IdUsuario": $("#txtIdUsuarioAdm").val(),
        "IdTablero": $("#txtIdTableroAdm").val(),
        "FechaCreacion": convertirFechaAFormatoMMDDYYYY("01/01/0001"),
        "Activo": "True"
    };

    $("#" + CONTENEDOR_ASPNET + "hidden_JSONTableroPorUsuarioTemporal").val(JSON.stringify(elementoTableroPorUsuarioTemporal));
}

function LlenarSelectUsuarios() {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        var ApellidosTemporal = "";
        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                IdTemporal = value;

            if (key == "Nombre")
                NombreTemporal = value;

            if (key == "Apellidos")
                ApellidosTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + " " + ApellidosTemporal + "</option>");

    });

    $("#usuariosSelect").html(elementosSelectGeneral.join(""));
    $("#usuariosSelectAdm").html(elementosSelectGeneral.join(""));
}

function LlenarSelectTableros() {
    LlenarSelectTablerosGeneral(objetoJSONTableros, "TablerosSelect");
    LlenarSelectTablerosGeneral(objetoJSONTableros, "TablerosSelectAdm");
}

function LlenarSelectTablerosGeneral(fuenteJSON, nombreSelect) {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(fuenteJSON, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "NombreCorto")
                NombreTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + "</option>");

    });

    $("#" + nombreSelect).html(elementosSelectGeneral.join(""));
}

function LlenarSelectPerfiles() {
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelect");
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelectAdm");
}

function ObtenerNombreTableroPorId(idTableroBuscado) {
    return ObtieneDatoPorId(idTableroBuscado, objetoJSONTableros, "item.NombreCorto", "Id");
}

function ObtenerNombrePerfilPorId(idPerfilBuscado) {
    return ObtieneDatoPorId(idPerfilBuscado, objetoJSONPerfiles, "item.Nombre", "Id");
}

function ObtenerNombreCompletoUsuarioPorId(idUsuarioBuscado) {
    return ObtieneDatoPorId(idUsuarioBuscado, objetoJSONUsuarios, "item.Nombre +' ' + item.Apellidos", "IdUsuario");
}

function ObtenerDatosPorId(idRegistroBuscado) {
    $.each(objetoJSONTablerosPorUsuario, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    ObtenerRegistroCompleto(item);

        });
    });
}

function LimpiarCamposAdministracion() {

    $("#txtIdAdm").val("");
    $("#txtIdTableroAdm").val("");
    $("#txtIdPerfilAdm").val("");
    $("#txtIdUsuarioAdm").val("");

    ResetearSelects();
}

function ObtenerRegistroCompleto(elemento) {
    idRegistroOperacionTemporal = elemento.Id;

    $("#txtIdAdm").val(elemento.Id);
    $("#txtIdTableroAdm").val(elemento.IdTablero);

    $("#txtIdPerfilAdm").val(elemento.IdPerfil);

    $("#txtIdUsuarioAdm").val(elemento.IdUsuario);

    $('#usuariosSelectAdm').val(elemento.IdUsuario).trigger('change');

    $("#txtActivoAdm").val(elemento.Activo);

    $("#cbActivoAdm").prop('checked', ObtenerBooleanoDeCadena(elemento.Activo));
}

function AsignarComportamientoChangePerfilesSelectAdm() {

    $("#perfilesSelectAdm").on("select2:selecting", function (e) {
        var IdPerfilAgregado = e.params.args.data.id;
        var IdsTablerosPorPerfilEncontrados = ObtenerIdsTablerosPorIdPerfil(IdPerfilAgregado);

        console.log("IdPerfilAgregado:" + IdPerfilAgregado);
        console.log("IdsTablerosPorPerfilEncontrados:" + IdsTablerosPorPerfilEncontrados);

        SeleccionarAutomaticamenteTablerosRelacionadasAlPerfil(IdsTablerosPorPerfilEncontrados);
    });

    $("#perfilesSelectAdm").on("change.select2", function () {
        var IdsTablerosPorPerfilEncontrados = ObtenerIdsTablerosPorIdPerfil(this.value);
        SeleccionarAutomaticamenteTablerosRelacionadasAlPerfil(IdsTablerosPorPerfilEncontrados);
    });
}

function RemoverTablerosRelacionadasAlPerfilRemovido(IdPerfilRemovido) {
    var IdsTablerosARemover = ObtenerIdsTablerosPorIdPerfil(IdPerfilRemovido);

    for (var indice = 0; indice < IdsTablerosARemover.length; indice++) {
        var opcionBuscada = $('#TablerosSelectAdm option[value="' + IdsTablerosARemover[indice] + '"]');
        opcionBuscada.prop('selected', false);
    }
    $('#TablerosSelectAdm').trigger('change.select2');

}

function AsignarComportamientoQuitarPerfilesSelectAdm() {

    $('#perfilesSelectAdm').on("select2:unselecting", function (e) {
        IdPerfilRemovido = e.params.args.data.id;
    }).trigger('change');

    $('#perfilesSelectAdm').on("select2:unselect", function (e) {
        RemoverTablerosRelacionadasAlPerfilRemovido(IdPerfilRemovido);
    }).trigger('change');

}

function EliminarTablerosConIndexCero(IdsTablerosPorPerfil) {
    var nuevoArreglo = [];

    for (var indice = 0; indice < IdsTablerosPorPerfil.length; indice++)
        if (IdsTablerosPorPerfil[indice] != 0)
            nuevoArreglo.push(IdsTablerosPorPerfil[indice]);

    return nuevoArreglo;
}

function SeleccionarAutomaticamenteTablerosRelacionadasAlPerfil(IdsTablerosPorPerfilEncontrados) {
    var TablerosSeleccionadasAnteriormente = $('#TablerosSelectAdm').val();

    var todosIdsTablerosPorPerfil = [];

    todosIdsTablerosPorPerfil = TablerosSeleccionadasAnteriormente.concat(IdsTablerosPorPerfilEncontrados);

    todosIdsTablerosPorPerfil = EliminarTablerosConIndexCero(todosIdsTablerosPorPerfil);

    $('#TablerosSelectAdm').val(todosIdsTablerosPorPerfil).trigger('change');
}

function ObtenerIdsTablerosPorIdPerfil(idPerfilBuscado) {
    var nombrePerfilEncontrado = "";
    var IdsTablerosPorPerfilEncontrados = "";
    var perfilEncontrado = false;

    //console.log("objetoJSONPerfiles");
    //console.log(JSON.stringify(objetoJSONPerfiles));

    $.each(objetoJSONPerfiles, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idPerfilBuscado) {
                    nombrePerfilEncontrado = item.Nombre;
                    perfilEncontrado = true;
                }


            if (perfilEncontrado)
                if (key == "IdsTablerosPorPerfil") {
                    //console.log("item.IdsTablerosPorPerfil -->" + item.IdsTablerosPorPerfil);
                    IdsTablerosPorPerfilEncontrados = item.IdsTablerosPorPerfil;
                    perfilEncontrado = false;
                }

        });
    });

    return IdsTablerosPorPerfilEncontrados;
}

function ObtenerTodosPerfiles(IdUsuarioTemporal) {
    var arregloPerfiles = [];

    $.each(objetoJSONPerfilesPorUsuario, function (key, value) {
        item = value;

        var usuarioCoincidente = false;

        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                if (value == IdUsuarioTemporal)
                    usuarioCoincidente = true;

            if (key == "IdPerfil")
                if (usuarioCoincidente)
                    arregloPerfiles.push(ObtenerNombrePerfilPorId(value));

        });
    });
    return arregloPerfiles.join(",");
}

function ObtenerTodosIdsPerfiles(IdUsuarioTemporal) {
    var arregloPerfiles = [];

    $.each(objetoJSONPerfilesPorUsuario, function (key, value) {
        item = value;

        var usuarioCoincidente = false;

        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                if (value == IdUsuarioTemporal)
                    usuarioCoincidente = true;

            if (key == "IdPerfil")
                if (usuarioCoincidente)
                    arregloPerfiles.push(value);

        });
    });
    return arregloPerfiles.join(",");
}

function ConsumirObjetoJSON() {

    var elementosTabla = [];

    var usuarioRepetido = false;
    var idUsuarioRepetido = 0;

    var IdTemporal = "";
    var IdUsuarioTemporal = "";
    var IdTableroTemporal = "";
    var FechaCreacionTemporal = "";
    var PerfilesTemporal = "";
    var TablerosPorUsuario = "";
    var NombresTablerosPorUsuarioTemporal = "";

    $.each(objetoJSONTablerosPorUsuario, function (key, value) {
        item = value;

        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "IdTablero")
                IdTableroTemporal = value;

            if (key == "FechaCreacion")
                FechaCreacionTemporal = CrearFechaFormatoDDMMYYYY(value);

            if (key == "IdUsuario") {
                IdUsuarioTemporal = value;

                //arregloTablerosTemporal.push(ObtenerNombreTableroPorId(IdTableroTemporal));

                if (idUsuarioRepetido != IdUsuarioTemporal) {
                    //arregloTablerosTemporal = [];

                    //listadoArregloTablerosTemporal.push(arregloTablerosTemporal.join(","));

                    idUsuarioRepetido = IdUsuarioTemporal;
                    usuarioRepetido = false;
                } else {
                    usuarioRepetido = true;
                }


                PerfilesTemporal = ObtenerTodosPerfiles(IdUsuarioTemporal);
            }

            if (key == "TablerosPorUsuario")
                NombresTablerosPorUsuarioTemporal = value;

            if (key == "IdsTablerosPorUsuario")
                IdsTablerosPorUsuarioTemporal = value;

        });

        if (!usuarioRepetido) {
            elementosTabla.push("<tr>");
            elementosTabla.push("<td>" + IdTemporal + "</td>");

            console.log("IdUsuarioTemporal : " + IdUsuarioTemporal);
            console.log("ObtenerNombreCompletoUsuarioPorId(IdUsuarioTemporal) : " + ObtenerNombreCompletoUsuarioPorId(IdUsuarioTemporal));
             
            elementosTabla.push("<td>" + ObtenerNombreCompletoUsuarioPorId(IdUsuarioTemporal) + "</td>");

            elementosTabla.push("<td>" + "<a class='enlaceListadoTableros btn btn-primary ' href='#' data-listadoTableros='" + NombresTablerosPorUsuarioTemporal + "'  ><i class='fa fa-eye'></i>&nbsp;&nbsp;ver tableros</a>" + "<span style='display:none;' >" + NombresTablerosPorUsuarioTemporal + "</span>" + "</td>");//IdUsuarioTemporal
            elementosTabla.push("<td>" + PerfilesTemporal + "</td>");
            elementosTabla.push("<td>" + FechaCreacionTemporal + "</td>");

            elementosTabla.push("<td>");
            elementosTabla.push("<a data-IdUsuario='" + IdUsuarioTemporal + "' data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEliminarRegistro' href='#' role='button'><i class='fa fa-trash'></i></a>");
            elementosTabla.push("&nbsp;&nbsp;&nbsp;");
            elementosTabla.push("<a data-IdsPerfiles='" + ObtenerTodosIdsPerfiles(IdUsuarioTemporal) + "' data-listadoIdsTableros='" + IdsTablerosPorUsuarioTemporal + "' data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEditarRegistro' href='#' role='button'><i class='fa fa-pencil'></i></a>");
            elementosTabla.push("</td>");
            elementosTabla.push("</tr>");
        }

    });

    return elementosTabla.join("");
}

function AsignarComportamientoBotonAgregarRegistro() {
    $("#btnAgregar").on("click", function () {
        LimpiarCamposAdministracion();

        $('#perfilesSelectAdm').val(null).trigger('change');

        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        $("#usuariosSelectAdm").removeAttr("disabled");

        $(".OcultarPorInsercion").css("visibility", "hidden");

        MostrarBotonInsercionNuevoRegistro(true);

    });
}

function MostrarBotonInsercionNuevoRegistro(mostrar) {
    $("#btnGuardar").css("display", ObtenerArgumentoDisplayDeBoolean(!mostrar));
    $("#btnInsertar").css("display", ObtenerArgumentoDisplayDeBoolean(mostrar));
}

function AsignarComportamientoEnlaceListadoTableros() {
    $('#table_id tbody').on('click', '.enlaceListadoTableros', function () {
        swal({
            title: '<b>Tableros Asignados</b>',
            type: 'info',
            html: "<b>" + $(this).attr("data-listadoTableros") + "</b>",
            showCloseButton: true,
            focusConfirm: false,

        });
    });
}

function SeleccionarUnoPorUnoLosPerfilesParaAgregarTablerosCorrespondientes(arregloPerfiles) {
    for (var indice = 0; indice < arregloPerfiles.length; indice++)
        $('#perfilesSelectAdm').val(arregloPerfiles[indice]).trigger('change');
}

function SeleccionarTodosLosPerfilesALaVezParaMostrarlos(arregloPerfiles) {
    $('#perfilesSelectAdm').val(arregloPerfiles).trigger('change');
}

function AsignarComportamientoBotonEditarRegistro() {
    $('#table_id tbody').on('click', '.btnEditarRegistro', function () {

        var cadenaPerfiles = $(this).attr("data-IdsPerfiles");
        var arregloPerfiles = cadenaPerfiles.split(",");

        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        //MostrarOcultarPanelesDeAcordeNecesidad();
        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        MostrarBotonInsercionNuevoRegistro(false);

        $(".OcultarPorInsercion").css("visibility", "visible");

        SeleccionarUnoPorUnoLosPerfilesParaAgregarTablerosCorrespondientes(arregloPerfiles);

        SeleccionarTodosLosPerfilesALaVezParaMostrarlos(arregloPerfiles);

        var listadoX = $(this).attr("data-listadoIdsTableros");

        setTimeout(
            function () {
                $('#TablerosSelectAdm').val(null).trigger('change');
                console.log("listadoX : " + listadoX);
                var listadoTablerosTemporal = [];
                eval("listadoTablerosTemporal=[" + listadoX + "];");
                $('#TablerosSelectAdm').val(listadoTablerosTemporal).trigger('change');
            }, 250
        );

    });
}

function LimpiarCamposFiltros() {
    $("#txtId").val("");
    $("#txtIdTablero").val("");
    $("#txtIdPerfil").val("");
    $("#txtIdUsuario").val("");

    ResetearSelects();
}

function AsignarComportamientoCambioSelectTableros() {
    AsignarComportamientoCambioSelectGeneralTexto("TablerosSelect", "txtIdTablero");
    AsignarComportamientoCambioSelectGeneralId("TablerosSelectAdm", "txtIdTableroAdm");
}

function AsignarComportamientoCambioSelectPerfiles() {
    AsignarComportamientoCambioSelectGeneralTexto("perfilesSelect", "txtIdPerfil");
    AsignarComportamientoCambioSelectGeneralId("perfilesSelectAdm", "txtIdPerfilAdm");
}

function AsignarComportamientoCambioSelectUsuarios() {
    AsignarComportamientoCambioSelectGeneralTexto("usuariosSelect", "txtIdUsuario");
    AsignarComportamientoCambioSelectGeneralId("usuariosSelectAdm", "txtIdUsuarioAdm");
}

function EstablecerObjetoJSONPerfilesPorUsuario() {
    EstablecerObjetoJSONGeneral("contenedorJSONPerfilesPorUsuario", "objetoJSONPerfilesPorUsuario");
}

function EstablecerObjetoJSONTablerosPorUsuario() {
    EstablecerObjetoJSONGeneral("contenedorJSONTablerosPorUsuario", "objetoJSONTablerosPorUsuario");
}

function EstablecerObjetoJSONUsuarios() {
    EstablecerObjetoJSONGeneral("contenedorJSONUsuarios", "objetoJSONUsuarios");
}

function EstablecerObjetoJSONPerfiles() {
    EstablecerObjetoJSONGeneral("contenedorJSONPerfiles", "objetoJSONPerfiles");
}

function EstablecerObjetoJSONTableros() {
    EstablecerObjetoJSONGeneral("contenedorJSONTableros", "objetoJSONTableros");
}

function BorrarRegistroSeleccionado() {
    $("#" + CONTENEDOR_ASPNET + "hidden_idTemporalTableroPorUsuario").val(idRegistroOperacionTemporal);
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnBorrarTablerosPorUsuario").click();
        }, 500
    );
}

function ActualizarRegistroSeleccionado() {
    LlenarElementoTableroPorUsuarioTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnActualizarTablerosPorUsuario").click();
        }, 500
    );
}

function InsertarRegistroSeleccionado() {
    idRegistroOperacionTemporal = 0;
    LlenarElementoTableroPorUsuarioTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnInsertarTablerosPorUsuario").click();
        }, 500
    );
}

function InicializarTabla() {

    $('#table_id').DataTable({
        "columnDefs": [
            {
                "targets": [Columnas.Id, Columnas.FechaCreacion],
                "visible": false
            }
        ],
        "order": [[Columnas.Usuario, "asc"]]
    });
}

function ObtenerIdUsuarioParaBorrarSusRegistros(idRegistroBuscado) {
    $.each(objetoJSONTablerosPorUsuario, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    idRegistroOperacionTemporal = item.IdUsuario;

        });
    });
}

AsignarComportamientoBotonEliminarRegistro = function () {
    $('#table_id tbody').on('click', '.btnEliminarRegistro', function () {
        var idRegistroBuscado = $(this).attr("data-IdRegistro");
        ObtenerIdUsuarioParaBorrarSusRegistros(idRegistroBuscado);
        MostrarMensajeBorrado();
    });
}

function ResetearArreglosIDsSeleccionados() {
    IdsTablerosSeleccionadas = [];
    IdsPerfilesSeleccionados = [];
    IdsUsuariosSeleccionados = [];
}

function ObtenerIdsTablerosSeleccionadas() {
    console.log("ObtenerIdsTablerosSeleccionadas: " + $('#TablerosSelectAdm').val());
    IdsTablerosSeleccionadas = $('#TablerosSelectAdm').val();
}

function ObtenerIdsPerfilesSeleccionados() {
    console.log("ObtenerIdsPerfilesSeleccionados: " + $('#perfilesSelectAdm').val());
    IdsPerfilesSeleccionados = $('#perfilesSelectAdm').val();
}

function ObtenerIdsUsuariosSeleccionados() {
    console.log("ObtenerIdsUsuariosSeleccionados: " + $('#usuariosSelectAdm').val());
    IdsUsuariosSeleccionados = $('#usuariosSelectAdm').val();
}

function InsertarRegistrosTablerosPerfilesUsuario() {
    $("#" + CONTENEDOR_ASPNET + "hidden_IdsTablerosSeleccionadas").val(IdsTablerosSeleccionadas);
    $("#" + CONTENEDOR_ASPNET + "hidden_IdsPerfilesSeleccionados").val(IdsPerfilesSeleccionados);
    $("#" + CONTENEDOR_ASPNET + "hidden_IdsUsuariosSeleccionados").val(IdsUsuariosSeleccionados);

    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnInsertarRegistrosTablerosPerfilesUsuario").click();
        }, 500
    );
}

function ActualizarGuardarRegistros() {
    ValidarCamposInformacion();

    ResetearArreglosIDsSeleccionados();

    ObtenerIdsTablerosSeleccionadas();
    ObtenerIdsPerfilesSeleccionados();
    ObtenerIdsUsuariosSeleccionados();

    InsertarRegistrosTablerosPerfilesUsuario();

    MostrarAlertaExitosa();
}

AsignarComportamientoBotonInsertarRegistro = function () {
    $("#btnInsertar").on("click", function () {
        ActualizarGuardarRegistros();
    });
}

AsignarComportamientoBotonGuardarRegistro = function () {
    $("#btnGuardar").on("click", function () {
        ActualizarGuardarRegistros();
    });
}

function HabilitarSeleccionMultiplesUsuarios() {
    $('#usuariosSelect').select2({ multiple: true });
}

function LimpiarUsuariosSeleccionados() {
    $('#usuariosSelect').select2('destroy').val('').select2();
}

function ProcesosUnaSolaVezPorPantalla() {

    InicializarBusquedaAjenaTabla();
    LlenarSelectTableros();
    LlenarSelectPerfiles();
    LlenarSelectUsuarios();
    InicializarSelect();

    LimpiarUsuariosSeleccionados();
    HabilitarSeleccionMultiplesUsuarios();

    AsignarComportamientoCambioSelectTableros();
    AsignarComportamientoCambioSelectPerfiles();
    AsignarComportamientoCambioSelectUsuarios();

    LimpiarElementoTableroPorUsuarioTemporal();



    AsignarComportamientoChangePerfilesSelectAdm();
    AsignarComportamientoQuitarPerfilesSelectAdm();
}

function TodosProcesosPorPantalla() {
    EstablecerObjetoJSONTablerosPorUsuario();
    EstablecerObjetoJSONPerfilesPorUsuario();
    EstablecerObjetoJSONUsuarios();
    EstablecerObjetoJSONPerfiles();
    EstablecerObjetoJSONTableros();

    ReiniciarTablaDatos();

    LlenarDatosTabla();
    InicializarTabla();

    OcultarFiltradorTabla();
    MostrarOcultarSeccionAdministraDatos();

    AsegurarMuestroCorrectoSeccionAdministraDatos();

    AsignarComportamientoEnlaceListadoTableros();
    AsignarComportamientoBotonAgregarRegistro();
    AsignarComportamientoBotonCancelarAgregacionRegistro();
    AsignarComportamientoBotonGuardarRegistro();
    AsignarComportamientoBotonInsertarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
    AsignarComportamientoBotonEditarRegistro();
    AsignarComportamientoBotonLimpiarFiltros();
}

$(document).ready(function () {
    TodosProcesosPorPantalla();
    ProcesosUnaSolaVezPorPantalla();
});