﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;

using System.IO;
using System.Net;
using System.Xml.Linq;

namespace Track_Web.AjaxPages
{
  public partial class AjaxReportes : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
            switch (Request.QueryString["Metodo"])
            {
                case "GetRpt_KmsRecorridos":
                    GetRpt_KmsRecorridos();
                    break;
                case "GetRpt_Alertas":
                    GetRpt_Alertas();
                    break;
                case "GetDashboard":
                    GetDashboard();
                    break;
                case "GetConductores":
                    GetConductores();
                    break;
                case "GetFormatos":
                    GetFormatos();
                    break;
                case "GetLocales":
                    GetLocales();
                    break;
                case "GetRpt_Alertas_DetalleArea":
                    GetRpt_Alertas_DetalleArea();
                    break;
                case "GetRpt_MonitoreoDiario":
                    GetRpt_MonitoreoDiario();
                    break;
                case "GetIntegracionDashboard":
                    GetIntegracionDashboard();
                    break;
                case "GetAlertasDashboard":
                    GetAlertasDashboard();
                    break;
                case "GetUtilizacionFlotaDashboard":
                    GetUtilizacionFlotaDashboard();
                    break;
                case "GetTendenciaAlertasDashboard":
                    GetTendenciaAlertasDashboard();
                    break;
                case "GetTendenciaIntegracionDashboard":
                    GetTendenciaIntegracionDashboard();
                    break;
                case "GetRpt_Temperatura":
                    GetRpt_Temperatura();
                    break;
                case "GetEstadoPatente":
                    GetEstadoPatente(Request.Form["patente"].ToString(), Request.Form["economico"].ToString());
                    break;
                case "GetReporteEstadiaTractos":
                    GetReporteEstadiaTractos();
                    break;
                case "GetReporteEstadiaRemolques":
                    GetReporteEstadiaRemolques();
                    break;
                case "GetReporteEstadiaTractos_Historico":
                    GetReporteEstadiaTractos_Historico();
                    break;
                case "GetReporteEstadiaRemolques_Historico":
                    GetReporteEstadiaRemolques_Historico();
                    break;
                case "GetConductoresCEDIS":
                    GetConductoresCEDIS();
                    break;
                case "GetLastPositions":
                    GetLastPositions();
                    break;
                case "GetReporteComercial":
                    GetReporteComercial();
                    break;
            }
        }

    public void GetRpt_KmsRecorridos()
    {
      string fecDesde = "" + Request.QueryString["desde"];
      string fecHasta = "" + Request.QueryString["hasta"];
      string transportista = "" + Request.QueryString["transportista"];
      string patente = "" + Request.QueryString["patente"];

      DateTime? _fecDesde;
      DateTime? _fecHasta;

      if (fecDesde != "")
      {
        //_fecDesde = Convert.ToDateTime(fecDesde);
        _fecDesde = Utilities.convertDateIc(fecDesde);
      }
      else
      {
        _fecDesde = null;
      }

      if (fecHasta != "")
      {
        //_fecHasta = Convert.ToDateTime(fecHasta);
        _fecHasta = Utilities.convertDateIc(fecHasta);
      }
      else
      {
        _fecHasta = null;
      }

      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetRpt_KmsRecorridos(_fecDesde, _fecHasta, transportista, patente));

      Response.Write(_response);
    }

    public void GetRpt_Alertas()
    {
      string fecDesde = "" + Request.QueryString["desde"];
      string fecHasta = "" + Request.QueryString["hasta"];
      string horaDesde = "" + Request.QueryString["HoraDesde"];
      string horaHasta = "" + Request.QueryString["HoraHasta"];

      string transportista = "" + Request.QueryString["transportista"];
      string proveedorGPS = "" + Request.QueryString["proveedorGPS"];
      string patente = "" + Request.QueryString["patente"];
      string scoreConductor = "" + Request.QueryString["scoreConductor"];
      string rutConductor = "" + Request.QueryString["rutConductor"];
      string tipoAlerta = "" + Request.QueryString["tipoAlerta"];
      string descripcionAlerta = "" + Request.QueryString["descripcionAlerta"];
      string tipoViaje = "" + Request.QueryString["tipoViaje"];
      string idOrigen = "" + Request.QueryString["idOrigen"];
      string permiso = "" + Request.QueryString["permiso"];
      string estadoViaje = "" + Request.QueryString["estadoViaje"];

      if (horaDesde.Trim() != "")
        fecDesde = fecDesde.Replace("T00:00:00", "T" + horaDesde + ":00");

      if (horaHasta.Trim() != "")
        fecHasta = fecHasta.Replace("T00:00:00", "T" + horaHasta + ":00");

      int _idOrigen;
      int.TryParse(idOrigen, out _idOrigen);

      DateTime? _fecDesde;
      DateTime? _fecHasta;

      if (fecDesde != "")
      {
        //_fecDesde = Convert.ToDateTime(fecDesde);
        _fecDesde = Utilities.convertDateIc(fecDesde);
      }
      else
      {
        _fecDesde = null;
      }

      if (fecHasta != "")
      {
        //_fecHasta = Convert.ToDateTime(fecHasta);
        _fecHasta = Utilities.convertDateIc(fecHasta);
      }
      else
      {
        _fecHasta = null;
      }

      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetRpt_Alertas(_fecDesde, _fecHasta, transportista, patente, scoreConductor, rutConductor, tipoAlerta, descripcionAlerta, tipoViaje, _idOrigen, permiso, estadoViaje, proveedorGPS));

      Response.Write(_response);
    }

    public void GetDashboard()
    {
      string year = "" + Request.QueryString["year"];
      string month = "" + Request.QueryString["month"];
      string transportista = "" + Request.QueryString["transportista"];

      int _year;
      int _month;

      int.TryParse(year, out _year);
      int.TryParse(month, out _month);

      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetDashboard(_year, _month, transportista));

      Response.Write(_response);
    }

    public void GetConductores()
    {
      string _todos = "" + Request.QueryString["Todos"];
      string _result = "";
      Methods_Reportes _obj = new Methods_Reportes();

      if (_todos == "True")
      {
        var _list = (from i in _obj.GetConductores(true)
                     select new
                     {
                       RutConductor = (i.Rut == "Todos") ? "Todos" : i.Rut + '-' + i.DV,
                       NombreConductor = i.Nombre + ' ' + i.Paterno
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }
      else
      {
        var _list = (from i in _obj.GetConductores()
                     select new
                     {
                       RutConductor = (i.Rut == "Todos") ? "Todos" : i.Rut + '-' + i.DV,
                       NombreConductor = i.Nombre + ' ' + i.Paterno
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }

      Response.Write(_result);
    }

    public void GetFormatos()
    {
      string _todos = "" + Request.QueryString["Todos"];
      string _result = "";
      Methods_Reportes _obj = new Methods_Reportes();

      if (_todos == "True")
      {
        var _list = (from i in _obj.GetFormatos(true)
                     select new
                     {
                       Id = i.Id,
                       Nombre = i.Nombre
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }
      else
      {
        var _list = (from i in _obj.GetFormatos()
                     select new
                     {
                       Id = i.Id,
                       Nombre = i.Nombre
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }

      Response.Write(_result);
    }

    public void GetLocales()
    {
      string idFormato = "" + Request.QueryString["IdFormato"];

      if (idFormato == null || idFormato == "")
      {
        idFormato = "0";
      }

      string _result = "";

      int _idFormato;
      int.TryParse(idFormato, out _idFormato);

      Methods_Reportes _obj = new Methods_Reportes();

      if (_idFormato == 0)
      {
        var _list = (from i in _obj.GetLocales()
                     orderby (i.CodigoInterno)
                     select new
                     {
                       CodigoInterno = i.CodigoInterno,
                       IdFormato = i.Id,
                       NumeroLocal = i.NumeroLocal
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }
      else
      {
        var _list = (from i in _obj.GetLocales()
                     where i.CodigoInterno == 0 || i.IdFormato == _idFormato
                     orderby (i.CodigoInterno)
                     select new
                     {
                       CodigoInterno = i.CodigoInterno,
                       IdFormato = i.Id,
                       NumeroLocal = i.NumeroLocal
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }

      Response.Write(_result);
    }

    public void GetRpt_Alertas_DetalleArea()
    {
      string fecDesde = "" + Request.QueryString["desde"];
      string fecHasta = "" + Request.QueryString["hasta"];
      string transportista = "" + Request.QueryString["transportista"];
      string proveedorGPS = "" + Request.QueryString["proveedorGPS"];
      string patente = "" + Request.QueryString["patente"];
      string scoreConductor = "" + Request.QueryString["scoreConductor"];
      string rutConductor = "" + Request.QueryString["rutConductor"];
      string tipoAlerta = "" + Request.QueryString["tipoAlerta"];
      string idFormato = "" + Request.QueryString["idFormato"];
      string codigoLocal = "" + Request.QueryString["codigoLocal"];
      string permiso = "" + Request.QueryString["permiso"];
      string estadoViaje = "" + Request.QueryString["estadoViaje"];
      string puntosPolygon = "" + Request.QueryString["vertices"];

      int _idFormato;
      int _codigoLocal;
      int.TryParse(idFormato, out _idFormato);
      int.TryParse(codigoLocal, out _codigoLocal);

      DateTime? _fecDesde;
      DateTime? _fecHasta;

      if (fecDesde != "")
      {
        //_fecDesde = Convert.ToDateTime(fecDesde);
        _fecDesde = Utilities.convertDateIc(fecDesde);
      }
      else
      {
        _fecDesde = null;
      }

      if (fecHasta != "")
      {
        //_fecHasta = Convert.ToDateTime(fecHasta);
        _fecHasta = Utilities.convertDateIc(fecHasta);
      }
      else
      {
        _fecHasta = null;
      }

      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetRpt_AlertasDetalleArea(_fecDesde, _fecHasta, transportista, patente, scoreConductor, rutConductor, tipoAlerta, _idFormato, _codigoLocal, permiso, estadoViaje, proveedorGPS, puntosPolygon));

      Response.Write(_response);
    }

    public void GetRpt_MonitoreoDiario()
    {
      string fecDesde = "" + Request.QueryString["desde"];
      string fecHasta = "" + Request.QueryString["hasta"];

      DateTime? _fecDesde;
      DateTime? _fecHasta;

      if (fecDesde != "")
      {
        //_fecDesde = Convert.ToDateTime(fecDesde);
        _fecDesde = Utilities.convertDateIc(fecDesde);
      }
      else
      {
        _fecDesde = null;
      }

      if (fecHasta != "")
      {
        //_fecHasta = Convert.ToDateTime(fecHasta);
        _fecHasta = Utilities.convertDateIc(fecHasta);
      }
      else
      {
        _fecHasta = null;
      }

      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetRpt_MonitoreoDiario(_fecDesde, _fecHasta));

      Response.Write(_response);
    }

    public void GetIntegracionDashboard()
    {
      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetDashboardIntegracion());

      Response.Write(_response);
    }
    public void GetAlertasDashboard()
    {
      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetDashboardAlertas());

      Response.Write(_response);
    }

    public void GetUtilizacionFlotaDashboard()
    {
      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetDashboardUtilizacionFlota());

      Response.Write(_response);
    }

    public void GetTendenciaIntegracionDashboard()
    {
      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetDashboardTendenciaIntegracion());

      Response.Write(_response);
    }

    public void GetTendenciaAlertasDashboard()
    {
      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetDashboardTendenciaAlertas());

      Response.Write(_response);
    }

    public void GetRpt_Temperatura()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];
      string fecDesde = "" + Request.QueryString["desde"];
      string fecHasta = "" + Request.QueryString["hasta"];
      string transportista = "" + Request.QueryString["transportista"];
      string patente = "" + Request.QueryString["patente"];

      DateTime? _fecDesde;
      DateTime? _fecHasta;
      int _nroTransporte;

      int.TryParse(nroTransporte, out _nroTransporte);

      if (fecDesde != "")
      {
        //_fecDesde = Convert.ToDateTime(fecDesde);
        _fecDesde = Utilities.convertDateIc(fecDesde);
      }
      else
      {
        _fecDesde = null;
      }

      if (fecHasta != "")
      {
        //_fecHasta = Convert.ToDateTime(fecHasta);
        _fecHasta = Utilities.convertDateIc(fecHasta);
      }
      else
      {
        _fecHasta = null;
      }

      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetRpt_Temperatura(_fecDesde, _fecHasta, transportista, patente, _nroTransporte));

      Response.Write(_response);
    }

    public void GetEstadoPatente(string patente, string economico)
    {
      Methods_Reportes _objMethodsReportes = new Methods_Reportes();

      string _response = JsonConvert.SerializeObject(_objMethodsReportes.GetEstadoPatente(patente, economico));

      Response.Write(_response);
    }

    public string getDireccion(string lat, string lon)
    {
      try
      {
                //var requestUri = "https://maps.google.com/maps/api/geocode/xml?sensor=false&latlng=" + lat + "+" + lon + "&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY";
                var requestUri = "https://maps.google.com/maps/api/geocode/xml?sensor=false&latlng=" + lat + "+" + lon + "&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8";
                var request = WebRequest.Create(requestUri);
        var response = request.GetResponse();
        var xdoc = XDocument.Load(response.GetResponseStream());

        var result = xdoc.Element("GeocodeResponse").Element("result");
        var locationElement = result.Element("formatted_address").Value;
        return locationElement.ToString();
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();
        return "";
      }

    }

    public void GetConductoresCEDIS()
    {
      string _todos = "" + Request.QueryString["Todos"];
      string cedis = "" + Request.QueryString["CEDIS"];

      int _cedis;

      int.TryParse(cedis, out _cedis);

      string _result = "";
      Methods_Reportes _obj = new Methods_Reportes();

      if (_todos == "True")
      {
        var _list = (from i in _obj.GetConductoresCEDIS(true, _cedis)
                     select new
                     {
                       RutConductor = i.RutConductor
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }
      else
      {
        var _list = (from i in _obj.GetConductoresCEDIS(false, _cedis)
                     select new
                     {
                       RutConductor = i.RutConductor
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);
      }

      Response.Write(_result);
    }

    public void GetLastPositions()
    {
      string pmovil = Request.QueryString["patente"].ToString();
    string economico = Request.QueryString["economico"].ToString();
    string ptransportista = Request.QueryString["transportista"].ToString();
      int ptipomovil = int.Parse(Request.QueryString["tipomovil"].ToString());
      string userName = Utilities.GetUsuarioSession(Session);

        if (pmovil.ToLower() == "todas")
        {
            pmovil = "";
        }

        if (ptransportista.ToLower() == "todos")
      {
        ptransportista = "";
      }

      Methods_Reportes _object = new Methods_Reportes();
      string _response = JsonConvert.SerializeObject(_object.getUltimoasPosiciones(pmovil, economico, ptransportista, ptipomovil, userName));
      Session["ReporteEstadoFlota"] = _response;
      Response.Write(_response);
    }

    public void GetReporteEstadiaTractos()
    {
        string pTransportista = Request.QueryString["transportista"].ToString();
        string pPatente = Request.QueryString["patente"].ToString();
        string userName = Utilities.GetUsuarioSession(Session);

        Methods_Viajes _object = new Methods_Viajes();
        string _response = JsonConvert.SerializeObject(_object.getReporteEstadiaTractos(pTransportista, pPatente, userName));
        Session["ReporteEstadiaTractos"] = _response;
        Response.Write(_response);
    }

    public void GetReporteEstadiaRemolques()
    {
        string pPatente = Request.QueryString["patente"].ToString();
        string pListCarrier = Request.QueryString["listCarrier"].ToString();
        string userName = Utilities.GetUsuarioSession(Session);

        Methods_Viajes _object = new Methods_Viajes();
        string _response = JsonConvert.SerializeObject(_object.getReporteEstadiaRemolques(pPatente, pListCarrier, userName));
        Session["ReporteEstadiaRemolques"] = _response;
        Response.Write(_response);
    }

    public void GetReporteEstadiaTractos_Historico()
    {
        string desde = "" + Request.QueryString["desde"];
        string hasta = "" + Request.QueryString["hasta"];
        string pTransportista = Request.QueryString["transportista"].ToString();
        string pPatente = Request.QueryString["patente"].ToString();
        string userName = Utilities.GetUsuarioSession(Session);

        DateTime _fecDesde;
        DateTime _fecHasta;

        _fecDesde = Utilities.convertDateIc(desde);
        _fecHasta = Utilities.convertDateIc(hasta);

        Methods_Viajes _object = new Methods_Viajes();
        string _response = JsonConvert.SerializeObject(_object.getReporteEstadiaTractos_Historico(_fecDesde, _fecHasta, pTransportista, pPatente, userName));
        Session["ReporteEstadiaTractos_Historico"] = _response;
        Response.Write(_response);
    }

    public void GetReporteEstadiaRemolques_Historico()
    {
        string desde = "" + Request.QueryString["desde"];
        string hasta = "" + Request.QueryString["hasta"];
        string pPatente = Request.QueryString["patente"].ToString();
        string pListCarrier = Request.QueryString["listCarrier"].ToString();
        string userName = Utilities.GetUsuarioSession(Session);

        DateTime _fecDesde;
        DateTime _fecHasta;

        _fecDesde = Utilities.convertDateIc(desde);
        _fecHasta = Utilities.convertDateIc(hasta);

        Methods_Viajes _object = new Methods_Viajes();
        string _response = JsonConvert.SerializeObject(_object.getReporteEstadiaRemolques_Historico(_fecDesde, _fecHasta, pPatente, pListCarrier, userName));
        Session["ReporteEstadiaRemolques_Historico"] = _response;
        Response.Write(_response);
    }

        public void GetReporteComercial()
    {
        string fecDesde = "" + Request.QueryString["desde"];
        string fecHasta = "" + Request.QueryString["hasta"];
        string pIdCedis = Request.QueryString["idCedis"].ToString();

        DateTime? _fecDesde;
        DateTime? _fecHasta;

        if (fecDesde != "")
        {
            _fecDesde = Utilities.convertDateIc(fecDesde);
        }
        else
        {
            _fecDesde = null;
        }

        if (fecHasta != "")
        {
            _fecHasta = Utilities.convertDateIc(fecHasta);
        }
        else
        {
            _fecHasta = null;
        }

        Methods_Viajes _object = new Methods_Viajes();
        string _response = JsonConvert.SerializeObject(_object.getReporteComercial(_fecDesde, _fecHasta, pIdCedis));
        Session["ReporteComercial"] = _response;
        Response.Write(_response);
    }

    }
}