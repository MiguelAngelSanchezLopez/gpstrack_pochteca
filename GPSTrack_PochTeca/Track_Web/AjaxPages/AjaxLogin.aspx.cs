﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;
using System.Reflection;

using System.IO;
using System.Configuration;

namespace Track_Web.AjaxPages
{
  public partial class AjaxLogin : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
            switch (Request.QueryString["Metodo"])
            {
                case "ValidarUsuario":
                    ValidarUsuario(Request.Form["Usuario"].ToString(), Request.Form["Password"].ToString());
                    break;
                case "GetVersion":
                    GetVersion();
                    break;
                case "GetPerfilUsuarioConectado":
                    GetPerfilUsuarioConectado();
                    break;
                case "GetNameUsuarioConectado":
                    GetNameUsuarioConectado();
                    break;
                case "GetCedisAsociado":
                    GetCedisAsociado();
                    break;
                case "GetDeterminantesAsociados":
                    GetDeterminantesAsociados();
                    break;
                case "GetDiferenciaHoraria":
                    GetDiferenciaHoraria();
                    break;
                case "GetUsuarios":
                    GetUsuarios();
                    break;
                case "GuardarUsuario":
                    GuardarUsuario(Request.Form["idUsuario"].ToString(), Request.Form["userName"].ToString(), Request.Form["password"].ToString(), Request.Form["nombre"].ToString(), Request.Form["apellidos"].ToString(), Request.Form["eMail"].ToString(), Request.Form["activo"].ToString(), Request.Form["listCEDISAsociados"].ToString());
                    break;
                case "getInfoUsuario":
                    getInfoUsuario();
                    break;
            }
        }

      public void ValidarUsuario(string usuario, string password)
      {
        Methods_User _obj = new Methods_User();
        try
        {
          string respuesta = _obj.ValidarUsuario(usuario, password).ToString();

          Session.Timeout = 1440;

          if (respuesta == "Ok")
          {
            string transportista = _obj.getTransportista(usuario).ToString();
            _obj.UpdateTrackId(usuario);

            Track_Usuarios userlogado = _obj.getUsuarioConectado(usuario);

            Session["userName"] = usuario;
            Session["idusuario"] = userlogado.IdUsuario.ToString();   //_obj.getidUsuario(usuario).ToString();
            Session["transportista"] = transportista;
            Session["IdAltoTrack"] = userlogado.IdAltoTrack.ToString(); //_obj.getIdAltotrack(usuario).ToString();
            Session["correo"] = userlogado.EMail.ToString();
            Session["Nombre"] = userlogado.Nombre.ToString() + " " + userlogado.Apellidos.ToString();
            int idRol = int.Parse(userlogado.IdRol.ToString()); //_obj.getidRol(usuario);
            Session["idperfil"] = idRol.ToString();
            Session["idzonageo"] = "";// _obj.getIdZonaGeo(usuario).ToString();

            string perfilUsuarioConectado = _obj.getPerfilUsuarioConectado(usuario).ToString();
            string nameUsuarioConectado = _obj.getNameUsuarioConectado(usuario).ToString();

            Session["perfilUsuarioConectado"] = perfilUsuarioConectado;
            Session["nameUsuarioConectado"] = nameUsuarioConectado;

            _obj.guardarlog(userlogado.IdUsuario, "GPSTrack. Login");
        }
          
          Response.Write(respuesta);

        }
        catch (Exception)
        {
                
          Response.Write("Se ha producido un error.");
                
        }
      }

    public void GetVersion()
    {
      string assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + '.' + Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString() + '.' + Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

      Response.Write(assemblyVersion);
    }

    public void GetPerfilUsuarioConectado()
    {
        string perfilUsuarioConectado = Session["perfilUsuarioConectado"].ToString();

        Response.Write(perfilUsuarioConectado);
    }

        public void GetNameUsuarioConectado()
    {
        string nameUsuarioConectado = Session["nameUsuarioConectado"].ToString();

        Response.Write(nameUsuarioConectado);
    }

    public void GetCedisAsociado()
    {
        Methods_User _obj = new Methods_User();

        string cedisAsociado = _obj.getCEDISAsociado(Session["userName"].ToString());

        Response.Write(cedisAsociado);
    }
        
    public void GetDeterminantesAsociados()
    {
        string idUsuario = "" + Request.QueryString["idUsuario"];

        int _idUsuario;

        int.TryParse(idUsuario, out _idUsuario);

        Methods_User _obj = new Methods_User();

        string _response = JsonConvert.SerializeObject(_obj.getDeterminantesAsociados(_idUsuario));

        Response.Write(_response);
        }

        public void GetDiferenciaHoraria()
    {
        Methods_User _obj = new Methods_User();

        string diferenciaHoraria = _obj.webDiferenciaHoraria().ToString(); ;

        Response.Write(diferenciaHoraria);
    }

    public void GetUsuarios()
    {
        string userName = Utilities.GetUsuarioSession(Session);

        Methods_User _objMethodsUser = new Methods_User();

        string _response = JsonConvert.SerializeObject(_objMethodsUser.GetUsuarios(userName));

        Response.Write(_response);
    }

        public void GuardarUsuario(string idUsuario, string userName, string password, string nombre, string apellidos, string eMail, string activo, string listCEDISAsociados)
        {
            string idusuarioConectado = Utilities.GetIdUsuarioSession(Session);
            int _idUsuarioConectado = 0;

            int.TryParse(idusuarioConectado, out _idUsuarioConectado);

            int _idUsuario;
            int _activo;

            int.TryParse(idUsuario, out _idUsuario);

            if (activo == "true")
            {
                _activo = 1;
            }
            else
            {
                _activo = 0;
            }


            Methods_User _obj = new Methods_User();
            try
            {
                string _resp = _obj.GuardarUsuario(_idUsuario,userName, password, nombre, apellidos, eMail, _activo, listCEDISAsociados, _idUsuarioConectado);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void getInfoUsuario()
        {
            try
            {


                string _UrlTicket = ConfigurationManager.AppSettings["UrlTicket"];
                string _LlavePerfil = ConfigurationManager.AppSettings["LlavePerfil"];
                string _LlaveFormato = ConfigurationManager.AppSettings["LlaveFormato"];
                string _EnviadoDesde = ConfigurationManager.AppSettings["ENVIADO_DESDE"];
                string _OrigenTicket = ConfigurationManager.AppSettings["ORIGEN_TICKET"];
                string _ComodinOtro = ConfigurationManager.AppSettings["COMODIN_OTRO"];

                var json = new
                {
                    UserName = Session["userName"].ToString(),
                    Nombre = Session["Nombre"].ToString(),
                    IdUsuario = Session["idusuario"].ToString(),
                    Tranportista = Session["transportista"].ToString(),
                    IdPerfil = Session["idperfil"].ToString(),
                    IdZonaGeografica = Session["idzonageo"].ToString(),
                    IdAltoTrack = Session["IdAltoTrack"].ToString(),
                    Correo = Session["correo"].ToString(),
                    Fono = "",
                    UrlTicket = _UrlTicket,
                    LlavePerfil = _LlavePerfil,
                    LlaveFormato = _LlaveFormato,
                    EnviadoDesde = _EnviadoDesde,
                    OrigenTicket = _OrigenTicket,
                    ComodinOtro = _ComodinOtro
                };

                string _response = JsonConvert.SerializeObject(json);

                Response.Write(_response);
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
            }
        }

    }
}