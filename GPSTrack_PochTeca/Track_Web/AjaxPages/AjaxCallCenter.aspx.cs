﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;

using System.IO;

namespace Track_Web.AjaxPages
{
  public partial class AjaxCallCenter : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      switch (Request.QueryString["Metodo"])
      {
        case "GetExplicacionGestion":
          GetExplicacionGestion();
          break;
        case "GetContactosGestion":
          GetContactosGestion();
          break;
        case "GetGestionCallCenter":
          GetGestionCallCenter();
          break;
        case "GuardarGestionAlerta":
          GuardarGestionAlerta(Request.Form["IdAlerta"].ToString(), Request.Form["idUsuario1"].ToString(), Request.Form["Explicacion1"].ToString(), Request.Form["Observacion1"].ToString(), Request.Form["idUsuario2"].ToString(), Request.Form["Explicacion2"].ToString(),
              Request.Form["Observacion2"].ToString(), Request.Form["idUsuario3"].ToString(), Request.Form["Explicacion3"].ToString(), Request.Form["Observacion3"].ToString(), Request.Form["idUsuario4"].ToString(), Request.Form["Explicacion4"].ToString(), Request.Form["Observacion4"].ToString(),
              Request.Form["idUsuario5"].ToString(), Request.Form["Explicacion5"].ToString(), Request.Form["Observacion5"].ToString());
          break;
      }
    }

    public void GetExplicacionGestion()
    {
      Methods_CallCenter _obj = new Methods_CallCenter();

      string _result;

      var _list = (from i in _obj.GetExpliacionGestion()
                    select new
                    {
                      i.Id,
                      i.Nombre,
                      i.Valor,
                      i.Extra1
                    }).ToList();
      _result = JsonConvert.SerializeObject(_list);

      Response.Write(_result);
    }

    public void GetContactosGestion()
    {
      string idAlerta = "" + Request.QueryString["idAlerta"];

      int _idAlerta;

      int.TryParse(idAlerta, out _idAlerta);

      Methods_CallCenter _objMethodsCallCenter = new Methods_CallCenter();

      string _response = JsonConvert.SerializeObject(_objMethodsCallCenter.GetContactosGestion(_idAlerta));

      Response.Write(_response);
    }

    public void GetGestionCallCenter()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];
      string idEmbarque = "" + Request.QueryString["idEmbarque"];
      string codLocal = "" + Request.QueryString["codLocal"];

      long _nroTransporte;
      long _idEmbarque;
      int _codLocal;

      long.TryParse(nroTransporte, out _nroTransporte);
      long.TryParse(idEmbarque, out _idEmbarque);
      int.TryParse(codLocal, out _codLocal);

      Methods_CallCenter _objMethodsCallCenter = new Methods_CallCenter();

      string _response = JsonConvert.SerializeObject(_objMethodsCallCenter.GetGestionCallCenter(_nroTransporte, _idEmbarque, _codLocal));

      Response.Write(_response);
    }

    public void GuardarGestionAlerta(string IdAlerta, string idUsuario1, string Explicacion1, string Observacion1, string idUsuario2, string Explicacion2, string Observacion2, string idUsuario3, string Explicacion3, string Observacion3, 
                                     string idUsuario4, string Explicacion4, string Observacion4, string idUsuario5, string Explicacion5, string Observacion5)
    {
      int _IdAlerta;
      int _idUsuario1;
      int _idUsuario2;
      int _idUsuario3;
      int _idUsuario4;
      int _idUsuario5;

      int.TryParse(IdAlerta, out _IdAlerta);
      int.TryParse(idUsuario1, out _idUsuario1);
      int.TryParse(idUsuario2, out _idUsuario2);
      int.TryParse(idUsuario3, out _idUsuario3);
      int.TryParse(idUsuario4, out _idUsuario4);
      int.TryParse(idUsuario5, out _idUsuario5);

      Methods_CallCenter _objMethodsCallCenter = new Methods_CallCenter();
      try
      {
        string respuesta = _objMethodsCallCenter.GuardarGestionAlerta(_IdAlerta, _idUsuario1, Explicacion1, Observacion1, _idUsuario2, Explicacion2, Observacion2, _idUsuario3, Explicacion3, Observacion3, _idUsuario4, Explicacion4, Observacion4, _idUsuario5, Explicacion5, Observacion5).ToString();

        Response.Write(respuesta);

      }
      catch (Exception)
      {
        Response.Write("Se ha producido un error.");

      }
    }

  }
}