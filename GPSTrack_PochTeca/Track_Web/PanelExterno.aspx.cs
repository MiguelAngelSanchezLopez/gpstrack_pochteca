﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class PanelExterno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);
            CargarTableroCorrespondiente(Convert.ToInt32(Request.QueryString["idTablero"]));
        }

        public void CargarTableroCorrespondiente(int idTablero)
        {
            StringBuilder cadenaListadoPaginas = new StringBuilder();

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"ObtenerListadoPaginasPermitidasAlUsuario\">\n");

            scriptJSGenerado.Append("var tableroInnerFrame = ");

            scriptJSGenerado.Append("\"<iframe src='" + CargarRutaTableroPorId(idTablero) + "' width='100%' height='100%' align='middle' style=' display: block;margin-left: auto;margin-right: auto; ' id='ext-gen1102'></iframe>\"");

            scriptJSGenerado.Append(";");

            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptPanelExterno", scriptJSGenerado.ToString());
        }

        public string CargarRutaTableroPorId(int idTablero)
        {
            return new ControladorTableros().ObtenerURLTableroPorId(idTablero);
        }
    }
}