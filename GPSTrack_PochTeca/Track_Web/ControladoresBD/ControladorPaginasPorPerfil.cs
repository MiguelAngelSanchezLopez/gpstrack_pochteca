﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorPaginasPorPerfil: ControladorGeneral
    {
        private List<Track_PaginasPorPerfil> listadoPaginasPorPerfil;
        
        public Track_PaginasPorPerfil LlenarPaginaPorPerfil() {
            Track_PaginasPorPerfil paginaPorPerfilTemporal = new Track_PaginasPorPerfil();
            paginaPorPerfilTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            paginaPorPerfilTemporal.IdPagina = Convert.ToInt32(lectorDatosSQL["IdPagina"]);
            paginaPorPerfilTemporal.IdPerfil = Convert.ToInt32(lectorDatosSQL["IdPerfil"]);
            paginaPorPerfilTemporal.FechaCreacion = Convert.ToDateTime(lectorDatosSQL["FechaCreacion"]);
            paginaPorPerfilTemporal.IdUsuarioCreo = Convert.ToInt32(lectorDatosSQL["IdUsuarioCreo"]);
            paginaPorPerfilTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"]);
            return paginaPorPerfilTemporal;
        }

        public void LlenarListadoPaginasPorPerfil() {
            while (lectorDatosSQL.Read())
                listadoPaginasPorPerfil.Add(LlenarPaginaPorPerfil());
        }
        
        public string ObtenerTodosRegistrosPaginasPorPerfil()
        {
            try
            {
                InicializarConexion();

                listadoPaginasPorPerfil = new List<Track_PaginasPorPerfil>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPaginasPorPerfil");

                LlenarLectorDatosSQL();

                LlenarListadoPaginasPorPerfil();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoPaginasPorPerfil);
            }
            catch (Exception ex) {
                return ArmarJSONGenericoConError(ex.Message);
            }
        }

        public void EliminarRegistroPaginasPorPerfilPorId(int id) {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarPaginasPorPerfil");
                EstablecerParametroProcedimientoAlmacenado(id,"int", "@Id");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex) {
                throw;
            }
        }

        public void ActualizarRegistroPaginasPorPerfilPorId(string cadenaJSONPaginaPorPerfil)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ActualizarPaginasPorPerfil");
                Track_PaginasPorPerfil paginaPorPerfilTemporal = JsonConvert.DeserializeObject<Track_PaginasPorPerfil>(cadenaJSONPaginaPorPerfil);
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroPaginasPorPerfilPorId(string cadenaJSONPaginaPorPerfil)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarPaginasPorPerfil");
                Track_PaginasPorPerfil paginaPorPerfilTemporal = JsonConvert.DeserializeObject<Track_PaginasPorPerfil>(cadenaJSONPaginaPorPerfil);
                
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPagina, "int", "@IdPagina");
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdPerfil, "int", "@IdPerfil");
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.IdUsuarioCreo, "int", "@IdUsuarioCreo");
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.FechaCreacion, "datetime", "@FechaCreacion");
                EstablecerParametroProcedimientoAlmacenado(paginaPorPerfilTemporal.Activo, "bool", "@Activo");
                
                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}