﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Track_Web.ModelosBD;

namespace Track_Web.ControladoresBD
{
    public class ControladorTablerosPorUsuario : ControladorGeneral
    {
        private List<Track_TablerosPorUsuario> listadoTablerosPorUsuario;

        public Track_TablerosPorUsuario LlenarTableroPorUsuario()
        {
            Track_TablerosPorUsuario TableroPorUsuarioTemporal = new Track_TablerosPorUsuario();
            TableroPorUsuarioTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            TableroPorUsuarioTemporal.IdUsuario = Convert.ToInt32(lectorDatosSQL["IdUsuario"]);
            TableroPorUsuarioTemporal.IdTablero = Convert.ToInt32(lectorDatosSQL["IdTablero"]);
            TableroPorUsuarioTemporal.FechaCreacion = Convert.ToDateTime(lectorDatosSQL["FechaCreacion"]);
            TableroPorUsuarioTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"]);

            //TableroPorUsuarioTemporal.TablerosPorUsuario = ObtenerNombreTablerosPorUsuario(TableroPorUsuarioTemporal.IdUsuario);
            string nombresTableros = lectorDatosSQL["NombresTableros"].ToString();
            TableroPorUsuarioTemporal.TablerosPorUsuario = nombresTableros;

            //TableroPorUsuarioTemporal.IdsTablerosPorUsuario = ObtenerIdsTablerosPorUsuario(TableroPorUsuarioTemporal.IdUsuario);
            string IdsTablerosPorUsuario = lectorDatosSQL["IdsTableros"].ToString();
            IdsTablerosPorUsuario = IdsTablerosPorUsuario.Substring(0, IdsTablerosPorUsuario.Length - 1);
            TableroPorUsuarioTemporal.IdsTablerosPorUsuario = new List<int>(IdsTablerosPorUsuario.Split(',').Select(s => int.Parse(s)));

            return TableroPorUsuarioTemporal;
        }

        public List<int> ObtenerIdsTablerosPorUsuario(int idUsuario)
        {
            List<int> IdsTablerosPorUsuario = new List<int>();
            try
            {
                SqlConnection conexionSQLInterna;
                conexionSQLInterna = new SqlConnection(CADENA_CONEXION);

                conexionSQLInterna.Open();

                SqlCommand comandoSQLInterno = new SqlCommand("Track_GetIdTablerosPorUsuario", conexionSQLInterna);

                comandoSQLInterno.CommandType = CommandType.StoredProcedure;
                comandoSQLInterno.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = idUsuario;

                IdsTablerosPorUsuario = ObtenerListadoIdsTablerosPorUsuario(comandoSQLInterno.ExecuteReader());

                conexionSQLInterna.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            return IdsTablerosPorUsuario;
        }

        public List<string> ObtenerNombresTablerosPorUsuario(int idUsuario)
        {
            List<string> nombresTablerosPorUsuario = new List<string>();
            try
            {
                SqlConnection conexionSQLInterna;
                conexionSQLInterna = new SqlConnection(CADENA_CONEXION);

                conexionSQLInterna.Open();

                SqlCommand comandoSQLInterno = new SqlCommand("Track_GetNombreTablerosPorUsuario", conexionSQLInterna);

                comandoSQLInterno.CommandType = CommandType.StoredProcedure;
                comandoSQLInterno.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = idUsuario;

                nombresTablerosPorUsuario = ObtenerListadoNombresCortosTablerosPorUsuario(comandoSQLInterno.ExecuteReader());

                conexionSQLInterna.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            return nombresTablerosPorUsuario;
        }

        public List<string> ObtenerListadoNombresCortosTablerosPorUsuario(SqlDataReader lectorDatos)
        {
            List<string> NombresTablerosPorUsuario = new List<string>();

            while (lectorDatos.Read())
                NombresTablerosPorUsuario.Add(lectorDatos["NombreCorto"].ToString());

            return NombresTablerosPorUsuario;
        }

        public List<int> ObtenerListadoIdsTablerosPorUsuario(SqlDataReader lectorDatos)
        {
            List<int> IdsTablerosPorUsuario = new List<int>();

            while (lectorDatos.Read())
                IdsTablerosPorUsuario.Add(Convert.ToInt32(lectorDatos["Id"]));

            return IdsTablerosPorUsuario;
        }

        private string ObtenerNombreTablerosPorUsuario(int idUsuario)
        {
            string nombresTableros = "";
            try
            {
                SqlConnection conexionSQLInterna;
                conexionSQLInterna = new SqlConnection(CADENA_CONEXION);

                conexionSQLInterna.Open();

                SqlCommand comandoSQLInterno = new SqlCommand("Track_GetNombreTablerosPorUsuario", conexionSQLInterna);

                comandoSQLInterno.CommandType = CommandType.StoredProcedure;
                comandoSQLInterno.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = idUsuario;

                nombresTableros = ObtenerListadoNombresTablerosPorUsuario(comandoSQLInterno.ExecuteReader());

                conexionSQLInterna.Close();
            }
            catch (Exception ex)
            {
                nombresTableros = "";
            }
            return nombresTableros;
        }

        public string ObtenerListadoNombresTablerosPorUsuario(SqlDataReader lectorDatos)
        {

            StringBuilder listadoNombresTablerosPorUsuario = new StringBuilder();

            while (lectorDatos.Read())
                listadoNombresTablerosPorUsuario.Append(lectorDatos["Nombre"] + "<br>");

            string nombresTableros = listadoNombresTablerosPorUsuario.ToString();

            return nombresTableros.Substring(0, nombresTableros.Length - 1);
        }

        public void DesactivarTablerosPorUsuarioCoincidente(int IdUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_DesactivarTablerosPorUsuario");

                EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AgregarNuevasTablerosPorUsuario(int IdUsuario, string IdsTablerosSeleccionadas)
        {
            List<int> arregloIdsTableros = new List<int>();

            arregloIdsTableros = IdsTablerosSeleccionadas.Split(',').Select(Int32.Parse).ToList();

            try
            {
                for (int indice = 0; indice < arregloIdsTableros.Count; indice++)
                {
                    InicializarConexion();
                    conexionSQL.Open();

                    EstablecerProcedimientoAlmacenado("Track_InsertarTablerosPorUsuario");

                    EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                    EstablecerParametroProcedimientoAlmacenado(arregloIdsTableros[indice], "int", "@IdTablero");

                    EjecutarProcedimientoAlmacenado();
                    conexionSQL.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DesactivarPerfilesPorUsuarioCoincidente(int IdUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_DesactivarPerfilesPorUsuario");

                EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AgregarNuevosPerfilesPorUsuario(int IdUsuario, string IdsPerfilesSeleccionados)
        {
            List<int> arregloIdsPerfiles = new List<int>();

            arregloIdsPerfiles = IdsPerfilesSeleccionados.Split(',').Select(Int32.Parse).ToList();

            try
            {
                for (int indice = 0; indice < arregloIdsPerfiles.Count; indice++)
                {
                    InicializarConexion();
                    conexionSQL.Open();

                    EstablecerProcedimientoAlmacenado("Track_InsertarPerfilPorUsuario");

                    EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                    EstablecerParametroProcedimientoAlmacenado(arregloIdsPerfiles[indice], "int", "@IdPerfil");

                    EjecutarProcedimientoAlmacenado();
                    conexionSQL.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroTablerosPerfilesUsuario(string IdsTablerosSeleccionadas, string IdsPerfilesSeleccionados, string IdUsuarioSeleccionado)
        {
            DesactivarTablerosPorUsuarioCoincidente(Convert.ToInt32(IdUsuarioSeleccionado));
            AgregarNuevasTablerosPorUsuario(Convert.ToInt32(IdUsuarioSeleccionado), IdsTablerosSeleccionadas);

            DesactivarPerfilesPorUsuarioCoincidente(Convert.ToInt32(IdUsuarioSeleccionado));
            AgregarNuevosPerfilesPorUsuario(Convert.ToInt32(IdUsuarioSeleccionado), IdsPerfilesSeleccionados);
        }

        public void LlenarListadoTablerosPorUsuario()
        {
            while (lectorDatosSQL.Read())
                listadoTablerosPorUsuario.Add(LlenarTableroPorUsuario());
        }

        public string ObtenerTodosRegistrosTablerosPorUsuario()
        {
            try
            {
                InicializarConexion();

                listadoTablerosPorUsuario = new List<Track_TablerosPorUsuario>();

                conexionSQL.Open();

                //EstablecerProcedimientoAlmacenado("Track_GetTablerosPorUsuario_110718");
                EstablecerProcedimientoAlmacenado("Track_GetTablerosPorUsuario");

                LlenarLectorDatosSQL();

                LlenarListadoTablerosPorUsuario();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoTablerosPorUsuario);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }

        public void EliminarRegistroTablerosPorUsuarioPorId(int IdUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarTablerosPorUsuario_PorIdUsuario");
                EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroTablerosPorUsuarioPorId(string cadenaJSONTableroPorUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarTablerosPorUsuario");
                Track_TablerosPorUsuario TableroPorUsuarioTemporal = JsonConvert.DeserializeObject<Track_TablerosPorUsuario>(cadenaJSONTableroPorUsuario);

                EstablecerParametroProcedimientoAlmacenado(TableroPorUsuarioTemporal.IdTablero, "int", "@IdTablero");
                EstablecerParametroProcedimientoAlmacenado(TableroPorUsuarioTemporal.IdUsuario, "int", "@IdUsuario");
                EstablecerParametroProcedimientoAlmacenado(TableroPorUsuarioTemporal.FechaCreacion, "datetime", "@FechaCreacion");
                EstablecerParametroProcedimientoAlmacenado(TableroPorUsuarioTemporal.Activo, "bool", "@Activo");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}