﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorGeneral
    {
        public SqlConnection conexionSQL;
        public SqlCommand comandoSQL;
        public SqlDataReader lectorDatosSQL;
        
        public string CADENA_CONEXION = ConfigurationManager.ConnectionStrings["ConexionPerfilamiento"].ConnectionString;

        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            } catch(Exception ex) {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado() {
            comandoSQL.ExecuteNonQuery();
        }

        public string EjecutarProcedimientoAlmacenadoEscalar()
        {
            return comandoSQL.ExecuteScalar().ToString();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro,string descripcionTipo,string nombreParametro) {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo) {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo) {
                case "int":
                    tipoDatoDevolver= SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL() {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public string ArmarJSONGenerico(ObjetoJSONGenerico objetoJSONGenerico)
        {
            return JsonConvert.SerializeObject(objetoJSONGenerico);
        }

        public string ArmarJSONGenericoConError(string mensajeError)
        {
            ObjetoJSONGenerico objetoJSONGenerico = new ObjetoJSONGenerico();

            objetoJSONGenerico.Status = false;
            objetoJSONGenerico.InformacionContenida = "";
            objetoJSONGenerico.Error = mensajeError;
            
            return ArmarJSONGenerico(objetoJSONGenerico);
        }

        public string ArmarJSONGenericoValido(Object listado)
        {
            ObjetoJSONGenerico objetoJSONGenerico = new ObjetoJSONGenerico();

            objetoJSONGenerico.Status = true;
            objetoJSONGenerico.InformacionContenida = JsonConvert.SerializeObject(listado).ToString();
            objetoJSONGenerico.Error = "";

            return ArmarJSONGenerico(objetoJSONGenerico);
        }

        public string ObtenerInformacionFormatoJSON(Object listado)
        {
            return ArmarJSONGenericoValido(listado);
        }
        
    }
}