﻿using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorPerfilesPorUsuario : ControladorGeneral
    {
        private List<Track_PerfilesPorUsuario> listadoPerfilesPorUsuario;

        public Track_PerfilesPorUsuario LlenarPerfilPorUsuario()
        {
            Track_PerfilesPorUsuario perfilPorUsuarioTemporal = new Track_PerfilesPorUsuario();
            perfilPorUsuarioTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            perfilPorUsuarioTemporal.IdUsuario = Convert.ToInt32(lectorDatosSQL["IdUsuario"]);
            perfilPorUsuarioTemporal.IdPerfil = Convert.ToInt32(lectorDatosSQL["IdPerfil"]);
            perfilPorUsuarioTemporal.FechaCreacion = Convert.ToDateTime(lectorDatosSQL["FechaCreacion"]);
            perfilPorUsuarioTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"]);

            return perfilPorUsuarioTemporal;
        }

        public void LlenarListadoPerfilesPorUsuario()
        {
            while (lectorDatosSQL.Read())
                listadoPerfilesPorUsuario.Add(LlenarPerfilPorUsuario());
        }

        public string ObtenerTodosRegistrosPerfilPorUsuario()
        {
            try
            {
                InicializarConexion();

                listadoPerfilesPorUsuario = new List<Track_PerfilesPorUsuario>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPerfilesPorUsuario");

                LlenarLectorDatosSQL();

                LlenarListadoPerfilesPorUsuario();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoPerfilesPorUsuario);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }
    }
}