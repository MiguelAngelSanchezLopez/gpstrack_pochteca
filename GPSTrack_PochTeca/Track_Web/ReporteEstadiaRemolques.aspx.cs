﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

namespace Track_Web
{
    public partial class ReporteEstadiaRemolques : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel();
                    return;
                default:
                    break;
            }
        }

        public void ExportExcel()
        {
            try
            {
                if (Session["ReporteEstadiaRemolques"] != null)
                {
                    string ob = Session["ReporteEstadiaRemolques"].ToString();
                    List<Track_GetReporteEstadiaRemolques_Result> _object = JsonConvert.DeserializeObject<List<Track_GetReporteEstadiaRemolques_Result>>(Session["ReporteEstadiaRemolques"].ToString());
                    bool habilitarCampoReferencia = false;
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "text/csv";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=Reporte_EstadiaRemolques_" + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".xls");
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(Methods_Export.HTML_RPT_EstadiaRemolques(_object, habilitarCampoReferencia));
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
            }


        }
    }
}