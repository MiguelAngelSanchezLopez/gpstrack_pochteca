﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

using System.Globalization;

namespace Track_Web
{
  public partial class PosicionesGPS : System.Web.UI.Page
  {
        IFormatProvider culture = new CultureInfo("en-US", true);

        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel(Request.Form["desde"].ToString(), Request.Form["horaDesde"].ToString(), Request.Form["hasta"].ToString(), Request.Form["horaHasta"].ToString(), Request.Form["patente"].ToString(), Request.Form["economico"].ToString());
                    return;
                default:
                    break;
            }
        }
        public void ExportExcel(string desde, string horaDesde, string hasta, string horaHasta, string patente, string economico)
        {

            if (horaDesde.Trim() != "")
                desde = desde.Replace("T00:00:00", "T") + " " + horaDesde;

            if (horaHasta.Trim() != "")
                hasta = hasta.Replace("T00:00:00", "T") + " " + horaHasta;

            DateTime _fechaDesde;
            DateTime _fechaHasta;

            DateTime.TryParseExact(desde, "dd-MM-yyyy HH:mm", culture, DateTimeStyles.None, out _fechaDesde);
            DateTime.TryParseExact(hasta, "dd-MM-yyyy HH:mm", culture, DateTimeStyles.None, out _fechaHasta);

            string now = DateTime.Now.ToString();
            now = now.Replace(" ", "_");
            now = now.Replace("-", "");
            now = now.Replace(":", "");

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            List<Track_GetPosicionesGPS_2_Result> _posiciones = _objMethosViajes.GetPosicionesGPS(_fechaDesde, _fechaHasta, patente, economico);

            Response.Clear();
            Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment;filename=PosicionesGPS_" + now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(Methods_Export.HTML_PosicionesGPS(_posiciones.ToList()));
            Response.End();
        }
    }
}