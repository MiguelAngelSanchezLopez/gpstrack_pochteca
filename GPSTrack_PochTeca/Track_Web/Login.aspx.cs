﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using BusinessLayer;

namespace Track_Web
{
  public partial class Login : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
            Methods_User _methodsUser = new Methods_User();

            string userName = Utilities.GetUsuarioSession(Session);

            if (userName != "" && userName != null)
            {
                _methodsUser.CerrarSesion(userName);
            }

            Utilities.ResetSession(Session);

    }
  }
}