﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

namespace Track_Web
{
    public partial class ViajesRuta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel(Request.Form["desde"].ToString(), Request.Form["hasta"].ToString(), Request.Form["nroTransporte"].ToString(), Request.Form["patente"].ToString(), Request.Form["economico"].ToString(), Request.Form["estadoViaje"].ToString(), Request.Form["transportista"].ToString(), Request.Form["tipoViaje"].ToString(), Request.Form["idOrigen"].ToString());
                    return;
                default:
                    break;
            }
        }

        public void ExportExcel(string desde, string hasta, string nroTransporte, string patente, string economico, string estadoViaje, string transportista, string tipoViaje, string idOrigen)
        {
            Track_Usuarios _users = new Track_Usuarios();
            string userName = Utilities.GetUsuarioSession(Session);

            desde = desde.Split('-')[2].ToString() + "/" + desde.Split('-')[1].ToString() + "/" + desde.Split('-')[0].ToString();
            hasta = hasta.Split('-')[2].ToString() + "/" + hasta.Split('-')[1].ToString() + "/" + hasta.Split('-')[0].ToString();
            DateTime _desde = Utilities.convertDateIc(desde);
            DateTime _hasta = Utilities.convertDateIc(hasta);

            //DateTime.TryParse(desde, out _desde);
            //DateTime.TryParse(hasta, out _hasta);

            int _nroTransporte;
            int _idOrigen;

            string now = DateTime.Now.ToString();
            now = now.Replace(" ", "_");
            now = now.Replace("-", "");
            now = now.Replace(":", "");



            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                int.TryParse(nroTransporte, out _nroTransporte);
            }

            int.TryParse(idOrigen, out _idOrigen);

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            List<Track_GetViajesHistoricos_2_Result> _viajes = _objMethosViajes.GetViajesHistoricos(_desde, _hasta, _nroTransporte, patente, economico, estadoViaje, transportista, _idOrigen, tipoViaje, userName);

            Response.Clear();
            Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment;filename=ViajesHistoricos_" + now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(Methods_Export.HTML_ViajesHistoricos(_viajes.ToList()));
            Response.End();
        }
    }
}