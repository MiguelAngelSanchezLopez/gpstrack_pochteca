﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportabilidadGPSTrack.aspx.cs" Inherits="Track_Web.ReportabilidadGPSTrack" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
    <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>
    <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

<script type="text/javascript">

    var patente;
    var velocidad;
    var direccion;
    var latitud;
    var longitud;
    var infowindow = new google.maps.InfoWindow();
    var geoLayer = new Array();

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });
        
        var storeZonasToDraw = new Ext.data.JsonStore({
            id: 'storeZonasToDraw',
            autoLoad: false,
            fields: ['IdZona', 'IdTipoZona'],
            proxy: new Ext.data.HttpProxy({
                url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonasToDrawFromCenter',
                reader: { type: 'json', root: 'Zonas' },
                headers: {
                    'Content-type': 'application/json'
                }
            })
        });

        var gridZonasToDraw = Ext.create('Ext.grid.Panel', {
            id: 'gridZonasToDraw',
            store: storeZonasToDraw,
            columns: [
                      { text: 'IdZona', flex: 1, dataIndex: 'IdZona' }
            ]

        });

        var textPatente = new Ext.form.TextField({
            id: 'textPatente',
            labelWidth: 50,
            fieldLabel: 'Placa',
            allowBlank: true,
            anchor: '99%',
            maxLength: 10,
            style: {
                marginTop: '5px',
                marginLeft: '20px'

            }
        });

        var textEconomico = new Ext.form.TextField({
            id: 'textEconomico',
            labelWidth: 60,
            fieldLabel: 'Económico',
            allowBlank: true,
            anchor: '99%',
            maxLength: 10,
            style: {
                marginTop: '5px',
                marginLeft: '20px'

            }
        });

        var btnBuscar = {
            id: 'btnBuscar',
            xtype: 'button',
            iconAlign: 'left',
            icon: 'Images/searchreport_black_20x20.png',
            text: 'Buscar',
            width: 100,
            height: 26,
            handler: function () {
                Buscar();
            },
            style: {
                marginTop: '3px',
                marginLeft: '80px'
            }
        };

        var imageStatus = Ext.create('Ext.Img', {
            id:  'imageStatus', 
            src: 'Images/empty_truck.PNG',
            height: 337,
            width: 500,
            renderTo: Ext.getBody()
        });

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            id: 'toolbar',
            height: 40,
            layout: 'column',
            items: [{
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.3,
                items:[textPatente]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.3,
                items: [textEconomico]
            }, {
                xtype: 'container',
                layout: 'anchor',
                columnWidth: 0.2,
                align: 'right',
                items: [btnBuscar]
            }]
        });

        var imagePanel = new Ext.FormPanel({
            id: 'imagePanel',
            layout: 'anchor',
            border: true,
            items: [imageStatus]
        });

        var displayEstado = new Ext.form.field.Display({
            id: 'displayEstado',
            fieldLabel: 'Estado GPS',
            labelWidth: 150,
            width: 400,
            style: {
                marginTop: '4px',
                marginLeft: '5px'
            },
            labelStyle: 'font-size: large'

        });
        
        var displayUltimoReporte = new Ext.form.field.Display({
            id: 'displayUltimoReporte',
            fieldLabel: 'Último reporte',
            labelWidth: 150,
            width: 400,
            style: {
                marginTop: '3px',
                marginLeft: '5px'
             
            },
            labelStyle: 'font-size: large',
            fieldStyle: 'font-size: large'

        });
        
        var displayTransportista = new Ext.form.field.Display({
            id: 'displayTransportista',
            fieldLabel: 'Línea Transporte',
            labelWidth: 150,
            width: 400,
            style: {
                marginTop: '3px',
                marginLeft: '5px'
            }
        });

        var displayProveedorGPS = new Ext.form.field.Display({
            id: 'displayProveedorGPS',
            fieldLabel: 'Proveedor GPS',
            labelWidth: 150,
            width: 400,
            style: {
                marginTop: '3px',
                marginLeft: '5px'
            }
        });

        var imageLinkMap = Ext.create('Ext.Img', {
            id: 'imageLinkMap',
            width: 105,
            height: 85,
            renderTo: Ext.getBody(),
            style: {
                marginTop: '15px',
                marginLeft: '20px'
            },
            disabled:true,
            listeners: {
                el: {
                    click: function () {
                        if (Ext.getCmp('imageLinkMap').isDisabled() == false) {
                            ShowMap();
                        }
                    }
                }
            },
        });

        var imageLinkMapPanel = new Ext.FormPanel({
            id: 'imageLinkMapPanel',
            layout: 'anchor',
            border: false,
            items: [imageLinkMap]
        });

        var statusPanel = new Ext.FormPanel({
            id: 'statusPanel',
            layout: 'column',
            border: false,
            items: [{
                layout: 'anchor',
                border: false,
                columnWidth: 0.7,
                items: [displayEstado, displayUltimoReporte, displayTransportista, displayProveedorGPS]
            }, {
                layout: 'anchor',
                border: false,
                align: 'center',
                columnWidth: 0.3,
                items: [imageLinkMapPanel]
            }]
        });

        var panelEstado = new Ext.FormPanel({
            id: 'panelEstado',
            title: 'Estado de la Placa',
            tbar: toolbar,
            width: 500,
            height: 520,
            items:[imagePanel, statusPanel]
        });

        var centerPanel = new Ext.FormPanel({
            id: 'centerPanel',
            region: 'center',
            border: true,
            margins: '0 3 3 0',
            layout: {
                type: 'vbox',
                align: 'center',
                pack: 'center'
            },
            items: [panelEstado]
        });

        var viewWidth = Ext.getBody().getViewSize().width;
        var viewHeight = Ext.getBody().getViewSize().height;

        var panelMap = new Ext.FormPanel({
            id: 'panelMap',
            region: 'center',
            html: '<div id="dvMap" style="width:100%; height:100%;"></div>'
        });

        var btnSalirMapa = {
            xtype: 'button',
            iconAlign: 'left',
            icon: 'Images/back_black_16x16.png',
            text: 'Salir',
            width: 70,
            height: 24,
            handler: function (a, b, c, d, e) {
                winMap.close();
            }
        };

        var winMap = new Ext.window.Window({
            id: 'winMap',
            title: 'Posición de la Placa',
            constrain: true,
            height: viewHeight / 1.5,
            width: viewWidth / 2,
            hidden: true,
            modal: true,
            resizable: true,
            border: true,
            draggable: true,
            closeAction: 'hide',
            maximizable: true,
            layout: 'border',
            items: [panelMap],
            buttons: [btnSalirMapa],
            listeners: {
                'resize': function (win, width, height, eOpts) {
                    google.maps.event.trigger(map, "resize");
                }
            }
        });

        var viewport = Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [topMenu, centerPanel]
        });
    });

</script>

    
<script type="text/javascript">
    
    Ext.onReady(function () {
        GeneraMapa("dvMap", true);
    });

    function Buscar() {

        var curPatente = Ext.getCmp("textPatente").getValue();
        var economico = Ext.getCmp("textEconomico").getValue();

        if (Ext.getCmp("textPatente").hasActiveError() || Ext.getCmp("textEconomico").hasActiveError()) {
            return;
        }

        patente = curPatente;

        Ext.Ajax.request({
            url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetEstadoPatente',
            params: {
                patente: patente,
                economico: economico,
            },
            success: function (data, success) {
                if (data != null) {
                    data = Ext.decode(data.responseText);

                    Ext.getCmp('displayEstado').setValue(data[0].Estado);
                    Ext.getCmp('displayUltimoReporte').setValue(data[0].Fecha);
                    Ext.getCmp('displayTransportista').setValue(data[0].Transportista);
                    Ext.getCmp('displayProveedorGPS').setValue(data[0].ProveedorGPS);

                    velocidad = data[0].Velocidad;
                    direccion = data[0].Direccion;
                    latitud = data[0].Latitud;
                    longitud = data[0].Longitud;

                    switch (data[0].Estado) {
                        case "Online":
                            Ext.getCmp('imageStatus').setSrc('Images/green_truck.PNG');
                            Ext.getCmp('displayEstado').setFieldStyle('color: #19c663; font-size: large');
                            Ext.getCmp('imageLinkMap').setSrc('Images/viewMap_105x86.png');
                            Ext.getCmp('imageLinkMap').setDisabled(false);
                            break;
                        case "Offline":
                            Ext.getCmp('imageStatus').setSrc('Images/red_truck.PNG');
                            Ext.getCmp('displayEstado').setFieldStyle('color: #d63b3b; font-size: large');
                            Ext.getCmp('imageLinkMap').setSrc('');
                            Ext.getCmp('imageLinkMap').setDisabled(true);
                            break;
                        case "Placa no integrada":
                            Ext.getCmp('imageStatus').setSrc('Images/gray_truck.PNG');
                            Ext.getCmp('displayEstado').setFieldStyle('color: Black; font-size: large');
                            Ext.getCmp('imageLinkMap').setSrc('');
                            Ext.getCmp('imageLinkMap').setDisabled(true);
                        default:
                            Ext.getCmp('imageStatus').setSrc('Images/gray_truck.PNG');
                            Ext.getCmp('displayEstado').setFieldStyle('color: Black; font-size: large');
                            Ext.getCmp('imageLinkMap').setSrc('');
                            Ext.getCmp('imageLinkMap').setDisabled(true);
                    }

                }
            }
        })

    }

    function ShowMap() {

        if (Ext.getCmp('winMap') != null) {
            Ext.getCmp('winMap').close();
        }

        var title = "Posición de la placa: " + patente;
        Ext.getCmp('winMap').setTitle(title);

        ClearMap();
        Ext.getCmp("winMap").show();

        var myLatlng = new google.maps.LatLng(latitud, longitud);
        var myOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("dvMap"), myOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: new google.maps.MarkerImage("Images/marker_blue_32x32.png"),
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(marker, 'click', function () {

            var contentString =

                '<br>' +
                    '<table>' +
                         '<tr>' +
                            '       <td><b>Fecha</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + Ext.getCmp('displayUltimoReporte').getValue() + '</td>' +
                        '</tr>' +
                         '<tr>' +
                            '       <td><b>Placa</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + patente + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '       <td><b>Línea Transporte</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '       <td>' + Ext.getCmp('displayTransportista').getValue() + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Velocidad:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + velocidad + ' Km/h </td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Latitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + latitud + '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '        <td><b>Longitud:</b></td>' +
                            '       <td><pre>     </pre></td>' +
                            '        <td>' + longitud + '</td>' +
                        '</tr>' +

                    '</table>' +
                    '<br>';

                infowindow.setContent(contentString);
                infowindow.open(map, this);

        });

        //Dibujar las zonas en un radio de 10 km
        var store = Ext.getCmp('gridZonasToDraw').store;
        store.load({
            params: {
                latitud: latitud,
                longitud: longitud
            },
            callback: function (r, options, success) {
                if (success) {
                    for (var i = 0; i < store.count() ; i++) {
                        DrawZone(store.getAt(i).data.IdZona);
                    }
                }
            }
        });
    }


    function DrawZone(idZona) {

        for (var i = 0; i < geoLayer.length; i++) {
            geoLayer[i].layer.setMap(null);
            geoLayer[i].label.setMap(null);
            geoLayer.splice(i, 1);
        }

        Ext.Ajax.request({
            url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
            params: {
                IdZona: idZona
            },
            success: function (data, success) {
                if (data != null) {
                    data = Ext.decode(data.responseText);
                    if (data.Vertices.length > 1) { //Polygon
                        var polygonGrid = new Object();
                        polygonGrid.IdZona = data.IdZona;

                        var arr = new Array();
                        for (var i = 0; i < data.Vertices.length; i++) {
                            arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
                        }

                        if (data.idTipoZona == 3) {
                            var colorZone = "#FF0000";
                        }
                        else {
                            var colorZone = "#7f7fff";
                        }

                        polygonGrid.layer = new google.maps.Polygon({
                            paths: arr,
                            strokeColor: "#000000",
                            strokeWeight: 1,
                            strokeOpacity: 0.9,
                            fillColor: colorZone,
                            fillOpacity: 0.3,
                            labelText: data.NombreZona
                        });
                        polygonGrid.label = new Label({
                            position: new google.maps.LatLng(data.Latitud, data.Longitud),
                            map: map
                        });
                        polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
                        polygonGrid.layer.setMap(map);
                        geoLayer.push(polygonGrid);
                    }
                    else
                        if (data.Vertices.length = 1) { //Point
                            var Point = new Object();
                            Point.IdZona = data.IdZona;

                            var image = new google.maps.MarkerImage("Images/greymarker_32x32.png");

                            Point.layer = new google.maps.Marker({
                                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                icon: image,
                                labelText: data.NombreZona,
                                map: map
                            });

                            Point.label = new Label({
                                position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                map: map
                            });

                            Point.label.bindTo('text', Point.layer, 'labelText');
                            Point.layer.setMap(map);
                            geoLayer.push(Point);
                        }

                }
            },
            failure: function (msg) {
                alert('Se ha producido un error. 3');
            }
        });
    }

</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
  <div id="dvMap"></div>
</asp:Content>