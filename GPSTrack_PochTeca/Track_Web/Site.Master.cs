﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;
using UtilitiesLayer;

namespace Track_Web
{
  public partial class SiteMaster : System.Web.UI.MasterPage
  {
        public List<string> listadoPaginasPermitidasAlUsuario = new List<string>();
        public List<string> listadoTablerosPermitidosAlUsuario = new List<string>();
        public List<int> listadoIdsTablerosPermitidosAlUsuario = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Page_Init(object o, EventArgs e)
        {
            ObtenerListadoPaginasPermitidasAlUsuario();
            CrearJavascriptListadoPaginasPermitidasAlUsuario();
            CrearJavascriptListadoTablerosPermitidosAlUsuario();
        }

        public string ObtenerCodigoJavascriptIdUsuarioLogueado()
        {
            StringBuilder codigoJavascriptIdUsuarioLogueado = new StringBuilder();

            codigoJavascriptIdUsuarioLogueado.Append("var idUsuarioLogueado=" + ObtenerIdUsuarioLogueado() + ";");

            return codigoJavascriptIdUsuarioLogueado.ToString();
        }

        public int ObtenerIdUsuarioLogueado()
        {
            string idusuario = Utilities.GetIdUsuarioSession(Session);
            int _idUsuario = 0;

            int.TryParse(idusuario, out _idUsuario);

            return _idUsuario;
        }

        public void CrearJavascriptListadoPaginasPermitidasAlUsuario()
        {
            StringBuilder cadenaListadoPaginas = new StringBuilder();

            foreach (string pagina in listadoPaginasPermitidasAlUsuario)
                cadenaListadoPaginas.Append("'" + pagina + "',");

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"ObtenerListadoPaginasPermitidasAlUsuario\">\n");

            scriptJSGenerado.Append("var listadoPaginasPermitidasAlUsuario = [");
            scriptJSGenerado.Append(cadenaListadoPaginas.ToString());
            scriptJSGenerado.Append("];");

            scriptJSGenerado.Append(ObtenerCodigoJavascriptIdUsuarioLogueado());

            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", scriptJSGenerado.ToString());

        }

        public String crearElementoMenuTablero(string nombreTablero, int indiceTablero)
        {
            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("   agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {");
            scriptJSGenerado.Append("       menuPaneles.add({");
            scriptJSGenerado.Append("           text: '" + nombreTablero + "',");
            scriptJSGenerado.Append("           icon: 'Images/control_black_24x24.png',");
            scriptJSGenerado.Append("           handler: function () {");
            scriptJSGenerado.Append("               window.location = 'PanelExterno.aspx?idTablero=" + indiceTablero + "';");
            scriptJSGenerado.Append("           }");
            scriptJSGenerado.Append("       })");
            scriptJSGenerado.Append("   };");
            scriptJSGenerado.Append("   ProcesoAgregacionMenu('PanelExterno.aspx', agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);");

            return scriptJSGenerado.ToString();
        }

        public void CrearJavascriptListadoTablerosPermitidosAlUsuario()
        {

            ControladorTablerosPorUsuario controladorTablerosPorUsuario = new ControladorTablerosPorUsuario();

            listadoIdsTablerosPermitidosAlUsuario = controladorTablerosPorUsuario.ObtenerIdsTablerosPorUsuario(ObtenerIdUsuarioLogueado());
            listadoTablerosPermitidosAlUsuario = controladorTablerosPorUsuario.ObtenerNombresTablerosPorUsuario(ObtenerIdUsuarioLogueado());

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" id=\"ObtenerListadoPaginasPermitidasAlUsuario\">\n");

            scriptJSGenerado.Append("var agregacionTablerosDinamica = function () { ");

            for (int indice = 0; indice < listadoTablerosPermitidosAlUsuario.Count; indice++)
                scriptJSGenerado.Append(crearElementoMenuTablero(listadoTablerosPermitidosAlUsuario[indice], listadoIdsTablerosPermitidosAlUsuario[indice]));

            scriptJSGenerado.Append("}");

            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptListadoTablerosPermitidosAlUsuario", scriptJSGenerado.ToString());
        }

        public void ObtenerListadoPaginasPermitidasAlUsuario()
        {
            LectorPaginasAutorizadasAlUsuario lectorPaginas = new LectorPaginasAutorizadasAlUsuario();
            listadoPaginasPermitidasAlUsuario = lectorPaginas.ObtenerTodosRegistrosPaginas(ObtenerIdUsuarioLogueado());
        }

    }
}
