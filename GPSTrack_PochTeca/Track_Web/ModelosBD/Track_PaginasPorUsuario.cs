﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ModelosBD
{
    public class Track_PaginasPorUsuario
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdPagina { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Boolean Activo { get; set; }

        public List<int> IdsPaginasPorUsuario { get; set; }
        public string PaginasPorUsuario { get; set; }
    }
}