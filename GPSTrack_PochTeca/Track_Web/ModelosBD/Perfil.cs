﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ModelosBD
{
    public class Perfil
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Llave { get; set; }

        public List<int> IdsPaginasPorPerfil { get; set; }
    }
}