﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Track_Web.ModelosBD
{
    public class Track_TablerosPorUsuario
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdTablero { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Boolean Activo { get; set; }

        public List<int> IdsTablerosPorUsuario { get; set; }
        public string TablerosPorUsuario { get; set; }
    }
}