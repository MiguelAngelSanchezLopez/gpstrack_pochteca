﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Track_Web.ModelosBD
{
    public class Track_ImagenesPorTablero
    {
        public int Id { get; set; }
        public int IdImagen { get; set; }
        public int IdTablero { get; set; }
        public bool Activo { get; set; }
    }
}