﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Track_Web.ModelosBD
{
    public class Track_ImagenesParaTablero
    {
        public int Id { get; set; }
        public string URL { get; set; }
        public bool Activo { get; set; }
    }
}