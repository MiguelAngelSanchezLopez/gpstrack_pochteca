﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ModelosBD
{
    public class Track_PerfilesPorUsuario
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdPerfil { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Boolean Activo { get; set; }
    }
}