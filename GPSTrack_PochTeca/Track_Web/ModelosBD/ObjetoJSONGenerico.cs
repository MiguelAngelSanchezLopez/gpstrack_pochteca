﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ModelosBD
{
    public class ObjetoJSONGenerico
    {
        public bool Status { get; set; }
        public string InformacionContenida { get; set; }
        public string Error { get; set; }
    }
}