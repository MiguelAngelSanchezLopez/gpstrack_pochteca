﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.SessionState;
using System.Web;

//Add's 
using System.Globalization;

namespace UtilitiesLayer
{
  public class Utilities
  {
    public static void VerifyLoginStatus(HttpSessionState _session, HttpResponse _response)
    {
      if (_session["userName"] == null)
      {
        _response.Redirect("Login.aspx");
      }
    }

    public static void ResetSession(HttpSessionState _session)
    {

        _session.Clear();
    }

    public static string GetTransportistaSession(HttpSessionState _session)
    {
      try
      {
        return _session["transportista"].ToString();
      }
      catch
      {
        return "";
      }

    }

    public static string GetExePath()
    {
      return (System.AppDomain.CurrentDomain.BaseDirectory);
    }

        public static string GetIdUsuarioSession(HttpSessionState _session)
        {
            try
            {
                return _session["idusuario"].ToString();
            }
            catch
            {
                return "";
            }

        }

        public static string GetUsuarioSession(HttpSessionState _session)
    {
      try
      {
        return _session["userName"].ToString();
      }
      catch
      {
        return "";
      }

    }

    /// <summary>
    /// Transforma un string en un formato Datetime, dependiendo de la globalization de la aplicación 
    /// </summary>
    /// <param name="pfecha">Fecha en string a convertir</param>
    /// <returns>Fecha convertida según la cultura</returns>
    public static DateTime convertDateIc(string pfecha)
    {
      DateTime _fecrtn;
      try
      {
        // Obtengo la cultura desde el webconfig
        CultureInfo _culture = CultureInfo.CurrentCulture;
        IFormatProvider formatCulture = _culture;

        // Formateo la fecha de acuerdo a la cultura habilitada 
        DateTime.TryParse(pfecha, formatCulture, DateTimeStyles.None, out _fecrtn);

        // Si no se logro dar formato a la fecha
        if (_fecrtn.Year.ToString() == "1")
        {
          // Habilito la cultura español, para dar un formato estandar a la fecha
          IFormatProvider tmpformat = new CultureInfo("es-cl");
          DateTime.TryParse(pfecha, tmpformat, DateTimeStyles.None, out _fecrtn);

          // Si el formateo estandar da resultado
          if (_fecrtn.Year.ToString() != "1")
          {
            // Vuelvo a intentar dar el formato de acuerdo a la cultura habilitada
            DateTime.TryParse(_fecrtn.ToString(), formatCulture, DateTimeStyles.None, out _fecrtn);
          }
        }
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();

        // Si existe un error se entrega la fecha de hoy
        _fecrtn = DateTime.Now;
      }
      return _fecrtn;
    }

        public static void InsertCriticalError()
        {

        }

  }
}
