﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Web.SessionState;
using System.Web;
using System.Net;
using System.Text.RegularExpressions;

namespace BusinessLayer
{
  public class Methods_User
  {
    private ModelEntities _context = new ModelEntities();

    public string ValidarUsuario(string usuario, string password)
    {
      try
      {
        string userIpAddress = GetUserIP();

        if (userIpAddress == "::1")
        {
            userIpAddress = "127.0.0.1";
        }

        List<Track_ValidarUsuario_Result> _result = _context.Track_ValidarUsuario(usuario, password, userIpAddress).ToList();
        return _result.FirstOrDefault().Respuesta;

      }
      catch(Exception e)
      {
        return "Se ha producido un error.";
      }
    }

    public string getTransportista(string usuario)
    {
      try
      {
        string transportista = "";

        int rolUsuario = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().IdRol;

        if (rolUsuario == 5) //Rol Transportista
        {
          transportista = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Transportista;
        }
 
        return transportista;

      }
      catch (Exception)
      {
        return "";
      }
    }

    public int getPerfilUsuarioConectado(string usuario)
    {
        try
        {
            int perfil = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().IdRol;

            return perfil;

        }
        catch (Exception)
        {
            return 0;
        }
    }

        public string getNameUsuarioConectado(string usuario)
    {
        try
        {
            string fulName = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Nombre + " " + (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Apellidos;

            return fulName;

        }
        catch (Exception)
        {
            return "";
        }
    }

    public string getCEDISAsociado(string usuario)
    {
        try
        {
            string cedis = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().CEDIS.ToString();

            return cedis;

        }
        catch (Exception)
        {
            return "";
        }
    }

    public List<Track_GetDeterminantesAsociados_Result> getDeterminantesAsociados(int idUsuario)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

            List<Track_GetDeterminantesAsociados_Result> _list = _context.Track_GetDeterminantesAsociados(idUsuario).ToList();

            return _list;

        }
        catch (Exception)
        {
            return new List<Track_GetDeterminantesAsociados_Result>();
        }
    }

    public List<Track_GetDeterminantesAsociados_Result> getDeterminantesAsociados(string idUsuario)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

            int intUsuario = (from a in _context.Track_Usuarios where a.Usuario == idUsuario select a).FirstOrDefault().IdUsuario;

            List<Track_GetDeterminantesAsociados_Result> _list = _context.Track_GetDeterminantesAsociados(intUsuario).ToList();

            return _list;

            }
        catch (Exception)
        {
            return new List<Track_GetDeterminantesAsociados_Result>();
        }
    }

    public int webDiferenciaHoraria()
    {
      try
      {
        int webDiferenciaHoraria = Convert.ToInt32((from a in _context.Configuracion where a.Clave == "webDiferenciaHoraria" select a).FirstOrDefault().Valor);
        return webDiferenciaHoraria;
      }
      catch (Exception)
      {
        return 0;
      }
    }

    public void CerrarSesion(string userName)
    {
        _context.Track_CerrarSesion(userName);
    }


    public List<Track_GetUsuarios_Result> GetUsuarios(string userName)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

            List<Track_GetUsuarios_Result> _listUsuarios = _context.Track_GetUsuarios(userName).ToList();
            return _listUsuarios;

        }
        catch (Exception)
        {
            return new List<Track_GetUsuarios_Result>();
        }
    }

    public string GuardarUsuario(int idUsuario, string userName, string password, string nombre, string apellidos, string eMail, int activo, string listCedisASociados, int idUsuarioConectado)
    {
        try
        {

            string _resp = _context.Track_GuardarUsuario(idUsuario, userName, password, nombre, apellidos, eMail, activo, listCedisASociados).FirstOrDefault().Response;

            Methods_User _user = new Methods_User();

            _user.guardarlog(idUsuarioConectado, "Creación / modificación de usuario : " + userName.ToString());

            return _resp;
        }
        catch (Exception)
        {
            return "Se ha producido un error.";
        }
    }

    public string GetUserIP()
    {

        String ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (string.IsNullOrEmpty(ip))
        {
            ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }
        else
        {
            ip = ip.Split(',')[0];
        }

        return ip;
    }

    public void guardarlog(int idUsuario, string action)
    {
        try
        {
            string userIpAddress = GetUserIP();

            if (userIpAddress == "::1")
            {
                userIpAddress = "127.0.0.1";
            }

            _context.Track_GuardarLog(idUsuario, userIpAddress, action);
        }
        catch (Exception ex)
        {

        }
    }

        public void UpdateTrackId(string Usuario)
        {
            try
            {
                _context.Track_UserAltoTrack(Usuario);
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
            }
        }

        public Track_Usuarios getUsuarioConectado(string pUsuario)
        {
            try
            {
                return (from a in _context.Track_Usuarios where a.Usuario == pUsuario select a).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new Track_Usuarios();
            }
        }

    }

}
