﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Web.SessionState;
using UtilitiesLayer;
using System.Configuration;
using CrossCutting;

namespace BusinessLayer
{
  public class Methods_Viajes
  {
    private ModelEntities _context = new ModelEntities();

    public List<Track_GetViajesRuta_Result> GetViajesRuta(int nroTransporte, string patente, string estadoViaje, string transportista)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetViajesRuta_Result> _listViajes = _context.Track_GetViajesRuta(nroTransporte, patente, estadoViaje, transportista).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetViajesRuta_Result>();
      }
    }

    public List<Track_GetNroTransportesRuta_Result> GetNroTransportesRuta()
    {
      try
      {
        List<Track_GetNroTransportesRuta_Result> _listNroTransportes = _context.Track_GetNroTransportesRuta().ToList();
        return _listNroTransportes;

      }
      catch (Exception)
      {
        return new List<Track_GetNroTransportesRuta_Result>();
      }
    }

    public List<Track_GetPatentesRuta_Result> GetPatentesRuta()
    {
      try
      {
        List<Track_GetPatentesRuta_Result> _listPatentesRuta = _context.Track_GetPatentesRuta().ToList();
        return _listPatentesRuta;

      }
      catch (Exception)
      {
        return new List<Track_GetPatentesRuta_Result>();
      }
    }

    public List<Track_GetTransportistasRuta_Result> GetTransportistasRuta()
    {
      try
      {
        List<Track_GetTransportistasRuta_Result> _listTransportistasRuta = _context.Track_GetTransportistasRuta().ToList();
        return _listTransportistasRuta;

      }
      catch (Exception)
      {
        return new List<Track_GetTransportistasRuta_Result>();
      }
    }

    public List<Track_GetPosicionesRuta_Result> GetPosicionesRuta(string patenteTracto, string patenteTrailer, DateTime? fechaHoraCreacion, DateTime? fechaHoraSalidaOrigen, DateTime? fechaHoraLlegadaDestino, long nroTransporte, long idEmbarque, int destino, string estadoViaje)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

        List<Track_GetPosicionesRuta_Result> _listaPosicionesRuta = _context.Track_GetPosicionesRuta(patenteTracto, patenteTrailer, fechaHoraCreacion, fechaHoraSalidaOrigen, fechaHoraLlegadaDestino, nroTransporte, idEmbarque, destino, estadoViaje).ToList();
        return _listaPosicionesRuta;

      }
      catch (Exception e)
      {
        CommonUtilities.WriteInfo("Message: " + e.Message + " Inner Exception: " + e.InnerException + " StackTrace: " + e.StackTrace, CommonUtilities.EventType.Error, ConfigurationManager.AppSettings["LogPath"].ToString(), true);
        return new List<Track_GetPosicionesRuta_Result>();
      }
    }

    public List<Track_Movil> GetAllPatentes(string transportista, string userName, bool _all = false)
    {
        try
        {
            List<Track_Movil> _listaPatentes = (from c in _context.Track_Movil where (c.Transportista == transportista || transportista == "Todos" || transportista == "") select c).ToList();

            if (_all)
            {
                Track_Movil newItem = new Track_Movil { Patente = "Todas" };
                    _listaPatentes.Insert(0, newItem);
            }

            return _listaPatentes;

        }
        catch (Exception)
        {
            return new List<Track_Movil>();
        }
    }

    public List<PoolPlaca> GetAllPatentesPool(string transportista, bool _all = false)
    {
      try
      {
        List<PoolPlaca> _listaPatentes = (from c in _context.PoolPlaca where (c.Transporte == transportista || transportista == "Todos" || transportista == "Todas" || transportista == "") select c).ToList();
        if (_all)
        {
          PoolPlaca newItem = new PoolPlaca { Placa = "Todas" };
          _listaPatentes.Insert(0, newItem);
        }

        return _listaPatentes;

      }
      catch (Exception)
      {
        return new List<PoolPlaca>();
      }
    }

    public List<PoolPlacaRemolque> GetAllPatentesPoolRemolques(string transportista, bool _all = false)
    {
        try
        {
            List<PoolPlacaRemolque> _listaPatentes = (from c in _context.PoolPlacaRemolque where (c.Carrier == transportista || transportista == "Todos" || transportista == "Todas" || transportista == "") select c).ToList();
            if (_all)
            {
                PoolPlacaRemolque newItem = new PoolPlacaRemolque { Placa = "Todas" };
                _listaPatentes.Insert(0, newItem);
            }

            return _listaPatentes;

        }
        catch (Exception)
        {
            return new List<PoolPlacaRemolque>();
        }
    }

    public List<PoolPlaca> GetAllCedisPool(string transportista, string userName, bool _all = false)
    {
      try
      {
        int idUsuario;
        string[] CEDISAsociado;

        List<PoolPlaca> _listaCedis = new List<PoolPlaca>();

        int perfil = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdRol).FirstOrDefault();

        if (perfil > 2)
        {
            idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

            CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

            _listaCedis = (from c in _context.PoolPlaca where (CEDISAsociado.Contains(c.Determinante.ToString())) && (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
        }
        else
        {
            _listaCedis = (from c in _context.PoolPlaca where (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
        }

        if (_all)
        {
          PoolPlaca newItem = new PoolPlaca {Determinante = -1, Cedis = "Todas" };
          _listaCedis.Insert(0, newItem);
        }

        return _listaCedis;

      }
      catch (Exception e)
      {
        return new List<PoolPlaca>();
      }
    }
/*
    public List<PoolPlacaRemolque> GetAllCedisPoolRemolque(string transportista, string userName, bool _all = false)
    {
        try
        {
            int idUsuario;
            string[] CEDISAsociado;

            List<PoolPlacaRemolque> _listaCedis;

            int perfil = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdRol).FirstOrDefault();

            if (perfil > 2)
            {
                idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

                CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

                _listaCedis = (from c in _context.PoolPlacaRemolque where ((CEDISAsociado.Contains(c.Determinante.ToString())) && (c.Carrier == transportista || transportista == "Todos" || transportista == "")) select c).Distinct().ToList();
            }
            else
            {
                _listaCedis = (from c in _context.PoolPlacaRemolque where (c.Carrier == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
            }

            if (_all)
            {
                PoolPlacaRemolque newItem = new PoolPlacaRemolque { Cedis = "Todas" };
                _listaCedis.Insert(0, newItem);
            }

            return _listaCedis;

        }
        catch (Exception e)
        {
            return new List<PoolPlacaRemolque>();
        }
    }
    */
        public List<PoolPlacaRemolque> GetAllCarrierPool(bool _all = false)
    {
        try
        {
            List<PoolPlacaRemolque> _listaCarrier = (from c in _context.PoolPlacaRemolque select c).Distinct().ToList();
            if (_all)
            {
                    PoolPlacaRemolque newItem = new PoolPlacaRemolque { Carrier = "Todos" };
                _listaCarrier.Insert(0, newItem);
            }

            return _listaCarrier;

        }
        catch (Exception e)
        {
            return new List<PoolPlacaRemolque>();
        }
    }

        public List<PoolPlaca> GetTransportistabyCedis(string cedis)
    {
      try
      {
        List<PoolPlaca> _listTransportistas = (from c in _context.PoolPlaca where (cedis.Contains(c.Cedis) || cedis == "Todos" || cedis == "Todas") select c).Distinct().ToList();
        return _listTransportistas;
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();
        return new List<PoolPlaca>();
      }
    }

    public List<Track_Movil> GetAllTransportistas(bool _all, HttpSessionState _session)
    {
      try
      {
        List<Track_Movil> _listaTransportistas = new List<Track_Movil>();
        string transportista = Utilities.GetTransportistaSession(_session);

        if (transportista != "")
        {
          Track_Movil newItem = new Track_Movil { Transportista = transportista };
          _listaTransportistas.Insert(0, newItem);
        }
        else
        {
          _listaTransportistas = (from c in _context.Track_Movil select c).ToList();
          if (_all)
          {
            Track_Movil newItem = new Track_Movil { Transportista = "Todos" };
            _listaTransportistas.Insert(0, newItem);
          }
        }
        return _listaTransportistas;

      }
      catch (Exception)
      {
        return new List<Track_Movil>();
      }
    }

    public List<Track_GetViajesHistoricos_2_Result> GetViajesHistoricos(DateTime desde, DateTime hasta, long nroTransporte, string patente, string economico, string estadoViaje, string transportista, int idOrigen, string tipoViaje, string userName)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetViajesHistoricos_2_Result> _listViajes = _context.Track_GetViajesHistoricos_2(desde, hasta, transportista, patente, economico, estadoViaje, nroTransporte, idOrigen, tipoViaje, userName).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetViajesHistoricos_2_Result>();
      }
    }

    public List<Track_GetPosicionesGPS_2_Result> GetPosicionesGPS(DateTime fechaDesde, DateTime fechaHasta, string patente, string economico)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

        List<Track_GetPosicionesGPS_2_Result> _listaPosicionesGPS = _context.Track_GetPosicionesGPS_2(fechaDesde, fechaHasta, patente, economico).ToList();
        return _listaPosicionesGPS;

      }
      catch (Exception)
      {
        return new List<Track_GetPosicionesGPS_2_Result>();
      }
    }

    public List<Track_GetPosicionesGPS_Ruta_Result> GetPosicionesGPS_Ruta(DateTime fechaDesde, DateTime fechaHasta, string patente)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

        List<Track_GetPosicionesGPS_Ruta_Result> _listaPosicionesGPS_Ruta = _context.Track_GetPosicionesGPS_Ruta(fechaDesde, fechaHasta, patente).ToList();
        return _listaPosicionesGPS_Ruta;

      }
      catch (Exception)
      {
        return new List<Track_GetPosicionesGPS_Ruta_Result>();
      }
    }

    public List<Track_GetInformeViajes_Result> GetInformeViajes(DateTime desde, DateTime hasta, long nroTransporte, string patente, string transportista, string userName)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetInformeViajes_Result> _listViajes = _context.Track_GetInformeViajes(desde, hasta, transportista, patente, nroTransporte, userName).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetInformeViajes_Result>();
      }
    }

    public List<Track_GetDetalleTrayecto_Result> GetDetalleTrayecto(long nroTransporte)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetDetalleTrayecto_Result> _listaDetalleTrayecto = _context.Track_GetDetalleTrayecto(nroTransporte).ToList();
        return _listaDetalleTrayecto;

      }
      catch (Exception)
      {
        return new List<Track_GetDetalleTrayecto_Result>();
      }
    }

    public List<Track_GetNroTransportes_Result> GetNroTransportes(DateTime desde, DateTime hasta)
    {
      try
      {
        List<Track_GetNroTransportes_Result> _listNroTransportes = _context.Track_GetNroTransportes(desde, hasta).ToList();
        return _listNroTransportes;

      }
      catch (Exception)
      {
        return new List<Track_GetNroTransportes_Result>();
      }
    }

    public List<Track_GetFlotaOnline_Result> GetFlotaOnline(string patente, string transportista, int ignicion, string estadoViaje, string estadoGPS, string proveedorGPS)
    {
      try
      {
        List<Track_GetFlotaOnline_Result> _listaFlotaOnline = _context.Track_GetFlotaOnline(patente, transportista, ignicion, estadoViaje, estadoGPS, proveedorGPS).ToList();
        return _listaFlotaOnline;

      }
      catch (Exception)
      {
        return new List<Track_GetFlotaOnline_Result>();
      }
    }

    public List<Track_GetViajesAsignados_Result> GetViajesAsignados(string userName)
    {
      try
      {
        List<Track_GetViajesAsignados_Result> _listViajes = _context.Track_GetViajesAsignados(userName).ToList();
        return _listViajes;

      }
      catch (Exception e)
      {
        return new List<Track_GetViajesAsignados_Result>();
      }
    }

    public List<Track_GetViajesControlPanel_Result> GetViajesControlPanel(DateTime desde, DateTime hasta, int nroTransporte, string patente, string estadoViaje, string transportista, string alertas, string userName)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetViajesControlPanel_Result> _listViajes = _context.Track_GetViajesControlPanel(desde, hasta, transportista, patente, estadoViaje, nroTransporte, alertas, userName).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetViajesControlPanel_Result>();
      }
    }

    public string EliminarViaje(int nroTransporte, int idEmbarque, int codLocal)
    {
      try
      {
        string _resp = _context.Track_EliminarViaje(nroTransporte, idEmbarque, codLocal).FirstOrDefault().Respuesta;

        return _resp;
      }
      catch (Exception)
      {
        return "Error al intentar eliminar el viaje.";
      }

    }

    public int ValidarMovilCD(string patente)
    {
      try
      {
        int _resp = _context.Track_ValidarMovilCD(patente).FirstOrDefault().Respuesta.Value;

        return _resp;
      }
      catch (Exception)
      {
        return -1;
      }
    }

    public bool ValidarNroTransporte(int nroTransporte)
    {
      try
      {
        TrazaViaje _existe = _context.TrazaViaje.Where(C => C.NroTransporte == nroTransporte).FirstOrDefault();

        if (_existe != null)
        {
          return false;
        }

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public string NuevoViaje(long nroTransporte, string tipoViaje, string transportista, string regresaEnvases, string tracto, string trailer, string tipoUnidad, string rutConductor, string nombreConductor, int codOrigen, string listaDestinos, DateTime fechaCita, string userName)
    {
      try
      {

        string _resp = _context.Track_NuevoViaje(nroTransporte, tipoViaje, transportista, regresaEnvases, tracto, trailer, tipoUnidad, rutConductor, nombreConductor, codOrigen, listaDestinos, fechaCita, userName).FirstOrDefault().Respuesta;

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public string EditarViaje(int nroTransporte, string transportista, string trailer, string tracto, int codOrigen, int codDestino, string rutConductor, string nombreConductor)
    {
      try
      {

        string _resp = _context.Track_EditarViaje(nroTransporte, transportista, trailer, tracto, codOrigen, codDestino, rutConductor, nombreConductor).FirstOrDefault().Respuesta;

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public List<Track_GetPosicionesRutaModuloMapa_Result> GetPosicionesRutaModuloMapa(int idAlerta)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

        List<Track_GetPosicionesRutaModuloMapa_Result> _listaPosicionesRuta = _context.Track_GetPosicionesRutaModuloMapa(idAlerta).ToList();
        return _listaPosicionesRuta;

      }
      catch (Exception)
      {
        return new List<Track_GetPosicionesRutaModuloMapa_Result>();
      }
    }

    public List<Track_GetProveedoresGPS_Result> GetProveedoresGPS(bool _all, string rutTransportista)
    {
      try
      {
        List<Track_GetProveedoresGPS_Result> _listProveedores = _context.Track_GetProveedoresGPS(rutTransportista).ToList();

        if (_all)
        {
          Track_GetProveedoresGPS_Result item = new Track_GetProveedoresGPS_Result { ProveedorGPS = "Todos" };
          _listProveedores.Insert(0, item);
        }

        return _listProveedores;

      }
      catch (Exception)
      {
        return new List<Track_GetProveedoresGPS_Result>();
      }
    }

    public List<Track_GetRutasGeneradas_Result> GetRutasGeneradas(int idRuta)
    {
      try
      {
        List<Track_GetRutasGeneradas_Result> _listRutas = _context.Track_GetRutasGeneradas(idRuta).ToList();
        return _listRutas;

      }
      catch (Exception)
      {
        return new List<Track_GetRutasGeneradas_Result>();
      }
    }

    public List<Track_GetDetalleRuta_Result> GetDetalleRuta(int idRuta)
    {
      try
      {
        List<Track_GetDetalleRuta_Result> _listDetalleRuta = _context.Track_GetDetalleRuta(idRuta).ToList();
        return _listDetalleRuta;

      }
      catch (Exception)
      {
        return new List<Track_GetDetalleRuta_Result>();
      }
    }

    public string EliminarRutaGenerada(int IdRuta)
    {
      try
      {
        string _resp = _context.Track_EliminarRutaGenerada(IdRuta).FirstOrDefault().ToString();

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public string EliminarDestinoRuta(int IdZona)
    {
      try
      {
        string _resp = _context.Track_EliminarDestinoRuta(IdZona).FirstOrDefault().ToString();

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public string AgrgarRutaGenerada(string nombreRuta)
    {
      try
      {
        string _resp = _context.Track_AgregarRutaGenerada(nombreRuta).FirstOrDefault().ToString();

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public string AgregarDestinoRuta(int idRuta, int idZona)
    {
      try
      {
        string _resp = _context.Track_AgregarDestinoRuta(idRuta, idZona).FirstOrDefault().ToString();

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public string NuevoViajeDestinoUnico(int nroTransporte, string transportista, int codOrigen, int codDestino, string patenteTrailer, string patenteTracto, string rutConductor, string nombreConductor)
    {
      try
      {
        string _resp = _context.Track_NuevoViajeDestinoUnico(nroTransporte, transportista, patenteTrailer, patenteTracto, codOrigen, codDestino, rutConductor, nombreConductor, 1).FirstOrDefault().ToString();

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public List<Track_GetViajesDashboard_2_Result> GetViajesDashboardControl(DateTime desde, DateTime hasta, long nroTransporte, string patenteTracto, string patenteTrailer, string economico, string estadoViaje, string transportista, string alertas, string userName)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetViajesDashboard_2_Result> _listViajes = _context.Track_GetViajesDashboard_2(desde, hasta, transportista, patenteTracto, patenteTrailer, economico, estadoViaje, nroTransporte, alertas, userName).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetViajesDashboard_2_Result>();
      }
    }

    public List<Track_GetMonitoreoOnline_2_Result> GetMonitoreoOnline(string patente, string economico, string transportista, int ignicion, string estadoViaje, string estadoGPS, string proveedorGPS, long nroTransporte, string userName)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetMonitoreoOnline_2_Result> _listaMonitoreoOnline = _context.Track_GetMonitoreoOnline_2(patente, economico, transportista, ignicion, estadoViaje, estadoGPS, proveedorGPS, nroTransporte, userName).ToList();
        return _listaMonitoreoOnline;

      }
      catch (Exception ex)
      {
        return new List<Track_GetMonitoreoOnline_2_Result>();
      }
    }

    public List<Track_GetViajesBackhaul_Result> GetViajesBackhaul(DateTime desde, DateTime hasta, long nroTransporte, string patente, string transportista, int codLocal, string userName)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetViajesBackhaul_Result> _listViajes = _context.Track_GetViajesBackhaul(desde, hasta, transportista, patente, nroTransporte, codLocal, userName).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetViajesBackhaul_Result>();
      }
    }

    public List<Track_GetCamionesCercanosBackhaul_Result> GetCamionesCercanosBackhaul()
    {
      try
      {
        List<Track_GetCamionesCercanosBackhaul_Result> _lista = _context.Track_GetCamionesCercanosBackhaul().ToList();
        return _lista;

      }
      catch (Exception)
      {
        return new List<Track_GetCamionesCercanosBackhaul_Result>();
      }
    }

    public string NuevoViajeBackhaul(long nroTransporte, long idEmbarque, string rutTransportista, string tracto, string trailer, int codDeterminante, int codproveedor, int codCEDIS)
    {
      try
      {
        string _resp = _context.Track_NuevoViajeBackhaul(nroTransporte, idEmbarque, rutTransportista, tracto, trailer, codDeterminante, codproveedor, codCEDIS).FirstOrDefault().ToString();

        return _resp;
      }
      catch (Exception)
      {
        return "Se ha producido un error.";
      }
    }

    public List<Track_GetUltimaPosicion_Result> GetUltimaPosicion(int idAlerta)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

        List<Track_GetUltimaPosicion_Result> _listViajes = _context.Track_GetUltimaPosicion(idAlerta).ToList();
        return _listViajes;

      }
      catch (Exception)
      {
        return new List<Track_GetUltimaPosicion_Result>();
      }
    }

    public List<Track_GetRotacionTracto_Result> getRotacionTracto()
    {
      try
      {
        return _context.Track_GetRotacionTracto().ToList();
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();
        return new List<Track_GetRotacionTracto_Result>();
      }
    }

    public List<Track_GetRotacionRemolque_Result> getRotacionRemolque(int cantidad)
    {
      try
      {
        List<Track_GetRotacionRemolque_Result> _result;

        if (cantidad > 3)
          _result = _context.Track_GetRotacionRemolque().Where(x => x.CantidadViajes > 3).ToList();
        else
          _result = _context.Track_GetRotacionRemolque().Where(x => x.CantidadViajes == cantidad).ToList();

        return _result;
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();
        return new List<Track_GetRotacionRemolque_Result>();
      }
    }

    public void SaveProgramacion(string tracto, string embarque, string determinante, string remolque, string rotacion, string zona, string UsuarioCreacion)
    {
      _context.Track_saveProgrmacion(tracto, embarque, determinante, remolque, rotacion, zona, UsuarioCreacion);
    }

    public int InsideVentanaHoraria(DateTime fecha, int codLocal)
    {
      try
      {
        int _resp = (int)_context.Track_InsideVentanaHoraria(fecha, codLocal).FirstOrDefault().Response;

        return _resp;
      }
      catch (Exception)
      {
        return -1;
      }
    }

    public List<Track_InforWindows_Result> InfoWindows(long nroTransporte)
    {
      try
      {
        return _context.Track_InforWindows(nroTransporte).ToList();
      }
      catch (Exception ex)
      {
        string err = ex.Message.ToString();
        return new List<Track_InforWindows_Result>();
      }
    }

    public List<Track_GetReporteEstadiaTractos_Result> getReporteEstadiaTractos(string Transportista, string patente, string userName)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
            return _context.Track_GetReporteEstadiaTractos(Transportista, patente, "Todas", userName).ToList();
        }
        catch (Exception ex)
        {
            return new List<Track_GetReporteEstadiaTractos_Result>();
        }
    }

    public List<Track_GetReporteEstadiaRemolques_Result> getReporteEstadiaRemolques(string patente, string listCarrier, string userName)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
            return _context.Track_GetReporteEstadiaRemolques(patente, "Todos", listCarrier, userName).ToList();
        }
        catch (Exception ex)
        {
            return new List<Track_GetReporteEstadiaRemolques_Result>();
        }
    }

    public List<Track_GetReporteEstadiaTractos_Historico_Result> getReporteEstadiaTractos_Historico(DateTime desde, DateTime hasta, string Transportista, string patente, string userName)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
            return _context.Track_GetReporteEstadiaTractos_Historico(desde, hasta, Transportista, patente, "Todas", userName).ToList();
        }
        catch (Exception ex)
        {
            return new List<Track_GetReporteEstadiaTractos_Historico_Result>();
        }
    }

    public List<Track_GetReporteEstadiaRemolques_Historico_Result> getReporteEstadiaRemolques_Historico(DateTime desde, DateTime hasta, string patente, string listCarrier, string userName)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
            return _context.Track_GetReporteEstadiaRemolques_Historico(desde, hasta, patente, "Todos", listCarrier, userName).ToList();
        }
        catch (Exception ex)
        {
            return new List<Track_GetReporteEstadiaRemolques_Historico_Result>();
        }
    }

    public List<Track_GetReporteComercial_Result> getReporteComercial(DateTime? fecDesde, DateTime? fecHasta, string Cedis)
    {
        try
        {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
            return _context.Track_GetReporteComercial(fecDesde, fecHasta, Cedis).ToList();
        }
        catch (Exception ex)
        {
            return new List<Track_GetReporteComercial_Result>();
        }
    }

    public List<Track_Movil> GetAllEconomicos (bool _all = false)
    {
        try
        {
            List<Track_Movil> _listaEconomicos = (from c in _context.Track_Movil orderby c.Economico select c).ToList();

            if (_all)
            {
                Track_Movil newItem = new Track_Movil { Economico = "Todos" };
                _listaEconomicos.Insert(0, newItem);
            }

            return _listaEconomicos;

        }
        catch (Exception)
        {
            return new List<Track_Movil>();
        }
    }

    }
}
